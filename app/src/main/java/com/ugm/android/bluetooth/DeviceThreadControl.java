package com.ugm.android.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.text.TextUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.UUID;

public class DeviceThreadControl {
    private static Logger logger = LoggerFactory.getLogger(DeviceThreadControl.class);

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device

    public static final UUID MY_UUID_SECURE =
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    public static final UUID MY_UUID_INSECURE =
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");


    protected ConnectingDeviceThread mConnectingDeviceThread;
    protected ConnectedDeviceThread mConnectedDeviceThread;
    protected BluetoothDevice mConnectedDevice;
    protected int mState;
    private int mNewState;

    public interface IThreadCreator {
        ConnectedDeviceThread Create(BluetoothSocket socket, String socketType, BluetoothDevice device);
    }

    public interface INotifier {
        void connected(BluetoothDevice device);
        void connectionLost(BluetoothDevice device);
        void connectionFailed(BluetoothDevice device);
    }


    protected final IThreadCreator mthreadCreator;
    protected final  INotifier mNotifer;

    public int getState() {return mState;}
    public BluetoothDevice getConnectedDevice() {return mConnectedDevice;}

    public DeviceThreadControl(IThreadCreator creator, INotifier notifier){
        mthreadCreator = creator;
        mState = STATE_NONE;
        mNewState = mState;
        mNotifer = notifier;
    }

    public void start(){
        // Cancel any thread attempting to make a connection
        if (mConnectingDeviceThread != null) {
            mConnectingDeviceThread.cancel();
            mConnectingDeviceThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedDeviceThread != null) {
            mConnectedDeviceThread.cancel();
            mConnectedDeviceThread = null;
        }
        mState = STATE_NONE;
    }

    public void stop(){
        if (mConnectingDeviceThread != null) {
            mConnectingDeviceThread.cancel();
            mConnectingDeviceThread = null;
        }

        if (mConnectedDeviceThread != null) {
            mConnectedDeviceThread.cancel();
            mConnectedDeviceThread = null;
        }

        mState = STATE_NONE;
    }

    public void connect(BluetoothDevice device, boolean secure){
        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectingDeviceThread != null) {
                mConnectingDeviceThread.cancel();
                mConnectingDeviceThread = null;
            }
        }

        // Cancel any thread currently running a connection
        if (mConnectedDeviceThread != null) {
            mConnectedDeviceThread.cancel();
            mConnectedDeviceThread = null;
        }

        // Start the thread to connectToLocator with the given device
        mState = STATE_CONNECTING;
        mConnectingDeviceThread = new ConnectingDeviceThread(device, secure, this);
        mConnectingDeviceThread.start();
    }
}

/**
 * This thread runs while attempting to make an outgoing connection
 * with a device. It runs straight through; the connection either
 * succeeds or fails.
 */
 class ConnectingDeviceThread extends Thread {
    private static Logger logger = LoggerFactory.getLogger(ConnectingDeviceThread.class);

    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private final DeviceThreadControl mmThreadObject;
    private String mSocketType;
    private final BluetoothAdapter mAdapter;

    public ConnectingDeviceThread(BluetoothDevice device, boolean secure, DeviceThreadControl threadObject) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mmDevice = device;
        mmThreadObject = threadObject;
        BluetoothSocket tmpSocket = null;
        mSocketType = secure ? "Secure" : "Insecure";
        logger.info("mSocketType:" + mSocketType);
        // Get a BluetoothSocket for a connection with the given BluetoothDevice
        try {
            if (secure) {
                logger.info("Device Name:" + device.getName());
                if (device != null && !TextUtils.isEmpty(device.getName()) && device.getName().equalsIgnoreCase("receiver")) {
                    tmpSocket = BluetoothUtils.createRfcommSocket(device);
                } else {
                    tmpSocket = BluetoothUtils.createBTSocket(device);
                }
            } else {
                tmpSocket = device.createInsecureRfcommSocketToServiceRecord(DeviceThreadControl.MY_UUID_INSECURE);
            }
        } catch (IOException e) {
            logger.error("Socket Type: " + mSocketType + "create() failed = " + e.toString());
        } catch (Exception e) {
            logger.error("Socket Type: " + mSocketType + "create() failed = " + e.toString());
        }
        mmSocket = tmpSocket;
        mmThreadObject.mState = DeviceThreadControl.STATE_CONNECTING;
    }

    public void run() {
        logger.info("BEGIN mConnectingDeviceThread SocketType:" + mSocketType);
        setName("ConnectingDeviceThread" + mSocketType);

        // Always cancel discovery because it will slow down a connection
        mAdapter.cancelDiscovery();

        // Make a connection to the BluetoothSocket
        try {
            // This is a blocking call and will only return on a
            // successful connection or an exception
            mmSocket.connect();
        } catch (IOException e) {
            mmThreadObject.mState = DeviceThreadControl.STATE_NONE;
            // Close the socket
            logger.error("command DEVICE CONNECTION FAILED ########" + e.toString());
            try {
                mmSocket.close();
            } catch (IOException e2) {
                logger.info("unable to close() " + mSocketType + " socket during connection failure" + e2.toString());
            }

            mmThreadObject.mNotifer.connectionLost(mmThreadObject.mConnectedDevice);
            //connectionFailed(mmThreadObject);
            return;
        }

        mmThreadObject.mConnectingDeviceThread = null;

        // Start the startConnectedDeviceThread thread
        startConnectedDeviceThread(mmSocket, mmDevice, mSocketType);
    }

    public void cancel() {
        try {
            logger.info("close()");
            mmSocket.close();
        } catch (IOException e) {
            logger.error("close() of connection to device " + mSocketType + " socket failed" + e.toString());
        }
        mmThreadObject.mState = DeviceThreadControl.STATE_NONE;
    }

    /**
     * Start the ConnectedDeviceThread to begin managing a Bluetooth connection
     *
     * @param socket The BluetoothSocket on which the connection was made
     * @param device The BluetoothDevice that has been startConnectedDeviceThread
     */
    private synchronized void startConnectedDeviceThread(BluetoothSocket socket, BluetoothDevice device, final String socketType) {
        logger.info("startConnectedDeviceThread, Socket Type:" + socketType + " , Device Name = " + device.getName());

        // Cancel the thread that completed the connection
        if (mmThreadObject.mConnectingDeviceThread != null) {
            mmThreadObject.mConnectingDeviceThread.cancel();
            mmThreadObject.mConnectingDeviceThread = null;
        }

        // Cancel any thread currently running a connection
        if (mmThreadObject.mConnectedDeviceThread != null) {
            mmThreadObject.mConnectedDeviceThread.cancel();
            mmThreadObject.mConnectedDeviceThread = null;
        }

        // Start the thread to manage the connection and perform transmissions
        if (mmThreadObject.mthreadCreator != null) {
            mmThreadObject.mConnectedDeviceThread = mmThreadObject.mthreadCreator.Create(socket, socketType, device);
            mmThreadObject.mConnectedDeviceThread.start();
        }

        mmThreadObject.mState = DeviceThreadControl.STATE_CONNECTED;
        logger.info("startConnectedDeviceThread Device Connected: Device Name = " + device.getName());
    }
}
