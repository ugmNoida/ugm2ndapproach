package com.ugm.android.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.ugm.android.R;
import com.ugm.android.ui.activities.AbstractBaseActivity;
import com.ugm.android.utilities.Constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseDevice implements DeviceThreadControl.INotifier {

        // Debugging
        private static Logger logger = LoggerFactory.getLogger(BaseDevice.class);
        protected DeviceThreadControl mDeviceThreadControl;

        protected Context mContext;
        protected AbstractBaseActivity mBaseActivity;
        
        protected synchronized void updateUserInterfaceTitle() {
            logger.info("BaseDevice:updateUserInterfaceTitle() " + mDeviceThreadControl.getState());
        }

        public synchronized int getState() {
            return mDeviceThreadControl.getState();
        }

        public BluetoothDevice getConnectedDevice() {
            if (mDeviceThreadControl.getState() == DeviceThreadControl.STATE_CONNECTED ) {
                BluetoothDevice device = mDeviceThreadControl.getConnectedDevice();
                return device;
            } else {
                return null;
            }
        }

        public synchronized void start() {
            logger.info("BaseDevice:start");
            mDeviceThreadControl.start();
            // Update UI title
            updateUserInterfaceTitle();
        }

        public synchronized boolean disconnect() {
            // Update UI title
            updateUserInterfaceTitle();
            // Start the service over to restart listening mode
            mDeviceThreadControl.start();

            expressIntent(Constants.ACTION_VALUE_CONNECTION_LOST);
            return true;
        }

    private void expressIntent(String actionValueConnectionLost) {
        if(null !=  mDeviceThreadControl &&  null != mDeviceThreadControl.mConnectedDevice) {
            Intent intent = new Intent(actionValueConnectionLost);
            intent.putExtra(Constants.DEVICE_NAME, mDeviceThreadControl.mConnectedDevice.getName());
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        }
    }

    /**
         * Start the ConnectingDeviceThread to initiate a connection to a GPS device.
         *
         * @param device The BluetoothDevice to connectToLocator
         * @param secure Socket Security type - Secure (true) , Insecure (false)
         */
        public synchronized void connect(BluetoothDevice device, boolean secure) {
            logger.info("BaseDevice:connectToLocator to: " + device);

            mDeviceThreadControl.connect(device, secure);
        }

        /**
         * Stop all threads
         * TODO = need to call this method when we are closing application
         */
        public synchronized void stop() {
            logger.info("BaseDevice:stop");
            mDeviceThreadControl.stop();
            // Update UI title
            updateUserInterfaceTitle();
        }

        /**
         * Indicate that the connection attempt failed and notify the UI Activity.
         */
        private void connectionFailed(DeviceThreadControl threadObject) {
            // Start the service over to restart listening mode
            if (mBaseActivity != null) {
                logger.info("BaseDevice:Bluetooth Connection Failed...device name: " + mDeviceThreadControl.mConnectedDevice.getName());
                mBaseActivity.showMessage(mContext.getString(R.string.bluetooth_unavailable), mContext.getString(R.string.ok), null);
            }

            expressIntent(Constants.ACTION_VALUE_CONNECTION_LOST);
        }

        /**
         * Indicate that the connection was lost and notify the UI Activity.
         */
        public void connectionLost(BluetoothDevice device) {
            updateUserInterfaceTitle();
            // Start the service over to restart listening mode
            if (mBaseActivity != null) {
                logger.info("BaseDevice: Bluetooth Connection Lost. Device name: " + mDeviceThreadControl.mConnectedDevice.getName());
                mBaseActivity.showMessage(mContext.getString(R.string.bluetooth_unavailable), mContext.getString(R.string.ok), null);
            }

            expressIntent(Constants.ACTION_VALUE_CONNECTION_LOST);
        }

        public void connected(BluetoothDevice device){
            if (mBaseActivity != null)
                mBaseActivity.showMessage(mContext.getString(R.string.device_connected), mContext.getString(R.string.ok), null);

            expressIntent(Constants.ACTION_VALUE_DEVICE_CONNECTED);
        }

        public void connectionFailed(BluetoothDevice device){
            if (mBaseActivity != null) {
                logger.info("BaseDevice:Bluetooth Connection Failed...device name: " + mDeviceThreadControl.mConnectedDevice.getName());
                mBaseActivity.showMessage(mContext.getString(R.string.bluetooth_unavailable), mContext.getString(R.string.ok), null);
            }

            //expressIntent(Constants.ACTION_VALUE_CONNECTION_LOST);
        }

    }
