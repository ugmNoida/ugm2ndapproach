package com.ugm.android.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Build;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BluetoothUtils {

    private static Logger logger = LoggerFactory.getLogger(BluetoothUtils.class);

    public static BluetoothSocket createRfcommSocket(BluetoothDevice device) {
        BluetoothSocket tmp = null;
        try {
            Class class1 = device.getClass();
            Class aclass[] = new Class[1];
            aclass[0] = Integer.TYPE;
            Method method = class1.getMethod("createRfcommSocket", aclass);
            Object aobj[] = new Object[1];
            aobj[0] = Integer.valueOf(1);

            tmp = (BluetoothSocket) method.invoke(device, aobj);
        } catch (NoSuchMethodException e) {
            logger.error("BluetoothSocket:createRfcommSocket() failed:"+e.toString());
        } catch (InvocationTargetException e) {
            logger.error("BluetoothSocket:createRfcommSocket() failed"+e.toString());
        } catch (IllegalAccessException e) {
            logger.error("BluetoothSocket:createRfcommSocket() failed"+e.toString());
        }
        return tmp;
    }

    public static BluetoothSocket createBTSocket(BluetoothDevice device){

        BluetoothSocket socket = null;
        try{
            socket = device.createInsecureRfcommSocketToServiceRecord(DeviceThreadControl.MY_UUID_SECURE);
        } catch (IOException e){
            logger.error("BluetoothSocket:createBTSocket() failed"+e.toString());
        } catch (Exception ex){
            logger.error("BluetoothSocket:createBTSocket() failed"+ex.toString());
        }
        return socket;
    }


    public static BluetoothAdapter getAdapter(Context context) {
        BluetoothAdapter bluetoothAdapter = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
            if (bluetoothManager != null)
                bluetoothAdapter = bluetoothManager.getAdapter();
        } else {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        return bluetoothAdapter;
    }

    public static byte[] concat(byte[] A, byte[] B) {
        byte[] C = new byte[A.length + B.length];
        System.arraycopy(A, 0, C, 0, A.length);
        System.arraycopy(B, 0, C, A.length, B.length);
        return C;
    }

}
