package com.ugm.android.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.text.TextUtils;

import com.ugm.android.ui.activities.AbstractBaseActivity;
import com.ugm.android.utilities.Constants;
import com.ugm.android.utilities.UGMUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class GPSDeviceBarefoot extends BaseDevice {

    // Debugging
    private static Logger logger = LoggerFactory.getLogger(GPSDeviceBarefoot.class);
    private static BaseDevice mGPSDeviceBarefootInstance;

    private Context mContext;
    private AbstractBaseActivity mBaseActivity;

    public static BaseDevice getInstance(Context context) {
        if (mGPSDeviceBarefootInstance == null) {
            synchronized (GPSDeviceBarefoot.class) {
                if (mGPSDeviceBarefootInstance == null) {
                    mGPSDeviceBarefootInstance = new GPSDeviceBarefoot(context);
                }
            }
        }
        return mGPSDeviceBarefootInstance;
    }

    private GPSDeviceBarefoot(Context context) {

        mDeviceThreadControl = new DeviceThreadControl(new DeviceThreadControl.IThreadCreator() {
            @Override
            public ConnectedDeviceThread Create(BluetoothSocket socket, String socketType, BluetoothDevice device) {
                return new GPSDeviceBarefoot.ConnectedGPSThread(socket, socketType, device, mDeviceThreadControl);
            }
        }, this);

        mContext = context;
        if (context instanceof AbstractBaseActivity)
            mBaseActivity = (AbstractBaseActivity) context;
    }

    /**
     * This thread runs during a connection with a remote GPS device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedGPSThread extends ConnectedDeviceThread {

        public ConnectedGPSThread(BluetoothSocket socket, String socketType, BluetoothDevice device, DeviceThreadControl threadObject) {
            super(socket, socketType, device, threadObject);
        }

        //
        //Must read line by line to maintain data integrity
        //
        protected String read() throws IOException {

            BufferedReader reader = new BufferedReader(new InputStreamReader(mmInStream));
            StringBuilder builder = new StringBuilder();
            while (reader.ready()) {
                builder.append((reader.readLine()));
            }

            return builder.toString();
        }

        //
        //Parse GPS data for coordinates and might need to call correction services
        //
        protected void parseData(String data) {
            if (!TextUtils.isEmpty(data)) {

                String[] sentences = data.split("\\$");
                try {
                    for (String sentence : sentences) {
                        if (sentence.length() <= 4) continue;

                        logger.info("GPS Sentance: " + sentence);
                        String sentencePrefix = sentence.substring(0, 5);
                        switch (sentencePrefix) {
                            case Constants.ACTION_GNGGA:
                            case Constants.ACTION_GPGGA:
                            case Constants.ACTION_GLGGA:

                                String[] words = sentence.split(",");

                                float latitude = UGMUtility.latitude2Decimal(words[2], words[3]);
                                float longitude = UGMUtility.longitude2Decimal(words[4], words[5]);

                                GPSDevice.updateUI(longitude, latitude, 0);

                                break;
                            case "GNGSA": //when correction is needed
                            case "GPGSA":
                                break;
                            default:  //do nothing
                                break;
                        }
                    }
                } catch (IndexOutOfBoundsException e) {
                }
            }
        }

    }
}
