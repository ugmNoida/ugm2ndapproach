package com.ugm.android.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;

import com.beidou.BDServer.app.InterfaceStatusChanged;
import com.beidou.BDServer.app.NTRIPTOOLConfig;
import com.beidou.BDServer.app.StatusChanged;
import com.beidou.BDServer.data.Enums;
import com.beidou.BDServer.event.SatelliteInfoChangedEventArgs;
import com.beidou.BDServer.gnss.data.CorsConnectionArgs;
import com.beidou.BDServer.gnss.data.GnssCommand;
import com.beidou.BDServer.gnss.data.GnssInfo;
import com.ugm.android.bluetooth.GNSS.GnssLocation;
import com.ugm.android.ui.activities.AbstractBaseActivity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.greenrobot.event.EventBus;

public final class GPSDeviceWithCorrection extends BaseDevice implements InterfaceStatusChanged {
    private static Logger logger = LoggerFactory.getLogger(GPSDeviceWithCorrection.class);

    private static GPSDeviceWithCorrection mGPSDeviceWithCorrection;
    private static EventBus BUS;
    private static NTRIPTOOLConfig mNTRIPTOOLConfig;
    private static GnssLocation mGnssLocation;
    private static final String BROADCAST_INTENT = "com.beidou.BDServer.COMMAND";


    public static BaseDevice getInstance(Context context) {
        if (mGPSDeviceWithCorrection == null) {
            synchronized (GPSDevice.class) {
                if (mGPSDeviceWithCorrection == null) {

                    BUS = new EventBus();

                    mNTRIPTOOLConfig = new NTRIPTOOLConfig();
                    mNTRIPTOOLConfig.initConfig(context, "config");
                    mGnssLocation = GnssLocation.getInstance(context, BUS);

                    mGPSDeviceWithCorrection = new GPSDeviceWithCorrection(context);
                }
            }
        }
        return mGPSDeviceWithCorrection;
    }

    private GPSDeviceWithCorrection(Context context) {
        mDeviceThreadControl = new DeviceThreadControl(null, this);

        mContext = context;
        if (context instanceof AbstractBaseActivity)
            mBaseActivity = (AbstractBaseActivity) context;
        //BUS.register(this);
        mNTRIPTOOLConfig.setOnStatusChanged(this);

        mGnssLocation.BUS.register(this);
        mGnssLocation.bindService();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(SatelliteInfoChangedEventArgs args) {
        try {
            if (args != null) {
                GnssInfo info = args.getInfo();
                updateUI(info);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    private void updateUI(GnssInfo info) {
        try {
            if (info == null) {
                return;
            }

            logger.info("Longitude: " + info.longitude + " Latitude: " + info.latitude);

            GPSDevice.updateUI(info.longitude, info.latitude, info.altitude);

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    //
    //InterfaceStatusChanged by correction service
    //
    public void onStatusChanged(StatusChanged statusChanged) {
        if (statusChanged == null) {
            return;
        }
        if (statusChanged.statustype == StatusChanged.StatusTYPE.ConnectSTATUS) {
            if (statusChanged.connectstatus == StatusChanged.CONNECTSTATUS.Connected) {
                logger.info("Connect log successfully");
            } else if (statusChanged.connectstatus == StatusChanged.CONNECTSTATUS.UnConnected) {
                logger.info("Connect log failed");
                connectionFailed(super.getConnectedDevice());
            }
        } else if (statusChanged.statustype == StatusChanged.StatusTYPE.DiffSTATUS) {
            if (statusChanged.diffstatus == StatusChanged.DIFFSTATUS.Logined) {
                logger.info("diff log successfully");
            } else if (statusChanged.diffstatus == StatusChanged.DIFFSTATUS.LogingFailed) {
                logger.info("diff log failed");
            }
        }
    }

    private void connectCORS() {
        if (!NTRIPTOOLConfig.getInstance().isconnect()) {
            return;
        }
        GPSDevice.CorrectionSettings settings = GPSDevice.getCorrectionSettings();

        if (settings != null) {
            Intent intent = new Intent(BROADCAST_INTENT);
            CorsConnectionArgs args = new CorsConnectionArgs()
                    .setAction(1)                       //命令类型 0：断开 1：连接
                    .setType(settings.Type)             //差分数据源链接类型 0:CORS 1:APIS 3:千寻知寸
                    .setIp(settings.IP)                 //IP地址
                    .setPort(settings.Port)             //端口
                    .setMountName(settings.DataStream)  //源名称    根据链接服务器设置类型选填
                    .setUserName(settings.UserName)     //登陆用户名 根据链接服务器设置类型选填
                    .setPassword(settings.Password);    //登陆密码   根据链接服务器设置类型选填
            intent.putExtra(GnssCommand.GNSS_COMMAND, GnssCommand.CORS)
                    .putExtra(GnssCommand.CORS, args);
            mContext.sendBroadcast(intent);
        }
    }

    private void CORSDisConnect() {
        Intent intent = new Intent(BROADCAST_INTENT);
        CorsConnectionArgs args = new CorsConnectionArgs()
                .setAction(0);           //命令类型 0：断开 1：连接
        intent.putExtra(GnssCommand.GNSS_COMMAND, GnssCommand.CORS)
                .putExtra(GnssCommand.CORS, args);
        mContext.sendBroadcast(intent);
    }

    //
    //BaseDevice Methods
    //
    @Override
    public synchronized void start() {
    }

    @Override
    public synchronized void stop() {
        this.disconnect();
    }

    @Override
    public synchronized boolean disconnect() {

        if (mNTRIPTOOLConfig.isconnect()) {
            mNTRIPTOOLConfig.disconnext();
            super.mDeviceThreadControl.mState = DeviceThreadControl.STATE_NONE;
            connectionLost(mDeviceThreadControl.mConnectedDevice);
        }
        CORSDisConnect();
        return true;
    }

    @Override
    public synchronized void connect(BluetoothDevice device, boolean secure) {
        super.mDeviceThreadControl.mConnectedDevice = device;

        try {

            if (!mNTRIPTOOLConfig.isconnect()) {
                mNTRIPTOOLConfig.initConnectSetting(Enums.ConnectionType.CONNECTION_BLUETOOTH, device.getName());
                if (mNTRIPTOOLConfig.connect()) {
                    super.mDeviceThreadControl.mState = DeviceThreadControl.STATE_CONNECTED;
                } else {
                    super.mDeviceThreadControl.mState = DeviceThreadControl.STATE_NONE;
                    connectionFailed(mDeviceThreadControl.mConnectedDevice);
                }
                ;
            }

            connectCORS();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
