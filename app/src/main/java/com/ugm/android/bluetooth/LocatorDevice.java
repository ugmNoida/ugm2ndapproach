package com.ugm.android.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.ugm.android.UGMApplication;
import com.ugm.android.ui.activities.AbstractBaseActivity;
import com.ugm.android.utilities.Constants;
import com.ugm.android.utilities.UmDataParser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public final class LocatorDevice extends BaseDevice {
    // Debugging
    private static Logger logger = LoggerFactory.getLogger(LocatorDevice.class);
    private static LocatorDevice mLocatorDeviceInstance;

    public static LocatorDevice getInstance(Context context) {
        if (mLocatorDeviceInstance == null) {
            synchronized (LocatorDevice.class) {
                if (mLocatorDeviceInstance == null) {
                    mLocatorDeviceInstance = new LocatorDevice(context);
                }
            }
        }
        return mLocatorDeviceInstance;
    }

    private LocatorDevice(Context context) {

        mDeviceThreadControl = new DeviceThreadControl(new DeviceThreadControl.IThreadCreator() {
            @Override
            public ConnectedDeviceThread Create(BluetoothSocket socket, String socketType, BluetoothDevice device) {
                return new LocatorDevice.ConnectedLocatorThread(socket, socketType, device, mDeviceThreadControl);
            }
        }, this);

        mContext = context;
        if (context instanceof AbstractBaseActivity)
            mBaseActivity = (AbstractBaseActivity) context;
    }

    /**
     * Write to the ConnectedLocatorThread in an unsynchronized manner
     *
     * @param out The bytes to write
     * @see LocatorDevice.ConnectedLocatorThread#write(byte[])
     */
    public void write(byte[] out) {
        // Create temporary object
        ConnectedDeviceThread r;
        // Synchronize a copy of the ConnectedLocatorThread
        synchronized (this) {
            if (mDeviceThreadControl.getState() != DeviceThreadControl.STATE_CONNECTED) {
                logger.info("LocatorDevice:write mState != STATE_CONNECTED write failed = " + mDeviceThreadControl.getState());
                return;
            }
            r = mDeviceThreadControl.mConnectedDeviceThread;
        }
        // Perform the write unsynchronized
        try{
            r.write(out);
        }catch (IOException e){
            logger.error("Write failed. Device name: " + mDeviceThreadControl.mConnectedDevice.getName());

            //lost connection somehow...
            mDeviceThreadControl.mState = DeviceThreadControl.STATE_NONE;
            super.connectionLost(mDeviceThreadControl.mConnectedDevice);
        }

    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    public void connectionFailed(BluetoothDevice device) {
        UGMApplication.getInstance().setCount(0);
        super.connectionFailed(device);
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    public void connectionLost(BluetoothDevice device) {
        UGMApplication.getInstance().setCount(0);
        super.connectionLost(device);
    }

    public void connected(BluetoothDevice device){
        UGMApplication.getInstance().setCount(0);
        super.connected(device);
    }

    /**
     * This thread runs during a connection with a remote locator device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedLocatorThread extends ConnectedDeviceThread {

        public ConnectedLocatorThread(BluetoothSocket socket, String socketType, BluetoothDevice device, DeviceThreadControl threadObject) {
            super(socket, socketType, device, threadObject);
        }

        //
        // locator read and write need to match out to avoid overflow or starving
        //
        protected String read() throws IOException {
            String data = super.read();
            if (data.length() > 0) {
                //UGMApplication.getInstance().subtractCount();
            }
            return data;
        }

        //
        //read and write must be balanced out
        //
        public void write(byte[] buffer) throws IOException{
            super.write(buffer);

            //UGMApplication.getInstance().addCount();
            // Share the sent message back to the UI Activity
        }

        protected void parseData(String data) {
            if (!TextUtils.isEmpty(data)) {
                String[] arrData = data.split(":");
                for (String value : arrData) {
                    if (!TextUtils.isEmpty(value)) {
                        parseDataFromUGMDevice(":" + value);
                    }
                }
            }
        }

        private void parseDataFromUGMDevice(String response) {
            //HashMap<String,Float> resulMap = new HashMap<>();
            try {
                if (!TextUtils.isEmpty(response)) {
                    UmDataParser umDataParser = new UmDataParser(response);
                    UmDataParser.UM_CommandData data = umDataParser.commandData;
                    int val1 = Integer.parseInt(data.data, 16);
                    float val;

                    switch (data.command) {
                        case Constants.DISTANCE_CMD_CODE:
                            // Step = 0.01
                            // Offset = 32000
                            val = 0.01f * (val1 - 32000);
                            //resulMap.put(Constants.DISTANCE_CMD_CODE,val);

                            Intent intent = new Intent(Constants.ACTION_CMD_VALUE_DEPTH);
                            intent.putExtra(Constants.DISTANCE_CMD_CODE, val);
                            logger.info("LocatorDevice:DISTANCE_CMD_CODE:val:" + val);
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                            break;
                        case Constants.PITCH_CMD_CODE_VALID:
                            val = val1;
                            Intent intent21 = new Intent(Constants.ACTION_CMD_VALUE_PITCH_VALID);
                            intent21.putExtra(Constants.PITCH_CMD_CODE_VALID, val);
                            logger.info("LocatorDevice:PITCH_CMD_CODE_VALID:val:" + val);
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent21);
                            break;
                        case Constants.PITCH_CMD_CODE:
                            // step = 0.1
                            // Offset = 32000
                            val = 0.1f * (val1 - 32000);
                            //resulMap.put(Constants.PITCH_CMD_CODE,val);

                            Intent intent2 = new Intent(Constants.ACTION_CMD_VALUE_PITCH);
                            intent2.putExtra(Constants.PITCH_CMD_CODE, val);
                            logger.info("LocatorDevice:PITCH_CMD_CODE:val:" + val);
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent2);
                            break;
                        case Constants.TEMPERATURE_CMD_CODE:
                        case Constants.TEMPERATURE_CMD_CODE_VALID:
                            // step = 1
                            // Offset = 100
                            val = val1 - 100;
                            //resulMap.put(Constants.TEMPERATURE_CMD_CODE,val);

                            Intent intent3 = new Intent(Constants.ACTION_CMD_VALUE_TEMP);
                            intent3.putExtra(Constants.TEMPERATURE_CMD_CODE, val);
                            logger.info("LocatorDevice:TEMPERATURE_CMD_CODE:val:" + val);
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent3);
                            break;
                        case Constants.ROLL_CMD_CODE:
                            float original = (float) val1;

                            if (original == 0)
                                val = 12;
                            else
                                val = original / 2;

                            //resulMap.put(Constants.ROLL_CMD_CODE,val);

                            Intent intent4 = new Intent(Constants.ACTION_CMD_VALUE_ROLL);
                            intent4.putExtra(Constants.ROLL_CMD_CODE, val);
                            logger.info("LocatorDevice:ROLL_CMD_CODE:val:" + val);
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent4);
                            break;
                        case Constants.ROLL_CMD_CODE_VALID:
                            val = val1;
                            Intent intent42 = new Intent(Constants.ACTION_CMD_VALUE_ROLL_VALID);
                            intent42.putExtra(Constants.ROLL_CMD_CODE_VALID, val);
                            logger.info("LocatorDevice:ROLL_CMD_CODE:val:" + val);
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent42);

                            break;
                        case Constants.BATTERY_CMD_CODE:
                            // step = 33.3
                            // offset = 0
                            val = 33.3f * val1;
                            //resulMap.put(Constants.BATTERY_CMD_CODE,val);
                            logger.info("LocatorDevice:BATTERY_CMD_CODE:val:" + val);
                            /*tv_battery_value.setText(val + " %");*/
                            break;
                    }
                }
            } catch (Exception ex) {
                logger.info("LocatorDevice:ConnectedLocatorThread:command Exception in data parsing = " + ex.toString());
            }
        }
    }
}
