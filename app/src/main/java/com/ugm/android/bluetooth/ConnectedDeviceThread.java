package com.ugm.android.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


//
//common connected thread class for any socket based connections
//
public abstract class ConnectedDeviceThread extends Thread {
    private static Logger logger = LoggerFactory.getLogger(ConnectedDeviceThread.class);

    private final BluetoothSocket mmSocket;
    protected final InputStream mmInStream;
    protected final OutputStream mmOutStream;
    private final BluetoothDevice mDevice;
    private final DeviceThreadControl mmThreadObject;

    public ConnectedDeviceThread(BluetoothSocket socket, String socketType, BluetoothDevice device, DeviceThreadControl threadObject) {
        logger.info("create  " + socketType + " device name: " + device.getName());
        mDevice = device;
        mmSocket = socket;
        mmThreadObject = threadObject;

        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        // Get the BluetoothSocket input and output streams
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
            logger.error("temp sockets not created" + e.toString());
        }

        mmInStream = tmpIn;
        mmOutStream = tmpOut;
        mmThreadObject.mConnectedDevice = mDevice;
        mmThreadObject.mState = DeviceThreadControl.STATE_CONNECTED;

        logger.info("command DEVICE CONNECTED SUCCESSFULLY ****************");
        mmThreadObject.mNotifer.connected(mmThreadObject.mConnectedDevice);
    }

    public void run() {
        logger.info("BEGIN ConnectedDeviceThread");

        // Keep listening to the InputStream while connected
        while (mmThreadObject.getState() == DeviceThreadControl.STATE_CONNECTED) {
            try {
                String data = read();
                if (data.length() > 0) {

                    logger.info("command read bytelen = " + data.length() + " , data = " + data);
                    parseData(data);
                }else{
                    try{
                        sleep(200);
                    }catch (InterruptedException e){
                        //do nothing
                        logger.info("command read/sleep " + e.toString());
                    }
                }
            } catch (IOException e) {
                logger.info("command read disconnected" + e.toString());
                mmThreadObject.mState = DeviceThreadControl.STATE_NONE;
                mmThreadObject.mNotifer.connectionLost(mmThreadObject.mConnectedDevice);
                break;
            }

        }
    }

    //
    // default simple reader but subclass can override
    //
    protected String read() throws IOException{
        byte[] buffer = new byte[1024];
        int bytes;
        // Read from the InputStream
        bytes = mmInStream.read(buffer, 0, 1024);

        String data = new String(buffer);

        return data;
    }

    protected abstract void parseData(String data);

    /**
     * Write to the connected OutStream.
     *
     * @param buffer The bytes to write
     */
    public void write(byte[] buffer) throws IOException{
        logger.info("command write payload  ..." + new String(buffer));
        mmOutStream.write(buffer);
        logger.info("command write done ...");
    }

    public void cancel() {
        try {
            if (null != mmSocket) {
                mmSocket.close();
            }
        } catch (Exception e) {
            logger.error("close() of connectToLocator socket failed" + e.toString());
        }
    }
}
