package com.ugm.android.bluetooth;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.ugm.android.utilities.Constants;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
This is a factory class to control the creation of GPS devices based on app configuration
*/
public final class GPSDevice {

    //Service types to use
    public static int CORRECTION_TYPE_CORS = 0;
    public static int CORRECTION_TYPE_APIS = 1;
    public static int CORRECTION_TYPE_QIANXUN = 3;

    //settings class to collect all settings for correction services
    public static class CorrectionSettings {
        public int Type;
        public String IP;
        public int Port;
        public String UserName;
        public String Password;
        public String DataStream;

        //default testing server settings
        public CorrectionSettings() {
            Type = CORRECTION_TYPE_CORS;
            IP = "211.144.118.5";
            Port = 2102;
            DataStream = "";
            UserName = "";
            Password = "";
        }

        ;
    }

    private static CorrectionSettings mCorrectionSettings = null;
    private static Logger logger = LoggerFactory.getLogger(GPSDevice.class);
    private static BaseDevice mGPSDeviceInstance;
    private static Context mContext;

    public static BaseDevice getInstance(Context context) {
        if (mGPSDeviceInstance == null) {
            synchronized (GPSDevice.class) {
                if (mGPSDeviceInstance == null) {
                    mContext = context;
                    if (mCorrectionSettings == null) {
                        mGPSDeviceInstance = GPSDeviceBarefoot.getInstance(context);
                    } else {
                        mGPSDeviceInstance = GPSDeviceWithCorrection.getInstance(context);
                    }
                }
            }
        }
        return mGPSDeviceInstance;
    }

    public static void setCorrectionSettings(CorrectionSettings settings) {
        mCorrectionSettings = settings;
    }

    public static CorrectionSettings getCorrectionSettings() {
        return mCorrectionSettings;
    }

    //
    // This is where UI elements gets updated
    //
    public static void updateUI(double longitude, double latitude, double altitude) {
        String strLatitude = Double.toString(latitude);
        String strLongitude = Double.toString(longitude);
        String latLong = strLatitude + "," + strLongitude;

        Intent intent = new Intent(Constants.ACTION_GPS);
        intent.putExtra(Constants.ACTION_GPS_LONGITUDE, longitude);
        intent.putExtra(Constants.ACTION_GPS_LATITUDE, latitude);
        intent.putExtra(Constants.ACTION_GPS_ALTITUDE, altitude);

        logger.info("GPSDevice: updateUI: val:" + latLong);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }
}
