package com.ugm.android.bluetooth.GNSS;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.beidou.BDServer.IGnssListener;
import com.beidou.BDServer.IGnssServiceBinder;
import com.beidou.BDServer.IReceiver;
import com.beidou.BDServer.IReceiverListener;
import com.beidou.BDServer.event.LocationChangedEventArgs;
import com.beidou.BDServer.event.ProviderDisabledEventArgs;
import com.beidou.BDServer.event.SatelliteInfoChangedEventArgs;
import com.beidou.BDServer.event.StatusChangedEventArgs;
import com.beidou.BDServer.event.error.GetCmdUpdateErrorEventArgs;
import com.beidou.BDServer.event.error.UpdateFileRecordParamsErrorEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetBaseParamsEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetCORSInfoEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetCSDDialStatusEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetCSDInfoEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetComBaudrateEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetFileRecordAutoStartEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetFileRecordParamsEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetFileRecordStatusEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetGPRSInfoEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetGPRSStatusEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetIODataEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetIOEnableEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetModemAutoDialParamsEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetModemCommunicationModeEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetModemDialStatusEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetNMEAOutputListEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetPosDataFrequencyEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetRadioChannelListEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetRadioInfoEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetRadioPowerStatusEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetReceiverInfoEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetRegCodeEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetSourceTableEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetWIFIAutoPowerOnEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetWIFIInfoEventArgs;
import com.beidou.BDServer.event.receiverd.CHCGetWIFIStatusEventArgs;
import com.beidou.BDServer.event.receiverd.GGADataEventArgs;
import com.beidou.BDServer.gnss.data.GnssInfo;
import com.beidou.BDServer.gnss.data.SatelliteInfo;
import com.beidou.BDServer.gnss.data.receiver.BaseParams;
import com.beidou.BDServer.gnss.data.receiver.BasePositionInfoArray;
import com.beidou.BDServer.gnss.data.receiver.Baudrate;
import com.beidou.BDServer.gnss.data.receiver.CorsInfo;
import com.beidou.BDServer.gnss.data.receiver.Course;
import com.beidou.BDServer.gnss.data.receiver.CsdInfo;
import com.beidou.BDServer.gnss.data.receiver.DataFrequency;
import com.beidou.BDServer.gnss.data.receiver.DataFrequencyArray;
import com.beidou.BDServer.gnss.data.receiver.DataSourceList;
import com.beidou.BDServer.gnss.data.receiver.DopsInfo;
import com.beidou.BDServer.gnss.data.receiver.EbubbleInfo;
import com.beidou.BDServer.gnss.data.receiver.EnumReceiverCmd;
import com.beidou.BDServer.gnss.data.receiver.ExpireDate;
import com.beidou.BDServer.gnss.data.receiver.FileRecordInfo;
import com.beidou.BDServer.gnss.data.receiver.FileRecordStatus;
import com.beidou.BDServer.gnss.data.receiver.GeoidModelInfo;
import com.beidou.BDServer.gnss.data.receiver.GnssDataConfigList;
import com.beidou.BDServer.gnss.data.receiver.GprsInfo;
import com.beidou.BDServer.gnss.data.receiver.MagnetometerInfo;
import com.beidou.BDServer.gnss.data.receiver.ModemBandMode;
import com.beidou.BDServer.gnss.data.receiver.ModemCommunicationMode;
import com.beidou.BDServer.gnss.data.receiver.ModemDialParams;
import com.beidou.BDServer.gnss.data.receiver.ModemDialStatus;
import com.beidou.BDServer.gnss.data.receiver.ModemSignal;
import com.beidou.BDServer.gnss.data.receiver.NetworkStatus;
import com.beidou.BDServer.gnss.data.receiver.NmeaData;
import com.beidou.BDServer.gnss.data.receiver.NoneMagneticSetParams;
import com.beidou.BDServer.gnss.data.receiver.NoneMagneticTiltInfo;
import com.beidou.BDServer.gnss.data.receiver.Position;
import com.beidou.BDServer.gnss.data.receiver.PositionInfo;
import com.beidou.BDServer.gnss.data.receiver.PowerStatus;
import com.beidou.BDServer.gnss.data.receiver.ProjectionInfo;
import com.beidou.BDServer.gnss.data.receiver.RadioChannelArray;
import com.beidou.BDServer.gnss.data.receiver.RadioChannelInspectArray;
import com.beidou.BDServer.gnss.data.receiver.RadioInfo;
import com.beidou.BDServer.gnss.data.receiver.ReceiverInfo;
import com.beidou.BDServer.gnss.data.receiver.ReceiverMode;
import com.beidou.BDServer.gnss.data.receiver.ReceiverWifiInfo;
import com.beidou.BDServer.gnss.data.receiver.SatelliteNumber;
import com.beidou.BDServer.gnss.data.receiver.TransformInfo;
import com.beidou.BDServer.gnss.data.receiver.WorkModeParams;
import com.beidou.BDServer.gnss.data.receiver.WorkModeStatus;
import com.beidou.BDServer.gnss.data.receiver.WorkWay;

import de.greenrobot.event.EventBus;

public class GnssLocation {
    private static GnssLocation instance = null;
    IGnssServiceBinder mService;
    public static final String GNSS_PACKAGE_NAME = "com.beidou.BDServer.GnssService";
    GnssInfo gnssinfo;
    private final Context mContext;
    public static EventBus BUS;

    IReceiver mReceiver;

    private GnssLocation(Context context, EventBus bus) {
        mContext = context;
        BUS = bus;
    }

    public static GnssLocation getInstance(Context context, EventBus bus) {
        if (instance == null) {
            synchronized (GnssLocation.class) {
                if (instance == null) {
                    instance = new GnssLocation(context, bus);
                }
            }
        }
        return instance;
    }

    public GnssInfo getGnssinfo() {
        return gnssinfo;
    }

    public void setGnssinfo(GnssInfo gnssinfo) {
        this.gnssinfo = gnssinfo;
    }

    public void bindService() {
        Intent intent = new Intent(GNSS_PACKAGE_NAME);
        intent.setPackage(mContext.getPackageName());
        mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    public void unBindService() {
        try {
            if (mService != null) {
                mService.removeUpdates(listener);
                mService.removeReceiverListener(mIReceiverListener);
            }
        } catch (RemoteException e) {

        }

        mContext.getApplicationContext().unbindService(mConnection);
    }

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            //KLog.d("mConnection" + "Service Connected");
            mService = IGnssServiceBinder.Stub.asInterface(service);
            if (mService == null) {
                return;
            }
            try {
                mService.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, listener);
                mService.requestReceiverListener(mIReceiverListener);
                mReceiver = IReceiver.Stub.asInterface(mService.getReceiver());
            } catch (RemoteException e) {
                e.printStackTrace();

            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            //KLog.d("mConnection" + "Service Disconnected");
            mService = null;
            mReceiver = null;
        }
    };

    IGnssListener listener = new IGnssListener.Stub() {
        @Override
        public void onLocationChanged(Location location) throws RemoteException {
            BUS.post(new LocationChangedEventArgs(location));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) throws RemoteException {
            BUS.post(new StatusChangedEventArgs(provider, status, extras));
        }

        @Override
        public void onGnssInfoChanged(GnssInfo Gnssinfo) throws RemoteException {
            setGnssinfo(Gnssinfo);
            BUS.post(new SatelliteInfoChangedEventArgs(Gnssinfo));
            if (Gnssinfo != null){
                String strTemp = String.valueOf(Gnssinfo.latitude) + ":" + String.valueOf(Gnssinfo.longitude);
                Log.d("Gnssinfo", strTemp);
            }
        }

        @Override
        public void onProviderEnabled(String provider) throws RemoteException {
            //            BUS.post(new ProviderEnabledEventArgs(provider));
        }

        @Override
        public void onProviderDisabled(String provider) throws RemoteException {
            GnssLocation.instance.setGnssinfo(new GnssInfo());
            BUS.post(new ProviderDisabledEventArgs(provider));

        }
    };

    private IReceiverListener mIReceiverListener = new IReceiverListener.Stub() {

        /***
         * 处理CHC_MESSAGE_TYPE_GNSS
         *********************************************************/
        @Override
        public void getSatelliteConstellations(int constellation) throws RemoteException {
        }

        @Override
        public void getSatelliteUsedNums(SatelliteNumber satNumbers) throws RemoteException {
        }

        @Override
        public void getSatelliteInfos(SatelliteInfo[] satelliteInfoArray) throws RemoteException {
        }


        @Override
        public void getPositionEx(PositionInfo positionInfo, Course course) throws RemoteException {
        }

        @Override
        public void getBasePosition(Position position) throws RemoteException {
        }

        @Override
        public void getGnssDops(DopsInfo info) throws RemoteException {
        }

        @Override
        public void getGpggaData(byte[] bts) throws RemoteException {
            BUS.post(new GGADataEventArgs(EnumReceiverCmd.RECEIVER_ASW_SET_GNSS_GPGGA, bts));
        }

        @Override
        public void getBaseParams(BaseParams params, WorkWay way) throws RemoteException {
            BUS.post(new CHCGetBaseParamsEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_GNSS_BASEPARAM, params, way.enumWorkWay));
        }

        @Override
        public void getIoEnable(boolean[] ioEnable) throws RemoteException {
            BUS.post(new CHCGetIOEnableEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_GNSS_IOENABLE, ioEnable));
        }

        @Override
        public void getIoData(GnssDataConfigList list) throws RemoteException {
            BUS.post(new CHCGetIODataEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_GNSS_IODATA, list));
        }

        @Override
        public void getNmeaOutputList(NmeaData[] nmeaArray) throws RemoteException {
            BUS.post(new CHCGetNMEAOutputListEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_GNSS_NMEAOUTPUTLIST, nmeaArray));
        }

        @Override
        public void getGnssPdopMask(int pdopMask) throws RemoteException {
        }

        @Override
        public void getGnssElevmask(int pdopMask) throws RemoteException {
        }

        @Override
        public void getEbubbleInfo(EbubbleInfo info) throws RemoteException {
        }

        @Override
        public void getMagnetometerInfo(MagnetometerInfo info) throws RemoteException {

        }

        @Override
        public void getBasePositionList(BasePositionInfoArray baseList) throws RemoteException {
        }

        @Override
        public void getBasePositionDifference(float diff) throws RemoteException {
        }

        @Override
        public void getTransmissionInfo(byte[] data) throws RemoteException {

        }

        @Override
        public void getPosDataFrequency(DataFrequency frequency) throws RemoteException {
            BUS.post(new CHCGetPosDataFrequencyEventArgs(
                    EnumReceiverCmd.RECEIVER_CMD_GET_POS_DATA_FREQUENCY, frequency.getEnumDataFrequency()));
        }

        /***
         * 处理CHC_MESSAGE_TYPE_SYSTEM
         *******************************************************/

        @Override
        public void getReceiverInfo(ReceiverInfo info) throws RemoteException {
            BUS.post(new CHCGetReceiverInfoEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_RECEIVER_INFO, info));
        }

        @Override
        public void getRegCode(String regCode) throws RemoteException {
            BUS.post(new CHCGetRegCodeEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_REGCODE, regCode));
        }

        @Override
        public void getFileRecordStatus(FileRecordStatus status) throws RemoteException {
            BUS.post(new CHCGetFileRecordStatusEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_FILERECORD_STATUS, status.enumFileRecordStatus));
        }

        @Override
        public void getFileRecordParams(FileRecordInfo info) throws RemoteException {
            BUS.post(new CHCGetFileRecordParamsEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_FILERECORD_PARAM, info));
        }

        @Override
        public void getFileRecordAutoStart(boolean isAuto) throws RemoteException {
            BUS.post(new CHCGetFileRecordAutoStartEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_FILERECORD_AUTO_START, isAuto));
        }

        @Override
        public void getComBaudrate(Baudrate baudrate) throws RemoteException {
            BUS.post(new CHCGetComBaudrateEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_RECEIVER_COM_BAUDRATE, baudrate.enumBaudrate));
        }

        @Override
        public void getExpireDate(ExpireDate date) throws RemoteException {
        }

        @Override
        public void getBattteyLife(int baudrate) throws RemoteException {
        }

        /***
         * 处理处理CHC_MESSAGE_TYPE_PDA
         *******************************************************/

        @Override
        public void getModemCommunicationMode(ModemCommunicationMode mode) throws RemoteException {
            BUS.post(new CHCGetModemCommunicationModeEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_MODEM_COMMUNICATION_MODE, mode.mMode));
        }

        @Override
        public void getModemDialStatus(ModemDialStatus status) throws RemoteException {
            BUS.post(new CHCGetModemDialStatusEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_MODEM_DIAL_STATUS, status));
        }

        @Override
        public void getCsdDialStatus(ModemDialStatus status) throws RemoteException {
            BUS.post(new CHCGetCSDDialStatusEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_MODEM_GSM_DIAL_STATUS, status));
        }

        @Override
        public void getCsdInfo(CsdInfo info) throws RemoteException {
            BUS.post(new CHCGetCSDInfoEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_MODEM_GSM_PARAM, info));
        }

        @Override
        public void getModemAutoDialParams(ModemDialParams params) throws RemoteException {
            BUS.post(new CHCGetModemAutoDialParamsEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_MODEM_DIAL_PARAM, params));
        }

        @Override
        public void getModemAutoDial(boolean autoDial) throws RemoteException {
        }

        @Override
        public void getModemAutoPowerOn(boolean autoPowerOn) throws RemoteException {
        }

        @Override
        public void getModemPowerStatus(PowerStatus status) throws RemoteException {
        }

        @Override
        public void getWifiAutoPowerOn(boolean auto) throws RemoteException {
            BUS.post(new CHCGetWIFIAutoPowerOnEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_WIFI_AUTO_POWERON, auto));
        }

        @Override
        public void getWifiInfo(ReceiverWifiInfo info) throws RemoteException {
            BUS.post(new CHCGetWIFIInfoEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_WIFI_PARAM, info));
        }

        @Override
        public void getWifiStatus(PowerStatus status) throws RemoteException {
            BUS.post(new CHCGetWIFIStatusEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_WIFI_STATUS, status.status));
        }

        @Override
        public void getModemSignal(ModemSignal status) throws RemoteException {
        }

        @Override
        public void getModemBandMode(ModemBandMode status) throws RemoteException {
        }

        /***
         * 处理CHC_MESSAGE_TYPE_DATALINK
         ******************************************************/

        @Override
        public void getRadioPowerStatus(PowerStatus status) throws RemoteException {
            BUS.post(new CHCGetRadioPowerStatusEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_RADIO_POWER_STATUS, status.status));
        }

        @Override
        public void getRadioChannelList(RadioChannelArray channelList) throws RemoteException {
            BUS.post(new CHCGetRadioChannelListEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_RADIO_CHANNELLIST, channelList));
        }

        @Override
        public void getRadioInfo(RadioInfo info) throws RemoteException {
            BUS.post(new CHCGetRadioInfoEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_RADIO_INFO, info));
        }

        @Override
        public void getGprsStatus(NetworkStatus status) throws RemoteException {
            BUS.post(new CHCGetGPRSStatusEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_GPRS_STATUS, status));
        }

        @Override
        public void getGprsInfo(GprsInfo info) throws RemoteException {
            BUS.post(new CHCGetGPRSInfoEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_GPRS_INFO, info));
        }

        @Override
        public void getCorsInfo(CorsInfo info) throws RemoteException {
            BUS.post(new CHCGetCORSInfoEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_CORS_INFO, info));
        }

        @Override
        public void getSourceTable(DataSourceList data) throws RemoteException {
            BUS.post(new CHCGetSourceTableEventArgs(EnumReceiverCmd.RECEIVER_ASW_GET_SOURCE_TABLE, data));
        }

        @Override
        public void getRadioAutoPowerOn(boolean autoPowerOn) throws RemoteException {
        }

        @Override
        public void getGprsLoginMdl(boolean autoPowerOn) throws RemoteException {
        }

        @Override
        public void UpdateFileRecordParamsError() throws RemoteException {
            BUS.post(new UpdateFileRecordParamsErrorEventArgs(false));
        }

        @Override
        public void GetCmdUpdateError() throws RemoteException {
            BUS.post(new GetCmdUpdateErrorEventArgs(false));
        }

        @Override
        public void getDiffDataTip() throws RemoteException {
        }

        @Override
        public void getTransformInfo(TransformInfo transformInfo) throws RemoteException {
        }

        @Override
        public void getGeoidModelInfo(GeoidModelInfo geoidModelInfo) throws RemoteException {
        }

        @Override
        public void getProjectionInfo(ProjectionInfo projectionInfo) throws RemoteException {
        }

        @Override
        public void getFileRecordFrequencyList(DataFrequencyArray dataFrequencyArray)
                throws RemoteException {
        }

        @Override
        public void getWorkModeParams(WorkModeParams workModeParams) throws RemoteException {

        }

        @Override
        public void getWorkModeStatus(WorkModeStatus workModeStatus) throws RemoteException {

        }

        @Override
        public void getStarDiffExpireDate(ExpireDate date) throws RemoteException {

        }

        @Override
        public void getRadioChannelInspectArray(RadioChannelInspectArray radioChannelInspectArray)
                throws RemoteException {

        }

        @Override
        public void getIsSupportRadioChannelInspect(boolean isNotSupport) throws RemoteException {

        }

        @Override
        public void getReceiverMode(ReceiverMode mode) throws RemoteException {
        }

        @Override
        public void getNoneMagneticTiltInfo(NoneMagneticTiltInfo info) throws RemoteException {
        }

        @Override
        public void getNoneMagneticSetParams(NoneMagneticSetParams params)
                throws RemoteException {

        }

    };

    public IReceiver getReceiver() {
        return mReceiver;
    }

    public ModemDialStatus getModemDialStatus() {
        try {
            return mReceiver.getModemDialStatus();
        } catch (RemoteException e) {
           // KLog.e(e);
            return new ModemDialStatus();
        }
    }

    public IGnssServiceBinder getService() throws NoBindGnssServiceException {
        if (mService == null) {
            throw new NoBindGnssServiceException();
        }
        return mService;
    }
}
