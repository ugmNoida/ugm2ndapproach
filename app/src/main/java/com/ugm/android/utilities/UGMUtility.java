package com.ugm.android.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;

import com.ugm.android.R;
import com.ugm.android.UGMApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class UGMUtility {

    private static Logger logger = LoggerFactory.getLogger(UGMUtility.class);
    private static SharedPreferences preferences;

    public static String covertLongToDFormattedDate(long time, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        Calendar calender = Calendar.getInstance();
        calender.setTimeInMillis(time);
        return formatter.format(calender.getTime());
    }

    public static SharedPreferences getPreference() {
        if(null == preferences) {
            UGMApplication application = UGMApplication.getInstance();
            preferences = application.getSharedPreferences(UGMMacros.MY_PREFS_FILE_NAME, MODE_PRIVATE);
        }
        return preferences;
    }

    public static void setStringPreference(String key, String value) {
        SharedPreferences.Editor editor = getPreference().edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getStringPreference(String key, String defaultVal) {
        return getPreference().getString(key, defaultVal);
    }

    public static void setBooleanPreference(String key, boolean value) {
        SharedPreferences.Editor editor = getPreference().edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static boolean getBooleanPreference(String key, boolean defaultVal) {
        return getPreference().getBoolean(key, defaultVal);
    }

    public static boolean isNetworkAvailable() {
        UGMApplication application = UGMApplication.getInstance();
        ConnectivityManager cm = (ConnectivityManager)application.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public static boolean isValidString(String value) {
        if(null != value && 0 < value.trim().length()) {
            return true;
        }
        return false;
    }

    public static boolean isValidIntegerString(String value) {
        if(null != value && 0 < value.trim().length()) {
            try {
                Integer.parseInt(value);
                return true;
            } catch(Exception ex){
                logger.error("UGMUtility:isValidIntegerString:Exception:" + ex.toString());
            }
        }
        return false;
    }

    public static boolean isValidFloatString(String value) {
        if(null != value && 0 < value.trim().length()) {
            try {
                Float.parseFloat(value);
                return true;
            } catch(Exception ex){
                logger.error("UGMUtility:isValidFloatString:Exception:" + ex.toString());
            }
        }
        return false;
    }

    public static boolean isValidDoubleString(String value) {
        if(null != value && 0 < value.trim().length()) {
            try {
                Double.parseDouble(value);
                return true;
            } catch(Exception ex){
                logger.error("UGMUtility:isValidDoubleString:Exception:" + ex.toString());
            }
        }
        return false;
    }

    public static String getCountry(Context context) {
        String locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = context.getResources().getConfiguration().getLocales().get(0).getCountry();
        } else {
            locale = context.getResources().getConfiguration().locale.getCountry();
        }
        return locale;
    }

    public static String getCurrentLocaleCountry(Context context) {
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            locale = context.getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = context.getResources().getConfiguration().locale;
        }
        if(null != locale && null != locale.getCountry()) {
            return locale.getCountry();
        }
        return UGMMacros.LOCALE_COUNTRY_CODE_IN;
    }

    public static String getDefaultDepth(boolean isMeter) {
        if(isMeter) {
            return "(" + UGMApplication.getInstance().getString(R.string.meter_m1) + ")";
        } else {
            return "(" + UGMApplication.getInstance().getString(R.string.feet_ft1) + ")";
        }
    }

    public static String getDefaultPitch(boolean isPercentage) {
        if(isPercentage) {
            return "(%)";
        } else {
            return "(\u00b0)";
        }
    }

    public static boolean isGoogleMapEnabled(){
        boolean mapF = false;

        if(UGMUtility.getBooleanPreference(UGMMacros.PREF_TRACK_LOCATION, false) == true &&
                UGMUtility.getStringPreference(UGMMacros.PREF_LOCATION, "").equalsIgnoreCase(UGMMacros.VALUE_GPS_LOCATION)){
            mapF = true;
        } else {
            mapF = false;
        }
        return mapF;
    }

    public static String floatFormat(float value) {
        DecimalFormatSymbols decimalSymbols = DecimalFormatSymbols.getInstance();
        decimalSymbols.setDecimalSeparator('.');
        return new DecimalFormat("0.00", decimalSymbols).format(value);
    }

    public static String getCurrentDateInFormat(String dateFormat) {
        String currentDateString = null;
        try {
            Date currentDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            currentDateString = sdf.format(currentDate);
        } catch (Exception e) {
            //logger.error("Couldn't get date in specified format:" + dateFormat, e);
        }
        return currentDateString;
    }

   public static float longitude2Decimal(String lon, String WE) {
        if (TextUtils.isEmpty(lon)||TextUtils.isEmpty(WE)){
            return 0;
        }
        float med = Float.parseFloat(lon.substring(3))/60.0f;
        med +=  Float.parseFloat(lon.substring(0, 3));
        if(WE.startsWith("W")) {
            med = -med;
        }
        return med;
    }

    public static float latitude2Decimal(String lat, String NS) {
        if (TextUtils.isEmpty(lat)||TextUtils.isEmpty(NS)){
            return 0;
        }
        float med = Float.parseFloat(lat.substring(2))/60.0f;
        med +=  Float.parseFloat(lat.substring(0, 2));
        if(NS.startsWith("S")) {
            med = -med;
        }
        return med;
    }

    public static String getCurrentTime() {
        //date output format
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }// end of getCurrentTime()
}