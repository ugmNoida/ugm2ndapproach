package com.ugm.android.utilities;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

    private static Retrofit retrofit;
    private static final String STAGING_BASE_URL = "https://api-staging.ummaps.com/";
    private static final String PRODUCTION_BASE_URL = "https://api.ummaps.com/";

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            synchronized (RetrofitClientInstance.class) {
                if(null == retrofit) {
                    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                    logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                    httpClient.addInterceptor(logging);
                    String baseURL = STAGING_BASE_URL;
                    if(UGMUtility.getBooleanPreference(UGMMacros.PREFS_IS_PRODUCTION, false)) {
                        baseURL = PRODUCTION_BASE_URL;
                    }
                    retrofit = new retrofit2.Retrofit.Builder()
                            .baseUrl(baseURL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(httpClient.build())
                            .build();
                }
            }
        }
        return retrofit;
    }

    public static void clearAllObjects() {
        retrofit = null;
    }

}
