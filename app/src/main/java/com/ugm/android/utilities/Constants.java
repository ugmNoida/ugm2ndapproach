package com.ugm.android.utilities;

/**
 * Defines several constants used between {@link BluetoothCommService} and the UI.
 */
public interface Constants {

    // Message types sent from the BluetoothCommService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothCommService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String DEVICE_ADDRESS = "device_address";
    public static final String TOAST = "toast";

    // CMD IDS
    public static final String DISTANCE_CMD = ":08*R142002877C";
    public static final String PITCH_CMD = ":08*R142402C7F8";
    public static final String PITCH_VALID_CMD = ":08*R141202E7BA";
    public static final String TEMPERATURE_CMD = ":08*R142602E7BA";
    public static final String BATTERY_CMD = ":08*R1428020674";
    public static final String SW_MAJOR_VERSION_CMD = ":08*R1004017D59";
    public static final String SIGNAL_STRENGTH_CMD = ":08*R1440022DDA";
    public static final String SIGNAL_TO_NOISE_CMD = ":08*R1442020D98";
    public static final String ROLL_CMD = ":08*R142202A73E";

    // CMD CODES
    public static final String DISTANCE_CMD_CODE = "1420";
    public static final String PITCH_CMD_CODE = "1424";
    public static final String PITCH_CMD_CODE_VALID = "1412";
    public static final String TEMPERATURE_CMD_CODE = "1426";
    public static final String TEMPERATURE_CMD_CODE_VALID = "1413";
    public static final String BATTERY_CMD_CODE = "1428";
    public static final String ROLL_CMD_CODE = "1422";
    public static final String ROLL_CMD_CODE_VALID = "1411";
    public static final String SIGNAL_STRENGTH_CMD_CODE = "1440";
    public static final String SW_MAJOR_CMD_CODE = "1004";
    public static final String SR_NO_CMD_CODE = "1000";


    // action filters
    public static final String ACTION_CMD_VALUE_PITCH = "action_cmd_value_pitch";
    public static final String ACTION_CMD_VALUE_PITCH_VALID = "action_cmd_value_pitch_valid";
    public static final String ACTION_CMD_VALUE_TEMP = "action_cmd_value_temp";
    public static final String ACTION_CMD_VALUE_DEPTH = "action_cmd_value_depth";
    public static final String ACTION_CMD_VALUE_ROLL = "action_cmd_value_roll";
    public static final String ACTION_CMD_VALUE_ROLL_VALID = "action_cmd_value_roll_valid";
    public static final String ACTION_VALUE_DEVICE_CONNECTED = "action_value_device_connected";
    public static final String ACTION_VALUE_CONNECTION_LOST = "action_value_connection_lost";
    public static final String ACTION_JOB_SYNCED = "action_value_job_synced";
    public static final String ACTION_RECORD_SYNCED = "action_value_record_synced";
    public static final String ACTION_TX_DATA_AVAILABLE = "action_tx_data_available";

    public static final String ACTION_GPS = "action_gps";
    public static final String ACTION_GPS_LONGITUDE = "action_gps_longitude";
    public static final String ACTION_GPS_LATITUDE = "action_gps_latitude";
    public static final String ACTION_GPS_ALTITUDE = "action_gps_altitude";
    public static final String DISTANCE_GPS_CODE = "1487";
    String ACTION_GPGGA = "GPGGA";
    String ACTION_GNGGA = "GNGGA";
    String ACTION_GLGGA = "GLGGA";
}
