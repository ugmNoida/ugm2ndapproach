package com.ugm.android.utilities;

public final class UmDataParser {

    public UM_CommandData commandData;

    public UmDataParser(String data) {
        commandData = parseRawData(data);
    }

    private UM_CommandData parseRawData(String packet) {
        if (!packet.startsWith(":"))
            return null;
        try {
            // Remove the start of packet
            packet = packet.substring(1);

            // Get the Packet Length
            UM_CommandData dataPacket = new UM_CommandData();
            String packetLength = packet.substring(0, 2);
            dataPacket.packetLength = Integer.parseInt(packetLength, 16);
            packet = packet.substring(2);


            //Get Command Flag
            dataPacket.commandFlag = packet.substring(0, 1);
            packet = packet.substring(1);

            // Get Command Code
            dataPacket.commandCode = packet.substring(0, 1);
            packet = packet.substring(1);

            // Get the Command
            dataPacket.command = packet.substring(0, 4);
            packet = packet.substring(4);

            // Get Block Length
            String blockLength = packet.substring(0, 2);
            dataPacket.blockLength = Integer.parseInt(blockLength, 16);
            packet = packet.substring(2);

            // Get Data
            String data = packet.substring(0, 2 * dataPacket.blockLength );

            dataPacket.data = data;
            return dataPacket;
        } catch (Exception ignore){
            return null;
        }

    }

    public class UM_CommandData {
        public int packetLength;
        public String commandFlag;
        public String commandCode;
        public String command;
        public int blockLength;
        public String data;
        public String dataString;
    }
}