package com.ugm.android.utilities;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.LogcatAppender;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import ch.qos.logback.core.util.StatusPrinter;

public class LogbackConfigure {

    public static final String TAG = "LogbackConfigure";

    private boolean isFileLoggerEnabled = false;
    private static LogbackConfigure logbackConfigure;
    private Context context;

    public static LogbackConfigure getInstance(Context context) {
        if(null == logbackConfigure) {
            synchronized (LogbackConfigure.class) {
                if(null == logbackConfigure) {
                    logbackConfigure = new LogbackConfigure(context);
                }
            }
        }
        return logbackConfigure;
    }

    private LogbackConfigure(Context context) {
        this.context = context;
    }

    private String getLoggerDirAbsolutePath(Context context) {
        return context.getFilesDir().getAbsolutePath() + File.separator + UGMMacros.FOLDERNAME_LOGS;
    }

    public void configureLogback(boolean rollingFileAppend, Level logLevel, int maxArchives) {
        if (!isFileLoggerEnabled) {
            // reset the default context (which may already have been initialized)
            // since we want to reconfigure it
            LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
            loggerContext.reset();

            PatternLayoutEncoder logcatEncoder = new PatternLayoutEncoder();
            logcatEncoder.setContext(loggerContext);
            logcatEncoder.setPattern(UGMMacros.FILE_LOGGING_PATTERN);
            logcatEncoder.start();

            // add the newly created appenders to the root logger;
            LogcatAppender logcatAppender = getLogcatAppender(loggerContext, logcatEncoder);

            // backup the newly created appenders to the root logger;
            // qualify Logger to disambiguate from org.slf4j.Logger
            ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
            root.addAppender(logcatAppender);
            root.setLevel(logLevel); //Level.ALL

            Appender fileAppender = getFileAppender(loggerContext, logcatEncoder, rollingFileAppend, maxArchives);
            if (fileAppender != null) {
                root.addAppender(fileAppender);
                isFileLoggerEnabled = true;
            }

            StatusPrinter.print(loggerContext);
        } else {
            //Logger is already enabled with file logging. No need to reConfigure logger.
        }
    }

    private LogcatAppender getLogcatAppender(LoggerContext loggerContext, PatternLayoutEncoder logcatEncoder) {
        LogcatAppender logcatAppender = new LogcatAppender();
        logcatAppender.setContext(loggerContext);
        logcatAppender.setEncoder(logcatEncoder);
        logcatAppender.start();
        return logcatAppender;
    }

    private Appender getFileAppender(LoggerContext loggerContext, PatternLayoutEncoder logcatEncoder, boolean rollingFileAppend, int maxArchives) {
        try {
            FileAppender<ILoggingEvent> fileAppender = null;
            RollingFileAppender<ILoggingEvent> rollingFileAppender = null;
            String logDirAbsolutePath = getLoggerDirAbsolutePath(context);
            if (logDirAbsolutePath != null) {
                String logFileAbsolutePath = logDirAbsolutePath + File.separator + UGMMacros.FILENAME_LOGS;
                String logRollingFileAbsPath = logDirAbsolutePath + File.separator + UGMMacros.FILENAME_ROLLING_LOGS;
                if (rollingFileAppend) {
                    rollingFileAppender = getRollingFileAppender(loggerContext, logcatEncoder, logRollingFileAbsPath, maxArchives);
                    return rollingFileAppender;
                } else {
                    fileAppender = getFileAppender(loggerContext, logcatEncoder, logFileAbsolutePath);
                    return fileAppender;
                }
            } else {
                Log.e(TAG, "logDirAbsolutePath is null");
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception in configureFileLogging", e);
        }
        return null;
    }

    private static RollingFileAppender<ILoggingEvent> getRollingFileAppender(LoggerContext loggerContext, PatternLayoutEncoder rollingEncoder, String logRollingFileAbsPath, int maxArchives) {
        // setup RollingFileAppender
        RollingFileAppender<ILoggingEvent> rollingFileAppender = new RollingFileAppender<ILoggingEvent>();
        rollingFileAppender.setAppend(true);
        rollingFileAppender.setContext(loggerContext);

        TimeBasedRollingPolicy<ILoggingEvent> rollingPolicy = new TimeBasedRollingPolicy<ILoggingEvent>();
        rollingPolicy.setFileNamePattern(logRollingFileAbsPath);
        rollingPolicy.setMaxHistory(maxArchives);
        rollingPolicy.setParent(rollingFileAppender); // parent and context required!
        rollingPolicy.setContext(loggerContext);
        rollingPolicy.start();

        rollingFileAppender.setRollingPolicy(rollingPolicy);

        rollingFileAppender.setEncoder(rollingEncoder);
        rollingFileAppender.start();
        return rollingFileAppender;
    }

    private FileAppender<ILoggingEvent> getFileAppender(LoggerContext loggerContext, PatternLayoutEncoder logcatEncoder, String logFileAbsolutePath) {
        FileAppender<ILoggingEvent> fileAppender = new FileAppender<ILoggingEvent>();
        fileAppender.setContext(loggerContext);
        fileAppender.setFile(logFileAbsolutePath);
        fileAppender.setEncoder(logcatEncoder);
        fileAppender.start();
        return fileAppender;
    }

    public void deleteLogFiles() {
        String logDirAbsolutePath = getLoggerDirAbsolutePath(context);
        File logsDir = new File(logDirAbsolutePath);
        if (logsDir != null && logsDir.isDirectory()) {
            File logFiles[] = logsDir.listFiles();
            for (File logFile : logFiles) {
                // delete archived log files
                logFile.delete();
            }
        }
    }

    public File getZipLogFolder() {
        File logFiles = null;
        try {
            String logDirAbsolutePath = getLoggerDirAbsolutePath(context);
            File logFilesDir = new File(logDirAbsolutePath);
            File rootFolder = context.getExternalFilesDir(Environment.MEDIA_SHARED);
            File pdfFolder = new File(rootFolder, "UGMLogs");
            if (!pdfFolder.exists()) {
                pdfFolder.mkdirs();
            }
            logFiles = new File(rootFolder, "logs");
            logFiles.deleteOnExit();
            logFiles.createNewFile();
            ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(logFiles.getAbsolutePath()));
            zipOutputStream = zipMultipleFiles(logFilesDir.listFiles(), zipOutputStream);
            zipOutputStream.close();
        } catch (Exception ex) {
            //logger.error("Exception while zipping log files & database file", ex);
        }
        return logFiles;
    }

    private ZipOutputStream zipMultipleFiles(File[] sourceFiles, ZipOutputStream destZipOutputStream) throws IOException {
        for (File eachSourceFile : sourceFiles) {
            if (eachSourceFile.exists()) {
                if (eachSourceFile.isDirectory()) {
                    destZipOutputStream = zipMultipleFiles(eachSourceFile.listFiles(), destZipOutputStream);
                    continue;
                }
                ZipEntry entries = new ZipEntry(eachSourceFile.getPath());
                destZipOutputStream.putNextEntry(entries);

                FileInputStream fileInputStream = new FileInputStream(eachSourceFile);
                int bufferSize = 2048;
                byte[] buffer = new byte[bufferSize];
                int count;
                while ((count = fileInputStream.read(buffer)) != -1) {
                    destZipOutputStream.write(buffer, 0, count);
                }
                fileInputStream.close();
                destZipOutputStream.closeEntry();
            }
        }
        return destZipOutputStream;
    }
}
