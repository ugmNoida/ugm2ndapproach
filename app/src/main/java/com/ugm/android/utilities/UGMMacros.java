package com.ugm.android.utilities;

public interface UGMMacros {

    String UGM_PREFIX = "@";
    String FORMATTER1 = "dd MMMM yyyy, hh:mm:ss a";
    String FORMATTER2 = "dd MMM yy, hh:mm:ss a";

    int REQUEST_CODE_GPS_LOCATION_SETTINGS = 500;
    int REQUEST_CODE_PERMISSION_LOCATION = 501;

    //User Status
    String USER_STATUS_CREATED = "00";
    String USER_STATUS_LOGIN_SESSION = "10";
    String USER_STATUS_LOGOUT_SESSION = "90";
    String USER_STATUS_DELETED = "99";
    //User Roles
    String USER_ROLE_ADMIN = "90";
    String USER_ROLE_USER = "10";
    //JobData Status
    String JOB_CREATED = "00";
    String JOB_INPROGRESS = "10";
    String JOB_COMPLETED = "90";
    String JOB_SOFT_DELETED = "99";
    String JOB_HARD_DELETED = "100";
    //Record Status
    String RECORD_CREATED = "00";
    String RECORD_EDITED = "50";
    String RECORD_DELETED = "99";

    String FOLDERNAME_LOGS = "logs";
    String LOGCAT_LOG_PATTERN = "[%thread] %msg%n";
    String FILE_LOGGING_PATTERN = "%d{HH:mm:ss.SSS} %-5level %logger{36} - %msg%n";
    String DAY_PATTERN = "yyyy-MM-dd";
    String HOUR_PATTERN = "yyyy-MM-dd_HH";
    String MINUTE_PATTERN = "yyyy-MM-dd_HH-mm";

    String LOG_DATE_SEPARATOR = "_";
    String ACTIVE_PATTERN = DAY_PATTERN;
    String LOG_EXTENSION = ".log";
    String LOG_ARCHIVE_EXTENSION = ".zip";
    String LOGFILE_BASENAME = "UGMLOG";

    String FILENAME_LOGS = LOGFILE_BASENAME + LOG_EXTENSION;
    String FILENAME_ROLLING_LOGS = LOGFILE_BASENAME + LOG_DATE_SEPARATOR + "%d{" + ACTIVE_PATTERN + "}" + LOG_ARCHIVE_EXTENSION;

    String FILENAME_CURRENT_DAY_LOGS = LOGFILE_BASENAME + LOG_DATE_SEPARATOR + "%s";
    String FILENAME_LOGS_ARCHIVE = LOGFILE_BASENAME + LOG_DATE_SEPARATOR + "%s" + LOG_ARCHIVE_EXTENSION;
    String LOG_RESET_MESSAGE = "Log File Reset.";

    String MY_PREFS_FILE_NAME = "UGM";
    String PREFS_FULL_NAME = "FullName";
    String PREFS_LOGGEDIN_EMAIL = "LoggedInUserEmail";
    String PREFS_LOGGEDIN_USERID = "LoggedInUserId";
    String PREFS_LOGGEDIN_SESSION_TOKEN = "LoggedInSessionToken";
    String PREFS_IS_PRODUCTION = "IsProduction";

    String PREFS_PITCH = "PrefPitch";
    String PREFS_DEPTH = "PrefDepth";
    String PREF_TRACK_LOCATION = "PrefTrackLocation";
    String PREF_LOCATION = "PrefLocation";

    String VALUE_DEPTH_METER = "Meter";
    String VALUE_DEPTH_FEET = "Feet";
    String VALUE_PITCH_PERCENTAGE = "Percentage";
    String VALUE_PITCH_DEGREE = "Degree";
    String VALUE_GPS_LOCATION = "Gps";
    String VALUE_GARMIN_LOCAION = "Garmin";
    String VALUE_ONLINE_SUPPORT = "OnlineSupport";

    String LOCALE_COUNTRY_CODE_IN = "IN";
    String LOCALE_COUNTRY_CODE_US = "US";

    String EXPIRED_TOKEN = "Expired token";

    String BAD_ELF = "badElfDevice";
    String MAG_DEVICE = "magDevice";

    String LAT_LONG = "latLong";
    String MAG_NAME_1="RX";
    String MAG_NAME_2="Receiver";

    String IS_LAT_LONG_SAVE="latLongSave";

    String GPS_DEVICE = "Bad Elf";
}
