package com.ugm.android.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.ugm.android.UGMApplication;
import com.ugm.android.database.UGMDatabase;
import com.ugm.android.utilities.UGMMacros;

import java.io.Serializable;
import java.util.Calendar;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = UGMDatabase.RECORD_TABLE, indices = {@Index(value = {"JobId", "RodNumber"}, unique = true)})
public class Record implements Serializable, Comparable<Record> {
    public static final String JOB_ID = "JobId";
    public static final String RECORD_ID = "RecordId";
    public static final String ROD_NUMBER = "RodNumber";
    public static final String ROD_LENGTH = "RodLength";
    public static final String TEMPERATURE = "Temperature";
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String PITCH = "Pitch";
    public static final String DEPTH = "Depth";
    public static final String ROLL = "Role";
    public static final String STATUS = "Status";
    public static final String ONLY_PITCH = "OnlyPitch";
    public static final String IS_EDITED = "IsEdited";
    public static final String CREATED_TIME = "CreatedTime";
    public static final String UPDATED_TIME = "UpdatedTime";
    public static final String IS_SYNCED = "IsSynced";
    public static final String RELATIVE_ELEVATION = "RelativeElevation";

    @NonNull
    @PrimaryKey
    @ColumnInfo(index = true, name = RECORD_ID)
    private String recordId;
    @NonNull
    @ColumnInfo(index = true, name = JOB_ID)
    @ForeignKey(entity = Job.class, parentColumns = Job.JOB_ID, childColumns = JOB_ID, onDelete = CASCADE)
    private String jobId;
    //this will be unique across the JobData. For e.g : 1, 2, 3, ... etc.
    @ColumnInfo(name = ROD_NUMBER)
    private int rodNumber;
    @ColumnInfo(name = ROD_LENGTH)
    private String rodLength;
    @ColumnInfo(name = TEMPERATURE)
    private String temperature;
    @ColumnInfo(name = LATITUDE)
    private String latitude;
    @ColumnInfo(name = LONGITUDE)
    private String longitude;
    @ColumnInfo(name = PITCH)
    private String pitch;
    @ColumnInfo(name = DEPTH)
    private String depth;
    @ColumnInfo(name = ROLL)
    private String roll;
    @ColumnInfo(name = ONLY_PITCH)
    private boolean onlyPitch;
    @ColumnInfo(name = IS_EDITED)
    private boolean isEdited;
    // Status maybe Created:00, Edited:50
    @ColumnInfo(name = STATUS)
    private String status;
    @ColumnInfo(name = CREATED_TIME)
    private long createdTime;
    @ColumnInfo(name = UPDATED_TIME)
    private long updatedTime;
    @ColumnInfo(name = IS_SYNCED)
    private boolean isSynced;
    @ColumnInfo(name = RELATIVE_ELEVATION)
    private String relativeElevation;

    public Record() {
        // Default constructor required for calls to DataSnapshot.getValue(Record.class)
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    @NonNull
    public String getJobId() {
        return jobId;
    }

    public void setJobId(@NonNull String jobId) {
        this.jobId = jobId;
    }

    public int getRodNumber() {
        return rodNumber;
    }

    public void setRodNumber(int rodNumber) {
        this.rodNumber = rodNumber;
    }

    public String getRodLength() {
        return rodLength;
    }

    public void setRodLength(String rodLength) {
        this.rodLength = rodLength;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPitch() {
        return pitch;
    }

    public void setPitch(String pitch) {
        this.pitch = pitch;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getRoll() {
        return roll;
    }

    public void setRoll(String roll) {
        this.roll = roll;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
    }

    public boolean isSynced() {
        return isSynced;
    }

    public void setSynced(boolean synced) {
        isSynced = synced;
    }

    public boolean isOnlyPitch() {
        return onlyPitch;
    }

    public void setOnlyPitch(boolean onlyPitch) {
        this.onlyPitch = onlyPitch;
    }

    public boolean isEdited() {
        return isEdited;
    }

    public void setEdited(boolean edited) {
        isEdited = edited;
    }

    public String getRelativeElevation() {
        return relativeElevation;
    }

    public void setRelativeElevation(String relativeElevation) {
        this.relativeElevation = relativeElevation;
    }

    @Override
    public int compareTo(@NonNull Record record) {
        if(this.rodNumber > record.getRodNumber()) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        return "Record{" +
                "recordId='" + recordId + '\'' +
                ", jobId='" + jobId + '\'' +
                ", rodNumber=" + rodNumber +
                ", rodLength='" + rodLength + '\'' +
                ", pitch='" + pitch + '\'' +
                ", depth='" + depth + '\'' +
                ", onlyPitch=" + onlyPitch +
                ", createdTime=" + createdTime +
                '}';
    }

    public static Record createRecord(@NonNull String jobId, int rodNumber, String rodLength, String temperature,
                                      String latitude, String longitude, String pitch, String depth, String roll, boolean onlyPitch) {
        Record record = new Record();
        record.setRecordId(UGMApplication.getUUID());
        record.setJobId(jobId);
        record.setRodNumber(rodNumber);
        record.setRodLength(rodLength);
        record.setTemperature(temperature);
        record.setLatitude(latitude);
        record.setLongitude(longitude);
        record.setPitch(pitch);
        record.setDepth(depth);
        record.setRoll(roll);
        record.setOnlyPitch(onlyPitch);
        record.setStatus(UGMMacros.RECORD_CREATED);
        record.setSynced(false);
        record.setEdited(false);
        long time = Calendar.getInstance().getTimeInMillis();
        record.setCreatedTime(time);
        record.setUpdatedTime(time);
        return record;
    }
}
