package com.ugm.android.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.ugm.android.database.UGMDatabase;
import com.ugm.android.utilities.UGMMacros;

import java.util.Calendar;

@Entity(tableName = UGMDatabase.USER_TABLE, indices = {@Index(value = {"EmailId", "SerialNumber"}, unique = true)})
public class User {
    public static final String USER_ID = "UserId";
    public static final String EMAIL_ID = "EmailId";
    public static final String SERIAL_NUMBER = "SerialNumber";
    public static final String COUNTRY_NAME = "CountryName";
    public static final String COMPANY_NAME = "CompanyName";
    public static final String CONTACT_NUMBER = "ContactNumber";
    public static final String DEVICE_NAME = "DeviceName";
    public static final String STATUS = "Status";
    public static final String ROLE = "Role";
    public static final String CREATED_TIME = "CreatedTime";
    public static final String UPDATED_TIME = "UpdatedTime";

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = USER_ID)
    private String userId;
    @NonNull
    @ColumnInfo(name = EMAIL_ID)
    private String emailId;
    @NonNull
    @ColumnInfo(name = SERIAL_NUMBER)
    private String serialNumber;
    @ColumnInfo(name = COUNTRY_NAME)
    private String countryName;
    @ColumnInfo(name = COMPANY_NAME)
    private String companyName;
    @ColumnInfo(name = CONTACT_NUMBER)
    private String contactNumber;
    @ColumnInfo(name = DEVICE_NAME)
    private String deviceName;
    //Status maybe Created:00, LoginSession:10, LogoutSession:90, Deleted:99
    @ColumnInfo(name = STATUS)
    private String status;
    //Role may be Admin:90, User:10
    @ColumnInfo(name = ROLE)
    private String role;
    @ColumnInfo(name = CREATED_TIME)
    private long createdTime;
    @ColumnInfo(name = UPDATED_TIME)
    private long updatedTime;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    @NonNull
    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(@NonNull String emailId) {
        this.emailId = emailId;
    }

    @NonNull
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(@NonNull String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
    }

    public static User createUser(@NonNull String emailId, @NonNull String serialNumber, String countryName, String companyName,
                                  String contactNumber, String deviceName, boolean isAdmin) {
        User user = new User();
        user.setCompanyName(companyName);
        user.setContactNumber(contactNumber);
        user.setCountryName(countryName);
        user.setDeviceName(deviceName);
        user.setEmailId(emailId);
        user.setSerialNumber(serialNumber);
        user.setStatus(UGMMacros.USER_STATUS_CREATED);
        if(isAdmin) {
            user.setRole(UGMMacros.USER_ROLE_ADMIN);
        } else {
            user.setRole(UGMMacros.USER_ROLE_USER);
        }
        long time = Calendar.getInstance().getTimeInMillis();
        user.setCreatedTime(time);
        user.setUpdatedTime(time);
        return user;
    }
}
