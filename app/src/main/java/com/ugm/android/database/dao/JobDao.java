package com.ugm.android.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.ugm.android.database.entity.Job;

import java.util.List;

@Dao
public abstract class JobDao implements BaseDao<Job> {

    @Query("SELECT * FROM JobData")
    public abstract List<Job> getAllJobs();

    @Query("SELECT * FROM JobData WHERE JobData.UserId=:userId")
    public abstract List<Job> getAllJobsByUserId(String userId);

    @Query("SELECT * FROM JobData WHERE JobData.UserId=:userId AND JobData.Status IN (:statuses) ORDER BY JobData.UpdatedTime DESC")
    public abstract List<Job> getJobsByUserId(String userId, List<String> statuses);

    @Query("SELECT * FROM JobData WHERE JobData.JobId=:jobId")
    public abstract Job getJobByJobId(String jobId);

    @Query("SELECT * FROM JobData WHERE JobData.UserId=:userId AND JobData.jobName=:jobName") //GLOB '*' ||:jobName|| '*'
    public abstract List<Job> getJobsByUserIdAndJobName(String userId, String jobName);

    @Query("SELECT * FROM JobData WHERE JobData.UserId=:userId AND JobData.jobName LIKE '%'||:searchTxt||'%' AND JobData.Status IN (:statuses) ORDER BY JobData.UpdatedTime DESC")
    public abstract List<Job> getJobsByJobNameSearch(String userId, String searchTxt, List<String> statuses);

    @Query("SELECT * FROM JobData WHERE JobData.UserId=:userId AND JobData.IsSynced=:isSynced")
    public abstract List<Job> getJobsByUserIdAndSyncStatus(String userId, boolean isSynced);

    @Query("SELECT * FROM JobData WHERE JobData.UserId=:userId AND JobData.Status=:status ORDER BY JobData.UpdatedTime DESC")
    public abstract List<Job> getJobsByUserIdAndStatus(String userId, String status);
}
