package com.ugm.android.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.ugm.android.UGMApplication;
import com.ugm.android.database.UGMDatabase;
import com.ugm.android.utilities.UGMMacros;

import java.io.Serializable;
import java.util.Calendar;

@Entity(tableName = UGMDatabase.JOB_TABLE, indices = {@Index(value = {"UserId", "JobName"}, unique = true)})
public class Job implements Serializable, Comparable<Job> {
    public static final String USER_ID = "UserId";
    public static final String JOB_ID = "JobId";
    public static final String JOB_NAME = "JobName";
    public static final String LOCATION_NAME = "LocationName";
    public static final String COMPANY_NAME = "CompanyName";
    public static final String CLIENT_NAME = "ClientName";
    public static final String JOB_DESCRIPTION = "JobDescription";
    public static final String FIRST_ROD_LENGTH = "FirstRodLength";
    public static final String DEFAULT_ROD_LENGTH = "DefaultRodLength";
    public static final String CREATED_TIME = "CreatedTime";
    public static final String UPDATED_TIME = "UpdatedTime";
    public static final String JOB_STATUS = "Status";
    public static final String JOB_IS_SYNCED = "IsSynced";
    public static final String PREF_DEPTH = "PrefDepth";
    public static final String PREF_PITCH = "PrefPitch";

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = JOB_ID)
    private String jobId;
    @NonNull
    @ColumnInfo(name = USER_ID)
    //@ForeignKey(entity = User.class, parentColumns = User.USER_ID, childColumns = USER_ID, onDelete = CASCADE)
    private String userId;
    // JobData name will be unique across the userId. We can not create same job name in the logged in UserId.
    @ColumnInfo(name = JOB_NAME)
    private String jobName;
    @ColumnInfo(name = LOCATION_NAME)
    private String locationName;
    @ColumnInfo(name = COMPANY_NAME)
    private String companyName;
    @ColumnInfo(name = CLIENT_NAME)
    private String clientName;
    @ColumnInfo(name = JOB_DESCRIPTION)
    private String jobDescription;
    @ColumnInfo(name = FIRST_ROD_LENGTH)
    private String firstRodLength;
    @ColumnInfo(name = DEFAULT_ROD_LENGTH)
    private String defaultRodLength;
    @ColumnInfo(name = CREATED_TIME)
    private long createdTime;
    @ColumnInfo(name = UPDATED_TIME)
    private long updatedTime;
    // Status maybe Created:00, InProgress:10, Completed:90, Deleted:99
    @ColumnInfo(name = JOB_STATUS)
    private String status;
    @ColumnInfo(name = JOB_IS_SYNCED)
    private boolean isSynced;
    @ColumnInfo(name = PREF_DEPTH)
    private String prefDepth;
    @ColumnInfo(name = PREF_PITCH)
    private String prefPitch;

    public Job() {
        // Default constructor required for calls to DataSnapshot.getValue(JobData.class)
    }

    @NonNull
    public String getJobId() {
        return jobId;
    }

    public void setJobId(@NonNull String jobId) {
        this.jobId = jobId;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getFirstRodLength() {
        return firstRodLength;
    }

    public void setFirstRodLength(String firstRodLength) {
        this.firstRodLength = firstRodLength;
    }

    public String getDefaultRodLength() {
        return defaultRodLength;
    }

    public void setDefaultRodLength(String defaultRodLength) {
        this.defaultRodLength = defaultRodLength;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSynced() {
        return isSynced;
    }

    public void setSynced(boolean synced) {
        isSynced = synced;
    }

    public String getPrefDepth() {
        return prefDepth;
    }

    public void setPrefDepth(String prefDepth) {
        this.prefDepth = prefDepth;
    }

    public String getPrefPitch() {
        return prefPitch;
    }

    public void setPrefPitch(String prefPitch) {
        this.prefPitch = prefPitch;
    }

    public static Job createOrUpdateJob(String userId, String jobName, String locationName, String companyName,
                                        String clientName, String jobDesc, String firstRodLen,
                                        String defaultRodLen, Job job) {
        long time = Calendar.getInstance().getTimeInMillis();
        if(null == job) {
            job = new Job();
            job.setCreatedTime(time);
            job.setJobName(jobName);
            job.setJobId(UGMApplication.getUUID());
            job.setUserId(userId);
            job.setStatus(UGMMacros.JOB_CREATED);
        } else {
            job.setStatus(UGMMacros.JOB_INPROGRESS);
        }
        job.setUpdatedTime(time);
        job.setLocationName(locationName);
        job.setCompanyName(companyName);
        job.setClientName(clientName);
        job.setJobDescription(jobDesc);
        job.setFirstRodLength(firstRodLen);
        job.setDefaultRodLength(defaultRodLen);
        job.setSynced(false);
        return job;
    }

    @Override
    public int compareTo(@NonNull Job job) {
        if(this.updatedTime == job.getUpdatedTime() || this.updatedTime > job.getUpdatedTime()) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        String.format("JobData Name:" + jobName +", UserId:" + userId);
        return super.toString();
    }
}
