package com.ugm.android.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.ugm.android.database.dao.JobDao;
import com.ugm.android.database.dao.RecordDao;
import com.ugm.android.database.dao.UserDao;
import com.ugm.android.database.entity.Job;
import com.ugm.android.database.entity.Record;
import com.ugm.android.database.entity.User;

@Database(entities = {User.class, Job.class, Record.class}, version = 1)
public abstract class UGMDatabase extends RoomDatabase {

    public static final String USER_TABLE = "User";
    public static final String JOB_TABLE = "JobData";
    public static final String RECORD_TABLE = "Record";

    private static final String DB_NAME = "ugm.db";
    private static volatile UGMDatabase instance;

    public static synchronized UGMDatabase getInstance(Context context) {
        if (instance == null) {
            synchronized (UGMDatabase.class) {
                if (instance == null) {
                    instance = create(context);
                }
            }
        }
        return instance;
    }

    private static UGMDatabase create(Context context) {
        return Room.databaseBuilder(context, UGMDatabase.class, DB_NAME)
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
    }

    public abstract UserDao getUserDao();
    public abstract JobDao getJobDao();
    public abstract RecordDao getRecordDao();

    @Override
    public void clearAllTables() {
    }
}
