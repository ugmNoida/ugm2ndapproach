package com.ugm.android.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.ugm.android.database.entity.Record;

import java.util.List;

@Dao
public abstract class RecordDao implements BaseDao<Record> {

    @Query("SELECT * FROM Record")
    public abstract List<Record> getAllRecords();

    @Query("SELECT * FROM Record WHERE Record.isSynced =:isSynced")
    public abstract List<Record> getRecordsBySynced(boolean isSynced);

    @Query("SELECT * FROM Record WHERE Record.isSynced =:isSynced AND Record.RodNumber <= :rodNumber")
    public abstract List<Record> getRecordsBySyncedAndRodNumber(boolean isSynced, int rodNumber);

    @Query("SELECT * FROM Record WHERE Record.jobId=:jobId AND Record.Status <> :status ORDER BY Record.RodNumber DESC")
    public abstract List<Record> getRecordsByJobIdNotStatus(String jobId, String status);

    @Query("SELECT * FROM Record WHERE Record.jobId=:jobId AND Record.Status IN (:statuses) ORDER BY Record.RodNumber DESC")
    public abstract List<Record> getRecordsByJobIdAndStatus(String jobId, List<String> statuses);

    @Query("SELECT * FROM Record WHERE Record.jobId=:jobID AND Record.rodNumber=:rodNo")
    public abstract List<Record> getRecordsByJobIdAndRodNo(String jobID, int rodNo);

    @Query("SELECT * FROM Record WHERE Record.jobId=:jobID AND Record.rodNumber=:rodNo AND Record.Status <> :status")
    public abstract List<Record> getRecordsByJobIdAndRodNoAndStatus(String jobID, int rodNo, String status);

    @Query("SELECT * FROM Record WHERE Record.jobId=:jobID")
    public abstract List<Record> getRecordsByJobId(String jobID);

    @Query("SELECT * FROM Record WHERE Record.jobId=:jobID AND Record.Status <> :status ORDER BY Record.RodNumber ASC")
    public abstract List<Record> getSortedRecordsByJobId(String jobID, String status);

    @Query("SELECT * FROM Record WHERE Record.jobId=:jobId AND Record.Status <> :status AND Record.isSynced =:isSynced")
    public abstract List<Record> getRecordsByJobIdAndSynced(String jobId, String status, boolean isSynced);

    @Query("SELECT * FROM Record WHERE Record.recordId=:recordId")
    public abstract Record getRecordByRecordId(String recordId);

    @Query("SELECT * FROM Record WHERE Record.jobId=:jobId AND Record.Status <> :status ORDER BY Record.RodNumber DESC LIMIT 1")
    public abstract Record getLastRecord(String jobId, String status);
}
