package com.ugm.android.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.ugm.android.database.entity.User;

import java.util.List;

@Dao
public abstract class UserDao implements BaseDao<User> {

    @Query("SELECT * FROM User")
    public abstract List<User> getAllUsers();

    @Query("SELECT * FROM User WHERE User.emailId=:emailId")
    public abstract User getUserByEmailId(String emailId);
}
