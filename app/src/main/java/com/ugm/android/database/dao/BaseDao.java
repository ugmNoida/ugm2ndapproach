package com.ugm.android.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

import java.util.ArrayList;

@Dao
public interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(T object);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(ArrayList<T> objects);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(T object);
    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateAll(ArrayList<T> objects);

    @Delete
    void delete(T object);
    @Delete
    void deleteAll(ArrayList<T> objects);
}
