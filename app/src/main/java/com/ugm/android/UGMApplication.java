package com.ugm.android;

import android.app.Application;
import android.util.Log;

import com.ugm.android.database.UGMDatabase;
import com.ugm.android.utilities.LogbackConfigure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import ch.qos.logback.classic.Level;

public class UGMApplication extends Application {

    private static Logger logger = LoggerFactory.getLogger(UGMApplication.class);

    private static UGMApplication instance;
    private AtomicInteger count = new AtomicInteger();

    public static UGMApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        LogbackConfigure logbackConfigure = LogbackConfigure.getInstance(instance);
        logbackConfigure.configureLogback(true, Level.ALL, 5);
    }

    public UGMDatabase getDatabase() {
        return UGMDatabase.getInstance(this);
    }

    public static String getUUID() {
        return UUID.randomUUID().toString();
    }

    public void addCount(){
        int i = count.incrementAndGet();
        logger.info("counttest addCount = "+i);
    }

    public void subtractCount(){
        int i = count.decrementAndGet();
        logger.info("counttest subtractCount = "+i);
    }

    public int getCount(){
        int i = count.get();
        if(i>1)
            logger.info("counttest getCount = "+i);
        return i;
    }

    public void setCount(int newVal){
        logger.info("counttest setCount = "+newVal);
        count.getAndSet(newVal);
    }

}
