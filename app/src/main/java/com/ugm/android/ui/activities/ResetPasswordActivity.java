package com.ugm.android.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ugm.android.R;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMUtility;
import com.ugm.android.webservice.GetResetPasswordData;
import com.ugm.android.webservice.ResetPassword;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends AbstractBaseActivity implements View.OnClickListener {

    private static Logger logger = LoggerFactory.getLogger(ResetPasswordActivity.class);

    private TextInputEditText editEmail, editPassword, editConfPassword;
    private TextInputLayout txtEmail, txtPassword, txtConfPassword;
    private Button resetPassBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initActionBar(getString(R.string.title_activity_rpd));

        txtEmail = (TextInputLayout)findViewById(R.id.text_email);
        editEmail = (TextInputEditText)findViewById(R.id.edit_email);
        editPassword = (TextInputEditText)findViewById(R.id.edit_new_password);
        editConfPassword = (TextInputEditText)findViewById(R.id.edit_conf_new_password);
        txtPassword = (TextInputLayout)findViewById(R.id.text_new_password);
        txtConfPassword = (TextInputLayout)findViewById(R.id.text_conf_new_password);

        resetPassBtn = (Button)findViewById(R.id.reset_password_btn);
        resetPassBtn.setOnClickListener(this);
    }

    private String doValidation() {
        String emailId = removePrefixEmail(editEmail.getText().toString());
        if(TextUtils.isEmpty(emailId)) {
            txtEmail.setError(getString(R.string.please_enter_email_id));
            emailId = null;
        } else if(!isEmailValid(emailId)) {
            txtEmail.setError(getString(R.string.please_enter_valid_email_if));
            emailId = null;
        }
        String password = editPassword.getText().toString();
        String confPassword = editConfPassword.getText().toString();
        if(TextUtils.isEmpty(password)) {
            txtPassword.setError(getString(R.string.please_enter_valid_password));
            emailId = null;
        } else if(TextUtils.isEmpty(confPassword)) {
            txtConfPassword.setError(getString(R.string.please_enter_valid_password));
            emailId = null;
        } else if(!isPasswordValid(password)) {
            txtPassword.setError(getString(R.string.password_must_greater_five));
            emailId = null;
        } else if(!(password.equals(confPassword))) {
            txtPassword.setError(getString(R.string.password_mismatch));
            emailId = null;
        }
        return emailId;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        final String emailId = doValidation();
        String password = editPassword.getText().toString();
        if(UGMUtility.isValidString(emailId) && UGMUtility.isValidString(password)) {
            if(UGMUtility.isNetworkAvailable()) {
                try {
                    JsonObject request = new JsonObject();
                    request.addProperty("email", emailId);
                    request.addProperty("password", password);
                    request.addProperty("reset_key", emailId);
                    logger.info("Reset EmailId:"+emailId);
                    GetResetPasswordData service = RetrofitClientInstance.getRetrofitInstance().create(GetResetPasswordData.class);
                    Call<ResetPassword> call = service.getResetPasswordData(request);
                    showProgressDialog();
                    call.enqueue(new Callback<ResetPassword>() {
                        @Override
                        public void onResponse(Call<ResetPassword> call, Response<ResetPassword> response) {
                            hideProgressDialog();
                            ResetPassword login = response.body();
                            logger.info("#######Reset Password:Response JSON:"+new Gson().toJson(login));
                            if(login != null){
                                if(response.body().getSuccess() != null && login.getSuccess().equalsIgnoreCase("true")) {
                                    showSuccessDialog(emailId);
                                } else {
                                    logger.info("onResponse:Invalid EmailId");
                                    showMessage(String.format(getString(R.string.invalid_emailid), emailId),
                                            getString(R.string.ok),null);
                                }
                            } else {
                                logger.info("onResponse:Something went wrong.");
                                showMessage(getString(R.string.something_went_wrong), getString(R.string.ok),null);
                            }
                        }
                        @Override
                        public void onFailure(Call<ResetPassword> call, Throwable t) {
                            logger.info("onFailure:Something went wrong.");
                            hideProgressDialog();
                            showMessage(getString(R.string.something_went_wrong), getString(R.string.ok),null);
                        }
                    });
                } catch(Exception ex) {
                    logger.info("Exception while send forgot password:" + ex.toString());
                }
            } else {
                showMessage(getString(R.string.network_unavailable), getString(R.string.ok), null);
            }
        }
    }

    private void showSuccessDialog(String emailId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_activity_fpd));
        builder.setMessage(String.format(getString(R.string.forgot_password_success_txt), emailId));
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }
}
