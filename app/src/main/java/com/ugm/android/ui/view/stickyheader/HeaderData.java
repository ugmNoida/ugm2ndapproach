package com.ugm.android.ui.view.stickyheader;

import android.support.annotation.LayoutRes;

public interface HeaderData extends StickyMainData {

    @LayoutRes
    int getHeaderLayout();

    int getHeaderType();
}
