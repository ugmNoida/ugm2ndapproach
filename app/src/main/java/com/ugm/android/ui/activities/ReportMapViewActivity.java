package com.ugm.android.ui.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.ugm.android.R;
import com.ugm.android.UGMApplication;
import com.ugm.android.database.entity.Job;
import com.ugm.android.database.entity.Record;
import com.ugm.android.utilities.UGMMacros;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class ReportMapViewActivity extends AbstractBaseActivity implements OnMapReadyCallback {

    private static final String TAG = "ReportMapViewActivity";

    private ArrayList<LatLng> mListLatLng;
    private Job job;
    private ArrayList<Record> records;
    private GoogleMap mGoogleMap = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_mapview_activity);
        processIntent(getIntent());
        initActionBar(getString(R.string.map_view_report));
        initMapData();
        showProgressDialog();
        findViewById(R.id.map).setVisibility(View.VISIBLE);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.captured_screenshot_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.captured_screen_menu:
                captureMap();
                break;
        }
        return true;
    }

    private void captureMap() {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bitmap;

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                bitmap = snapshot;
                try {
                    File rootFolder = getExternalFilesDir(Environment.MEDIA_SHARED);
                    File capturedFolder = new File(rootFolder, "UGMReport");
                    if (!capturedFolder.exists()) {
                        capturedFolder.mkdirs();
                    }
                    File capturedFile = new File(capturedFolder, job.getJobId() + "_Map.jpg");
                    if (capturedFile.exists()) {
                        capturedFile.delete();
                    }
                    capturedFile.createNewFile();

                    FileOutputStream outputStream = new FileOutputStream(capturedFile);
                    int quality = 100;
                    bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
                    outputStream.flush();
                    outputStream.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception while getScreenshotFileFromView UGM Report file.", e);
                }
            }
        };
        mGoogleMap.snapshot(callback);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void processIntent(Intent intent) {
        if (intent != null) {
            if (intent != null) {
                job = (Job) intent.getSerializableExtra("JOB_ITEM");
                //records =  (ArrayList<Record>) intent.getSerializableExtra("RECORD_LIST");
                records = (ArrayList<Record>) UGMApplication.getInstance().getDatabase().getRecordDao()
                        .getSortedRecordsByJobId(job.getJobId(), UGMMacros.RECORD_DELETED);
            }
        }
    }

    private void initMapData() {
        if (records != null && records.size() > 0) {
            mListLatLng = null;
            mListLatLng = new ArrayList<>();
            for (Record record : records) {
                Double lat = 0.0, lng = 0.0;
                if (!TextUtils.isEmpty(record.getLatitude()))
                    lat = Double.parseDouble(record.getLatitude());

                if (!TextUtils.isEmpty(record.getLongitude()))
                    lng = Double.parseDouble(record.getLongitude());

                if (lat != 0.0 && lng != 0.0)
                    mListLatLng.add(new LatLng(lat, lng));
                else
                    Log.e(TAG, "Lat and Lng value == 0.0");
            }
        }
    }

    public void initActionBar(String title) {
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
        Window window = this.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void addMarkersToMap() {
        if (mGoogleMap != null && mListLatLng != null && mListLatLng.size() > 0) {
            double prevLatitude = 0, prevLongitude = 0;
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            int totalSize = mListLatLng.size();
            int index = 0;
            for (int i = 0; i < totalSize; i++) {
                LatLng latLng = mListLatLng.get(i);
                if (i == 0) {
                    mGoogleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.startpointer)).zIndex(totalSize));
                } else if (i == (totalSize - 1)) {
                    mGoogleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.endpointer)).zIndex(totalSize - i));
                } else {
                    mGoogleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.markerpointer)).zIndex(totalSize - i));
                }
                builder.include(latLng);

                if (index > 0 && index < totalSize) {
                    mGoogleMap.addPolyline(new PolylineOptions()
                            .add(new LatLng(prevLatitude, prevLongitude), new LatLng(latLng.latitude, latLng.longitude))
                            .width(5)
                            .color(ResourcesCompat.getColor(getResources(), R.color.map_line_color, null)));
                }

                prevLatitude = latLng.latitude;
                prevLongitude = latLng.longitude;
                index++;
            }
            LatLngBounds bounds = builder.build();
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 40));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        hideProgressDialog();
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        addMarkersToMap();
    }
}
