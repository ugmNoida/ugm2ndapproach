package com.ugm.android.ui.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ugm.android.R;
import com.ugm.android.UGMApplication;
import com.ugm.android.database.entity.Job;
import com.ugm.android.ui.fragments.JobDetailFragment;
import com.ugm.android.ui.fragments.JobMapViewFragment;
import com.ugm.android.ui.fragments.RecordsViewFragment;
import com.ugm.android.utilities.UGMUtility;

public class RecordPagerAdapter extends FragmentStatePagerAdapter {

    private Job mJob;
    private Fragment currFragment;
    public RecordPagerAdapter(FragmentManager fm,Job job) {
        super(fm);
        this.mJob = job;
    }

    public Fragment getCurrFragment(){
        return currFragment;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            currFragment = new RecordsViewFragment();
        } else if (position == 1) {
            currFragment = new JobDetailFragment();
        } else if (position == 2) {
            currFragment = new JobMapViewFragment();
        }
        return currFragment;
    }

    @Override
    public int getCount() {
        if(UGMUtility.isGoogleMapEnabled())
            return 3;
        else
            return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = UGMApplication.getInstance().getString(R.string.rodwise_view);;
        } else if (position == 1) {
            title = UGMApplication.getInstance().getString(R.string.job_details);
        } else if (position == 2) {
            title = UGMApplication.getInstance().getString(R.string.map_view);;
        }
        return title;
    }


}