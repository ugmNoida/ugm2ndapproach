package com.ugm.android.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ugm.android.R;
import com.ugm.android.database.entity.Job;
import com.ugm.android.datamodel.JobItemListModel;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;

import java.util.ArrayList;

public class RecyclerDeletedJobListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private ArrayList<JobItemListModel> jobItemList = new ArrayList<JobItemListModel>();
    private JobItemMenuClickListener itemClickListener;

    public interface JobItemMenuClickListener {
        void onJobItemMenuClick(Job jobItem);
        void onEmptyBinAll();
    }

    public RecyclerDeletedJobListAdapter(ArrayList<JobItemListModel> jobItemList, JobItemMenuClickListener listener) {
        this.jobItemList = jobItemList;
        this.itemClickListener = listener;
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public View view;
        private TextView headerText;
        private RelativeLayout jobViewItemLayout;
        private RelativeLayout jobHeaderLayout;
        public HeaderViewHolder(View headerView) {
            super(headerView);
            view = headerView;
            this.headerText = (TextView) view.findViewById(R.id.header_text);
            this.jobViewItemLayout = (RelativeLayout) view.findViewById(R.id.job_view_item);
            jobViewItemLayout.setVisibility(View.GONE);
            this.jobHeaderLayout = (RelativeLayout) view.findViewById(R.id.job_view_header);
            jobHeaderLayout.setVisibility(View.VISIBLE);
            jobHeaderLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onEmptyBinAll();
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public View view;
        private RelativeLayout jobViewItemLayout;
        private RelativeLayout jobHeaderLayout;
        private TextView jobName;
        private TextView jobLocation;
        private TextView jobTime;
        private ImageView rightChevIcon;
        public ItemViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            jobLocation = (TextView) view.findViewById(R.id.job_location);
            jobName = (TextView) view.findViewById(R.id.job_name);
            jobTime = (TextView) view.findViewById(R.id.job_date);
            rightChevIcon = (ImageView)view.findViewById(R.id.right_chev);
            rightChevIcon.setOnClickListener(this);
            this.jobViewItemLayout = (RelativeLayout) view.findViewById(R.id.job_view_item);
            jobViewItemLayout.setVisibility(View.VISIBLE);
            this.jobHeaderLayout = (RelativeLayout) view.findViewById(R.id.job_view_header);
            jobHeaderLayout.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onJobItemMenuClick(jobItemList.get(getAdapterPosition()).getJobModel());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isGroupPosition(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private boolean isGroupPosition(int position) {
        if(null != jobItemList && 0 < jobItemList.size()) {
            JobItemListModel itemListModel = jobItemList.get(position);
            if(null != itemListModel.getHeaderName()) {
                return true;
            }
            return false;
        }
        return false;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.deleted_joblist_adapter, viewGroup, false);
            return new HeaderViewHolder(view);
        }
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.deleted_joblist_adapter, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof HeaderViewHolder) {
            //((HeaderViewHolder) viewHolder).headerText.setText(jobItemList.get(position).getHeaderName());
        } else {
            ((ItemViewHolder) viewHolder).jobLocation.setText(jobItemList.get(position).getJobModel().getLocationName());
            ((ItemViewHolder) viewHolder).jobName.setText(jobItemList.get(position).getJobModel().getJobName());
            long jobTime = jobItemList.get(position).getJobModel().getUpdatedTime();
            ((ItemViewHolder) viewHolder).jobTime.setText(UGMUtility.covertLongToDFormattedDate(jobTime, UGMMacros.FORMATTER1));
            (((ItemViewHolder) viewHolder).rightChevIcon).setBackgroundResource(R.drawable.vert_menu_black);
        }
    }

    @Override
    public int getItemCount() {
        return jobItemList.size();
    }
}
