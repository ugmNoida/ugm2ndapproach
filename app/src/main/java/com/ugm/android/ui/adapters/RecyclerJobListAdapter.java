package com.ugm.android.ui.adapters;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ugm.android.R;
import com.ugm.android.UGMApplication;
import com.ugm.android.database.entity.Job;
import com.ugm.android.datamodel.JobItemListModel;
import com.ugm.android.ui.view.RecyclerViewStickyHeaderItem;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;

import java.util.ArrayList;

public class RecyclerJobListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RecyclerViewStickyHeaderItem.StickyHeaderInterface{

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private ArrayList<JobItemListModel> jobItemList = new ArrayList<JobItemListModel>();
    private JobItemClickListener itemClickListener;

    public interface JobItemClickListener {
        void onJobItemClick(Job jobItem);
    }

    public RecyclerJobListAdapter(ArrayList<JobItemListModel> jobItemList, JobItemClickListener listener) {
        this.jobItemList = jobItemList;
        this.itemClickListener = listener;
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public View view;
        private TextView headerText;
        private RelativeLayout jobViewItemLayout;
        private RelativeLayout jobHeaderLayout;
        public HeaderViewHolder(View headerView) {
            super(headerView);
            view = headerView;
            this.headerText = (TextView) view.findViewById(R.id.header_text);
            this.jobViewItemLayout = (RelativeLayout) view.findViewById(R.id.job_view_item);
            jobViewItemLayout.setVisibility(View.GONE);
            this.jobHeaderLayout = (RelativeLayout) view.findViewById(R.id.job_view_header);
            jobHeaderLayout.setVisibility(View.VISIBLE);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public View view;
        private RelativeLayout jobViewItemLayout;
        private RelativeLayout jobHeaderLayout;
        private TextView jobName;
        private TextView jobLocation;
        private TextView jobTime;
        private TextView jobStatus;
        private ImageView rightChevIcon;
        public ItemViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            jobLocation = (TextView) view.findViewById(R.id.job_location);
            jobName = (TextView) view.findViewById(R.id.job_name);
            jobTime = (TextView) view.findViewById(R.id.job_date);
            jobStatus = (TextView) view.findViewById(R.id.job_status);
            rightChevIcon = (ImageView)view.findViewById(R.id.right_chev);
            this.jobViewItemLayout = (RelativeLayout) view.findViewById(R.id.job_view_item);
            jobViewItemLayout.setVisibility(View.VISIBLE);
            this.jobHeaderLayout = (RelativeLayout) view.findViewById(R.id.job_view_header);
            jobHeaderLayout.setVisibility(View.GONE);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onJobItemClick(jobItemList.get(getAdapterPosition()).getJobModel());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isGroupPosition(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private boolean isGroupPosition(int position) {
        if(null != jobItemList && 0 < jobItemList.size()) {
            JobItemListModel itemListModel = jobItemList.get(position);
            if(null != itemListModel.getHeaderName()) {
                return true;
            }
            return false;
        }
        return false;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.joblist_adapter, viewGroup, false);
            return new HeaderViewHolder(view);
        }
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.joblist_adapter, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof HeaderViewHolder) {
            ((HeaderViewHolder) viewHolder).headerText.setText(jobItemList.get(position).getHeaderName());
        } else {
            ((ItemViewHolder) viewHolder).jobLocation.setText(jobItemList.get(position).getJobModel().getLocationName());
            ((ItemViewHolder) viewHolder).jobName.setText(jobItemList.get(position).getJobModel().getJobName());
            long jobTime = jobItemList.get(position).getJobModel().getUpdatedTime();
            ((ItemViewHolder) viewHolder).jobTime.setText(UGMUtility.covertLongToDFormattedDate(jobTime, UGMMacros.FORMATTER1));
            (((ItemViewHolder) viewHolder).rightChevIcon).setBackgroundResource(R.drawable.next);
            TextView statusView = ((ItemViewHolder) viewHolder).jobStatus;
            statusView.setVisibility(View.VISIBLE);
            if(UGMMacros.JOB_INPROGRESS.equals(jobItemList.get(position).getJobModel().getStatus())) {
                statusView.setText(UGMApplication.getInstance().getString(R.string.in_progress));
                statusView.setTextColor(Color.parseColor("#c22f00"));
                GradientDrawable tvStatusBg = (GradientDrawable) statusView.getBackground();
                tvStatusBg.setColor(Color.parseColor("#f9a88f"));
            } else if(UGMMacros.JOB_COMPLETED.equals(jobItemList.get(position).getJobModel().getStatus())) {
                statusView.setText(UGMApplication.getInstance().getString(R.string.completed));
                statusView.setTextColor(Color.parseColor("#4cae4c"));
                GradientDrawable tvStatusBg = (GradientDrawable) statusView.getBackground();
                tvStatusBg.setColor(Color.parseColor("#dff0d8"));
            } else {
                (((ItemViewHolder) viewHolder).jobStatus).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return jobItemList.size();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        int headerPosition = 0;
        do {
            if (this.isHeader(itemPosition)) {
                headerPosition = itemPosition;
                break;
            }
            itemPosition -= 1;
        } while (itemPosition >= 0);
        return headerPosition;
    }

    @Override
    public int getHeaderLayout(int headerPosition) {
        return R.layout.joblist_adapter;
    }

    @Override
    public void bindHeaderData(View header, int headerPosition) {
        JobItemListModel jobItem = jobItemList.get(headerPosition);//.getHeaderName()
        if(null != jobItem.getHeaderName()) {
            RelativeLayout jobViewLayout = (RelativeLayout) header.findViewById(R.id.job_view_item);
            jobViewLayout.setVisibility(View.GONE);
            RelativeLayout headerLayout = (RelativeLayout) header.findViewById(R.id.job_view_header);
            headerLayout.setVisibility(View.VISIBLE);
            headerLayout.setBackgroundColor(Color.parseColor("#ececec"));
            TextView headerText = (TextView) header.findViewById(R.id.header_text);
            headerText.setText(jobItem.getHeaderName());
            headerText.setTextColor(Color.parseColor("#b82d46"));
        }
    }

    @Override
    public boolean isHeader(int itemPosition) {
        if(null != jobItemList && 0 < jobItemList.size() && itemPosition < jobItemList.size()) {
            return (null != jobItemList.get(itemPosition).getHeaderName()) ? true : false;
        }
        return false;
    }

}
