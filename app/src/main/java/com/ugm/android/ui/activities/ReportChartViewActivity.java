package com.ugm.android.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.ugm.android.R;
import com.ugm.android.database.entity.Job;
import com.ugm.android.database.entity.Record;
import com.ugm.android.datamodel.ReportPDFViewModel;
import com.ugm.android.datamodel.ReportRecordModel;
import com.ugm.android.utilities.UGMUtility;

import java.util.ArrayList;
import java.util.List;

public class ReportChartViewActivity extends AbstractBaseActivity {

    private static final String TAG = "ReportChartViewActivity";

    private TextView titleTxt;
    private RelativeLayout chartViewLayout;
    private LineChart chart;
    private Job job;
    private ArrayList<Record> records;
    private ArrayList<ReportRecordModel> chartRecords;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_chartview_activity);
        processIntent(getIntent());
        initActionBar(getString(R.string.chart_view_report));
        initView();
    }

    private void processIntent(Intent intent){
        if(intent != null){
            if(intent != null){
                job =  (Job)intent.getSerializableExtra("JOB_ITEM");
                records =  (ArrayList<Record>) intent.getSerializableExtra("RECORD_LIST");
            }
        }
    }

    private void initView() {
        titleTxt = findViewById(R.id.title_txt);
        titleTxt.setText(getString(R.string.job_profile_chart));
        chartViewLayout = findViewById(R.id.chartview_layout);
        chart = (LineChart)findViewById(R.id.chart);
        chartViewLayout = (RelativeLayout)findViewById(R.id.chartview_layout);
        chartRecords = ReportRecordModel.getChartDataModel(records, job);
        prepareChartView(chartRecords);
    }

    List<List<Entry>> relDepthEntriesAll = new ArrayList<List<Entry>>();
    private void prepareRelDepthEntry(ArrayList<ReportRecordModel> chartRecords, int index) {
        List<Entry> depthEntries = new ArrayList<Entry>();
        for (int i = index; i < chartRecords.size(); i++) {
            ReportRecordModel recordModel = chartRecords.get(i);
            if (UGMUtility.isValidFloatString(recordModel.getPitch()) &&
                    UGMUtility.isValidFloatString(recordModel.getRelativeDepth()) &&
                    UGMUtility.isValidFloatString(recordModel.getxDistance())) {
                depthEntries.add(new Entry(Float.parseFloat(recordModel.getxDistance()),
                        Float.parseFloat(recordModel.getRelativeDepth())));
            } else if ((i + 1) < chartRecords.size()) {
                if (chartRecords.get(i + 1) != null) {
                    prepareRelDepthEntry(chartRecords, i + 1);
                    break;
                }
            }
        }
        relDepthEntriesAll.add(depthEntries);
    }

    List<List<Entry>> relElevEntriesAll = new ArrayList<List<Entry>>();
    private void prepareRelElevEntry(ArrayList<ReportRecordModel> chartRecords, int index) {
        List<Entry> relElvEntries = new ArrayList<Entry>();
        for (int i = index; i < chartRecords.size(); i++) {
            ReportRecordModel recordModel = chartRecords.get(i);
            if (UGMUtility.isValidFloatString(recordModel.getRelativeElevation()) &&
                    UGMUtility.isValidFloatString(recordModel.getxDistance())) {
                relElvEntries.add(new Entry(Float.parseFloat(recordModel.getxDistance()),
                        Float.parseFloat(recordModel.getRelativeElevation())));
            } else if ((i + 1) < chartRecords.size()) {
                if (chartRecords.get(i + 1) != null) {
                    prepareRelElevEntry(chartRecords, i + 1);
                    break;
                }
            }
        }
        relElevEntriesAll.add(relElvEntries);
    }

    private void prepareChartView(ArrayList<ReportRecordModel> chartRecords) {
        if(null != chartRecords && 0 < chartRecords.size()) {
            List<ILineDataSet> chartDataSets = new ArrayList<ILineDataSet>();
            prepareRelDepthEntry(chartRecords, 0);
            prepareRelElevEntry(chartRecords, 0);
            if (null != relDepthEntriesAll && 0 < relDepthEntriesAll.size()) {
                for (List<Entry> relDepthEntries : relDepthEntriesAll) {
                    if (0 < relDepthEntries.size()) {
                        LineDataSet relDepthDataSet = drawPdfDepth(relDepthEntries);
                        relDepthDataSet.setDrawHighlightIndicators(false);
                        relDepthDataSet.setDrawHorizontalHighlightIndicator(false);
                        relDepthDataSet.setDrawValues(true);
                        chartDataSets.add(relDepthDataSet);
                    }
                }
            }

            if (null != relElevEntriesAll && 0 < relElevEntriesAll.size()) {
                for (List<Entry> relElvEntries : relElevEntriesAll) {
                    if (0 < relElvEntries.size()) {
                        LineDataSet relElevDataSet = drawRelativeElevation(relElvEntries);
                        relElevDataSet.setDrawHighlightIndicators(false);
                        relElevDataSet.setDrawHorizontalHighlightIndicator(false);
                        relElevDataSet.setDrawValues(true);
                        chartDataSets.add(relElevDataSet);
                    }
                }
            }

            LineData lineData = new LineData(chartDataSets);
            chart.setData(lineData);
            chart.getDescription().setEnabled(false);
            chart.getAxisRight().setEnabled(false);

            Legend legend = chart.getLegend();
            legend.setForm(Legend.LegendForm.CIRCLE);
            LegendEntry legendEntry1 = new LegendEntry();
            legendEntry1.label = getString(R.string.relative_elevation);
            legendEntry1.formColor = Color.parseColor("#ff0000");
            LegendEntry legendEntry2 = new LegendEntry();
            legendEntry2.label = getString(R.string.relative_depth);
            legendEntry2.formColor = Color.parseColor("#0000ff");
            ArrayList<LegendEntry> legendEntries = new ArrayList<LegendEntry>();
            legendEntries.add(legendEntry1);
            legendEntries.add(legendEntry2);
            legend.setCustom(legendEntries);

            XAxis xAxis = chart.getXAxis();
            xAxis.setGranularity(1f); // min interval 1f (1 year in this case)
            xAxis.setGranularityEnabled(true);
            chart.animateX(2500);
        }
    }

    private LineDataSet drawRelativeElevation(List<Entry> relElvEntries) {
        LineDataSet dataSet = new LineDataSet(relElvEntries, getString(R.string.relative_elevation));
        dataSet.setColor(Color.parseColor("#ff0000"));
        dataSet.setValueTextColor(Color.parseColor("#ff0000"));
        dataSet.setCircleColor(Color.parseColor("#ff0000"));
        //dataSet.disableDashedLine();
        return dataSet;
    }

    private LineDataSet drawPdfDepth(List<Entry> pdfDepthEntries) {
        LineDataSet dataSet = new LineDataSet(pdfDepthEntries, getString(R.string.relative_depth)); // add entries to dataset
        dataSet.setColor(Color.parseColor("#0000ff"));
        dataSet.setValueTextColor(Color.parseColor("#0000ff"));
        dataSet.setCircleColor(Color.parseColor("#0000ff"));
        //dataSet.disableDashedLine();
        return dataSet;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.captured_screen_menu:
                ReportPDFViewModel.captureChartScreen(ReportChartViewActivity.this, chartViewLayout, job.getJobId());
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.captured_screenshot_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
