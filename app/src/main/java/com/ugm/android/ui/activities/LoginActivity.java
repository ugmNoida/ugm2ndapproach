package com.ugm.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ugm.android.R;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;
import com.ugm.android.webservice.GetLoginData;
import com.ugm.android.webservice.GetSettingsData;
import com.ugm.android.webservice.Login;
import com.ugm.android.webservice.SettingsData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AbstractBaseActivity implements View.OnClickListener {

    private static Logger logger = LoggerFactory.getLogger(LoginActivity.class);

    private Button loginButton;
    private TextView touTxv, umMapsLable, registerButton, forgotPassword;
    private TextInputLayout txtEmail, txtPassword;
    private TextInputEditText editEmail, editPassword;
    private CheckBox touCheckBox;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        String userId = UGMUtility.getStringPreference(PREFS_LOGGEDIN_USERID, null);
        if(null != userId && !TextUtils.isEmpty(userId)) {
            openJobList();
            finish();
        }
    }

    private void initUI() {
        editEmail = (TextInputEditText)findViewById(R.id.edit_email);
        umMapsLable = (TextView)findViewById(R.id.um_maps_lable);
        //editEmail.setText("a@a.com");
        txtEmail = (TextInputLayout)findViewById(R.id.text_email);
        editPassword = (TextInputEditText)findViewById(R.id.edit_password);
        //editPassword.setText("a@12345");
        txtPassword = (TextInputLayout)findViewById(R.id.text_password);
        loginButton = (Button)findViewById(R.id.btn_login);
        registerButton = findViewById(R.id.btn_register);
        registerButton.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        touCheckBox = (CheckBox) findViewById(R.id.chk_box);
        touTxv = (TextView) findViewById(R.id.text_conditions);
        touTxv.setOnClickListener(this);
        forgotPassword = (TextView)findViewById(R.id.forgot_password_txt);
        forgotPassword.setOnClickListener(this);
        umMapsLable.setText(getString(R.string.app_prefix)+getString(R.string.ummaps));
    }

    private void login() {
        RetrofitClientInstance.clearAllObjects();
        boolean isErrorExist = false;
        final String emailId = removePrefixEmail(editEmail.getText().toString());
        final String password = editPassword.getText().toString();
        if(TextUtils.isEmpty(emailId)) {
            isErrorExist = true;
            txtEmail.setError(getString(R.string.please_enter_email_id));
        } else if(!isEmailValid(emailId)) {
            isErrorExist = true;
            txtEmail.setError(getString(R.string.please_enter_valid_email_if));
        } else if(TextUtils.isEmpty(password)) {
            isErrorExist = true;
            txtPassword.setError(getString(R.string.please_enter_valid_password));
        } else if(!isPasswordValid(password)) {
            isErrorExist = true;
            txtPassword.setError(getString(R.string.password_must_greater_five));
        }
        if(!isErrorExist) {
            if(UGMUtility.isNetworkAvailable()) {
                try{
                    JsonObject request = new JsonObject();
                    request.addProperty("email", emailId);
                    request.addProperty("password",password);
                    logger.info("EmailId:"+emailId);
                    GetLoginData service = RetrofitClientInstance.getRetrofitInstance().create(GetLoginData.class);
                    Call<Login> call = service.getLoginData(request);
                    //logger.info("#######LoginActivity:login:Request JSON:"+new Gson().toJson(call.request()));
                    showProgressDialog();
                    call.enqueue(new Callback<Login>() {
                        @Override
                        public void onResponse(Call<Login> call, Response<Login> response) {
                            Login login = response.body();
                            logger.info("#######LoginActivity:login:Response JSON:"+new Gson().toJson(login));
                            if(login != null){
                                if(response.body().getSuccess() != null && login.getSuccess().equalsIgnoreCase("true")) {
                                    doLogin(emailId,login.getFullName(),login.getId(),login.getToken());
                                } else {
                                    logger.info("onResponse:Invalid Credential");
                                    showMessage(getString(R.string.invalid_credentials), getString(R.string.ok),null);
                                }
                            } else {
                                logger.info("onResponse:Something went wrong.");
                                showMessage(getString(R.string.something_went_wrong), getString(R.string.ok),null);
                            }
                            hideProgressDialog();
                        }
                        @Override
                        public void onFailure(Call<Login> call, Throwable t) {
                            logger.info("onFailure:Something went wrong.");
                            hideProgressDialog();
                            showMessage(getString(R.string.something_went_wrong), getString(R.string.ok),null);
                        }
                    });
                } catch (Exception ex){
                    logger.info("Exception while login:" + ex.toString());

                }
            } else {
                logger.info("Network is not available while login");
                showMessage(getString(R.string.network_unavailable), getString(R.string.ok), null);
            }
        }
    }

    private void doLogin(String userName, String fullName, String userID , String token) {
        UGMUtility.setStringPreference(PREFS_LOGGEDIN_USERID, userID);
        UGMUtility.setStringPreference(PREFS_FULL_NAME, fullName);
        UGMUtility.setStringPreference(PREFS_LOGGEDIN_EMAIL, userName);
        UGMUtility.setStringPreference(PREFS_LOGGEDIN_SESSION_TOKEN, token);
        logger.info("Login Session Token : " + token);
        getSettingsPrefernces();
        openJobList();
    }

    private void openJobList() {
        logger.info("Opening Container Activity form LoginActivity.");
        Intent containerIntent = new Intent(LoginActivity.this, ContainerActivity.class);
        startActivity(containerIntent);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                if(touCheckBox.isChecked())
                    login();
                else
                    showMessage(getString(R.string.please_accept_terms),getString(R.string.ok),null);
                break;
            case R.id.text_conditions:
                logger.info("Opening TOU Activity form LoginActivity.");
                startActivity(new Intent(this, TOUActivity.class));
                break;
            case R.id.forgot_password_txt:
                logger.info("Opening Forgot password Activity form LoginActivity.");
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                break;
            case  R.id.btn_register:
                Intent regIntent = new Intent(this, CreateAccountActivity.class);
                startActivity(regIntent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private void getSettingsPrefernces() {
        try {
            logger.info("Initializing to get Setting Preferences.");
            final String token = "Bearer " + UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_SESSION_TOKEN, "");
            GetSettingsData service = RetrofitClientInstance.getRetrofitInstance().create(GetSettingsData.class);
            Call<SettingsData> call = service.getSettingsData(token);
            logger.info("#######LoginActivity:getSettingsPrefernces:Request JSON:"+new Gson().toJson(call.request()));
            call.enqueue(new Callback<SettingsData>() {
                @Override
                public void onResponse(Call<SettingsData> call, Response<SettingsData> response) {
                    SettingsData settings = response.body();
                    logger.info("#######LoginActivity:getSettingsPrefernces:Response JSON:"+new Gson().toJson(settings));
                    if (settings != null) {
                        if(null != settings) {
                            UGMUtility.setBooleanPreference(UGMMacros.PREF_TRACK_LOCATION, settings.isRequiredlocation());
                            UGMUtility.setStringPreference(UGMMacros.PREF_LOCATION, settings.getWhichlocation());
                            logger.info("Setting Response RequiredLocation:"+settings.isRequiredlocation());
                            logger.info("Setting Response WhichLocation:"+settings.getWhichlocation());
                        }
                    } else {
                        logger.info("getSettingsPrefernces:onResponse:Something went wrong.");
                        showMessage(getString(R.string.something_wrong_while_fetchingsetting), getString(R.string.ok), null);
                    }
                }

                @Override
                public void onFailure(Call<SettingsData> call, Throwable t) {
                    logger.info("getSettingsPrefernces:onFailure:Something went wrong.");
                    showMessage(getString(R.string.something_wrong_while_fetchingsetting), getString(R.string.ok), null);
                }
            });
        } catch (Exception ex) {
            logger.info("getSettingsPrefernces:onFailure:Exception occurred."+ex.toString());
            showMessage(getString(R.string.something_wrong_while_fetchingsetting), getString(R.string.ok), null);
        }
    }
}
