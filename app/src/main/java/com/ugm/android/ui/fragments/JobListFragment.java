package com.ugm.android.ui.fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ugm.android.R;
import com.ugm.android.bluetooth.BluetoothUtils;
import com.ugm.android.bluetooth.DeviceThreadControl;
import com.ugm.android.bluetooth.GPSDevice;
import com.ugm.android.bluetooth.LocatorDevice;
import com.ugm.android.database.entity.Job;
import com.ugm.android.datamodel.JobItemListModel;
import com.ugm.android.ui.activities.AbstractBaseActivity;
import com.ugm.android.ui.activities.ContainerActivity;
import com.ugm.android.ui.activities.DeviceListActivity;
import com.ugm.android.ui.activities.RecordsOverviewActivity;
import com.ugm.android.ui.adapters.RecyclerJobListAdapter;
import com.ugm.android.ui.view.RecyclerViewStickyHeaderItem;
import com.ugm.android.utilities.Constants;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;

import java.util.ArrayList;
import java.util.List;

import static android.support.v7.widget.SearchView.OnQueryTextListener;

public class JobListFragment extends Fragment implements RecyclerJobListAdapter.JobItemClickListener {

    private static final String TAG = "JobListFragment";

    /*  Bluetooth socket connection params */
    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    private static final int REQUEST_ENABLE_BT_FAB = 4;
    private static final int REQUEST_ENABLE_BT_SYNC = 5;
    private static final int REQUEST_ENABLE_BT_CON = 6;

    /**
     * Local Bluetooth adapter
     */
    private BluetoothAdapter mBluetoothAdapter = null;
    private ArrayList<String> mConnectedDeviceAddress = null;
    private String mConnectingAddress = null;
    private Menu menu;
    private RecyclerView recyclerView;
    private RelativeLayout jobListEmptyView;
    private RecyclerView.LayoutManager recycleLayoutManager;
    private RecyclerJobListAdapter jobListAdapter;
    private ContainerActivity activity;
    private String loggedInUserId;

    private int deviceType;

    public JobListFragment() {

    }

    public static JobListFragment newInstance() {
        JobListFragment fragment = new JobListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loggedInUserId = UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_USERID, null);
        setHasOptionsMenu(true);
        mBluetoothAdapter = BluetoothUtils.getAdapter(activity);

        // If the adapter is null, then Bluetooth is not supported
        //TODO = need to confirm what should we do when bluetooth is not available
        if (mBluetoothAdapter == null) {
            if (activity != null) {
                activity.showMessage(getString(R.string.bluetooth_is_not_available), getString(R.string.ok), null);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job_list, container, false);
        createNewJob(view);
        return view;
    }

    private void createNewJob(View view) {
        initView(view);
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ContainerActivity) getActivity()).openJobCreateActivity();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ContainerActivity)
            activity = (ContainerActivity) context;
        else {
            Log.e(TAG, "onAttach context != ContainerActivity ***");
            activity = null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getAllJobs();
        if (null != mBluetoothAdapter && !mBluetoothAdapter.isEnabled()) {
//            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            startActivityForResult(enableIntent, );
            // Otherwise, setup the comm session
        } /*else if (BluetoothCommSer\]vice.getInstance() == null) {
            setupBTCommunication();
            BluetoothCommService.getInstance();
        }*/
        //todo = need to check whether else part required or not
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_VALUE_DEVICE_CONNECTED);
        filter.addAction(Constants.ACTION_VALUE_CONNECTION_LOST);
        filter.addAction(Constants.ACTION_JOB_SYNCED);
        LocalBroadcastManager.getInstance(activity).registerReceiver(listener, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(listener);
    }

    private void initView(View view) {
        activity.getSupportActionBar().setTitle(getString(R.string.my_job_list));
        activity.getSupportActionBar().setSubtitle(getString(R.string.zero_jobs_found));
        jobListEmptyView = (RelativeLayout) view.findViewById(R.id.joblist_empty_view);
        jobListEmptyView.setVisibility(View.VISIBLE);
        recyclerView = (RecyclerView) view.findViewById(R.id.joblist_recycler_view);
        recyclerView.setVisibility(View.GONE);
        //getAllJobs();
    }

    private void refreshList(List<Job> jobs) {
        if (null != jobs && 0 < jobs.size()) {
            ArrayList<JobItemListModel> jobItemListModels = JobItemListModel.getJobItemList(jobs);
            if (null != jobItemListModels && 0 < jobItemListModels.size()) {
                activity.getSupportActionBar().setSubtitle(jobs.size() + " " + getString(R.string.jobs_found));
                jobListEmptyView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView.setHasFixedSize(true);
                recycleLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(recycleLayoutManager);
                jobListAdapter = new RecyclerJobListAdapter(jobItemListModels, JobListFragment.this);
                recyclerView.addItemDecoration(new RecyclerViewStickyHeaderItem(recyclerView, jobListAdapter));
                recyclerView.setAdapter(jobListAdapter);
            } else {
                jobListEmptyView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        } else {
            jobListEmptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void getAllJobs() {
        List<Job> jobs = new ArrayList<>();
        if (null != loggedInUserId) {
            jobs = activity.getLocalDB().getJobDao().getJobsByUserId(loggedInUserId, activity.getActiveJobStatuses());
        }
        refreshList(jobs);
    }

    @Override
    public void onJobItemClick(Job jobItem) {
        //Toast.makeText(getActivity(), jobItem.getJobName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity(), RecordsOverviewActivity.class);
        intent.putExtra("JOB_ITEM", jobItem);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (LocatorDevice.getInstance(getActivity()) != null) {
            if (LocatorDevice.getInstance(getActivity()).getState() == DeviceThreadControl.STATE_NONE) {
                LocatorDevice.getInstance(getActivity()).start();
                ;
            }
        }
        if (GPSDevice.getInstance(getActivity()) != null) {
            if (GPSDevice.getInstance(getActivity()).getState() == DeviceThreadControl.STATE_NONE) {
                GPSDevice.getInstance(getActivity()).start();
            }
        }
        updateMenuStatus();
    }

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        mConnectingAddress = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);

        mConnectingAddress = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        if (null == mConnectedDeviceAddress) {
            mConnectedDeviceAddress = new ArrayList<>();
        }
        boolean lIsNeedToConnect = true;
        for (String lMacId : mConnectedDeviceAddress) {
            if (lMacId.equalsIgnoreCase(mConnectingAddress)) {
                lIsNeedToConnect = false;
            }
        }
        if (lIsNeedToConnect) {
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mConnectingAddress);
            //Connect device
            if (deviceType == R.id.secure_connect_scan) {
                LocatorDevice.getInstance(getActivity()).connect(device, secure);
            } else {
                GPSDevice.getInstance(getActivity()).connect(device, secure);
            }
        }

        /*if ((mConnectedDeviceAddress == null) || (mConnectedDeviceAddress != null && !mConnectedDeviceAddress.equalsIgnoreCase(mConnectingAddress))) {

            // Get the BluetoothDevice object
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mConnectingAddress);

            //Connect device
            if (deviceType == R.id.secure_connect_scan) {
                LocatorDevice.getInstance(getActivity()).connect(device, secure);
            } else {
                GPSDevice.getInstance(getActivity()).connect(device, secure);
            }

        } else if ((mConnectedDeviceAddress != null && mConnectedDeviceAddress.equalsIgnoreCase(mConnectingAddress))) {

            //Disconnect device
            boolean disconnected = false;
            if (deviceType == R.id.secure_connect_scan) {
                disconnected = LocatorDevice.getInstance(getActivity()).disconnect();
            } else {
                disconnected = GPSDevice.getInstance(getActivity()).disconnect();
            }
            if (disconnected) {

                if (getActivity() != null && getActivity() instanceof AbstractBaseActivity)
                    ((AbstractBaseActivity) getActivity()).showMessage(getActivity().getString(R.string.device_disconnected), getActivity().getString(R.string.ok), null);

                mConnectedDeviceAddress = null;
            }
        }*/
        updateMenuStatus();
    }

    private void updateMenuStatus() {

        if (deviceType == R.id.secure_connect_scan) {
            if (LocatorDevice.getInstance(getActivity()).getState() == DeviceThreadControl.STATE_CONNECTED) {
                if (this.menu != null)
                    this.menu.findItem(R.id.secure_connect_scan).setIcon(getResources().getDrawable(R.drawable.bt_connect));
            } else {
                if (this.menu != null)
                    this.menu.findItem(R.id.secure_connect_scan).setIcon(getResources().getDrawable(R.drawable.bt_not));
            }
        } else {
            if (GPSDevice.getInstance(getActivity()).getState() == DeviceThreadControl.STATE_CONNECTED) {
                if (this.menu != null)
                    this.menu.findItem(R.id.secure_connect_scan_gps).setIcon(getResources().getDrawable(R.drawable.ic_device_gps_fixed));
            } else {
                if (this.menu != null)
                    this.menu.findItem(R.id.secure_connect_scan_gps).setIcon(getResources().getDrawable(R.drawable.ic_device_gps_off));
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.bluetooth_chat, menu);
        searchMenuHandle(menu);

        updateMenuStatus();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        deviceType = item.getItemId();
        switch (deviceType) {
            case R.id.secure_connect_scan:
            case R.id.secure_connect_scan_gps: {
                if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT_CON);
                    // Otherwise, setup the comm session
                } else {
                    // Launch the DeviceListActivity to see devices and do scan
                    Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                }
                return true;

                //activity.doAppDataSync();
            }
            case R.id.sync_btn: {
                activity.doAppDataSync();
            }

            /*case R.id.insecure_connect_scan: {
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
                return true;
            }
            case R.id.discoverable: {
                // Ensure this device is discoverable by others
                ensureDiscoverable();
                return true;
            }*/
        }
        return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a comm session
                    LocatorDevice.getInstance(getActivity());
                    GPSDevice.getInstance(getActivity());

                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");

                    if (activity != null) {
                        activity.showMessage(getString(R.string.bluetooth_is_not_enabled), getString(R.string.ok), null);
                    }

//                    getActivity().finish();
                }
            case REQUEST_ENABLE_BT_FAB:
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a comm session
                    LocatorDevice.getInstance(getActivity());
                    GPSDevice.getInstance(getActivity());

                    ((ContainerActivity) getActivity()).openJobCreateActivity();
                }
                break;
            case REQUEST_ENABLE_BT_SYNC:
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a comm session
                    LocatorDevice.getInstance(getActivity());
                    GPSDevice.getInstance(getActivity());

                    activity.doAppDataSync();
                }
                break;
            case REQUEST_ENABLE_BT_CON:
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a comm session
                    LocatorDevice.getInstance(getActivity());
                    GPSDevice.getInstance(getActivity());

                    // Launch the DeviceListActivity to see devices and do scan
                    Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                }
                break;
        }
    }

    private void searchMenuHandle(final Menu menu) {
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(new OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String searchStr) {
                //searchView.clearFocus();
                //jobSearchText(searchStr);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String searchStr) {
                //if (TextUtils.isEmpty(searchStr)){
                jobSearchText(searchStr);
                //}
                return false;
            }
        });
    }

    private void jobSearchText(String searchText) {
        List<Job> jobs;
        if (0 < searchText.length()) {
            jobs = activity.getLocalDB().getJobDao().getJobsByJobNameSearch(loggedInUserId, searchText, activity.getActiveJobStatuses());
        } else {
            jobs = activity.getLocalDB().getJobDao().getJobsByUserId(loggedInUserId, activity.getActiveJobStatuses());
        }
        refreshList(jobs);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private BroadcastReceiver listener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction().equalsIgnoreCase(Constants.ACTION_VALUE_DEVICE_CONNECTED)) {
                mConnectedDeviceAddress.add(mConnectingAddress);
                mConnectingAddress = null;
                if (deviceType == R.id.secure_connect_scan) {
                    menu.findItem(R.id.secure_connect_scan).setIcon(getResources().getDrawable(R.drawable.bt_connect));
                } else {
                    menu.findItem(R.id.secure_connect_scan_gps).setIcon(getResources().getDrawable(R.drawable.ic_device_gps_fixed));
                }
            } else if (intent != null && intent.getAction().equalsIgnoreCase(Constants.ACTION_VALUE_CONNECTION_LOST)) {
                if(null != mConnectedDeviceAddress) {
                    for (String lMacId : mConnectedDeviceAddress) {
                        if(lMacId.equalsIgnoreCase(mConnectingAddress)) {
                            mConnectedDeviceAddress.remove(mConnectingAddress);
                        }
                    }
                }
                if (deviceType == R.id.secure_connect_scan) {
                    menu.findItem(R.id.secure_connect_scan).setIcon(getResources().getDrawable(R.drawable.bt_not));
                }else{
                    menu.findItem(R.id.secure_connect_scan_gps).setIcon(getResources().getDrawable(R.drawable.ic_device_gps_off));
                }
                if(activity != null && activity instanceof AbstractBaseActivity)
                    ((AbstractBaseActivity)activity).showMessage(getString(R.string.not_able_connect_device),getString(R.string.ok),null);
            } else if(intent != null && intent.getAction().equalsIgnoreCase(Constants.ACTION_JOB_SYNCED)){
                getAllJobs();
            }
        }
    };
}
