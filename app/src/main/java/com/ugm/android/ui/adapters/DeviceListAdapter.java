package com.ugm.android.ui.adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ugm.android.R;
import com.ugm.android.bluetooth.GPSDevice;
import com.ugm.android.bluetooth.LocatorDevice;

import java.util.ArrayList;

public class DeviceListAdapter extends ArrayAdapter<BluetoothDevice> {

    private ArrayList<BluetoothDevice> mBluetoothDevices;


    public DeviceListAdapter(Context context, ArrayList<BluetoothDevice> bluetoothDeviceSet) {
        super(context, R.layout.device_list_row, bluetoothDeviceSet);
        this.mBluetoothDevices = bluetoothDeviceSet;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView deviceNameTxv;
        TextView deviceAddressTxv;
        ImageView statusImg;
    }

    public void setData(ArrayList<BluetoothDevice> bluetoothDeviceSet) {
        this.mBluetoothDevices = bluetoothDeviceSet;
    }

    @Override
    public int getCount() {
        if (mBluetoothDevices != null)
            return mBluetoothDevices.size();
        else
            return 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.device_list_row, parent, false);
            viewHolder.deviceNameTxv = (TextView) convertView.findViewById(R.id.device_name);
            viewHolder.deviceAddressTxv = (TextView) convertView.findViewById(R.id.device_address);
            viewHolder.statusImg = (ImageView) convertView.findViewById(R.id.status_img);

            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;

        }

        BluetoothDevice bluetoothDevice = getItem(position);

        if (bluetoothDevice != null) {
            viewHolder.deviceNameTxv.setText(bluetoothDevice.getName());
            viewHolder.deviceAddressTxv.setText(bluetoothDevice.getAddress());

            BluetoothDevice mConnectedDevice = LocatorDevice.getInstance(getContext()).getConnectedDevice();
            BluetoothDevice mGpsConnectedDevice = GPSDevice.getInstance(getContext()).getConnectedDevice();
            if (mConnectedDevice != null || mGpsConnectedDevice != null) {

                if (null != mConnectedDevice && mConnectedDevice.getAddress().equalsIgnoreCase(bluetoothDevice.getAddress())) {
                    viewHolder.statusImg.setVisibility(View.VISIBLE);
                } else if (null != mGpsConnectedDevice && mGpsConnectedDevice.getAddress().equalsIgnoreCase(bluetoothDevice.getAddress())) {
                    viewHolder.statusImg.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.statusImg.setVisibility(View.GONE);
                }
            } else {
                viewHolder.statusImg.setVisibility(View.GONE);
            }

        } else {
            viewHolder.deviceNameTxv.setText("");
            viewHolder.deviceAddressTxv.setText("");
            viewHolder.statusImg.setVisibility(View.GONE);
        }

        return convertView;

    }
}
