package com.ugm.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.ugm.android.R;
import com.ugm.android.database.entity.Job;
import com.ugm.android.database.entity.Record;
import com.ugm.android.datamodel.HeaderDataImpl;
import com.ugm.android.datamodel.ReportRecordModel;
import com.ugm.android.ui.adapters.ReportRecordViewAdapter;
import com.ugm.android.ui.view.stickyheader.StickHeaderItemDecoration;

import java.util.ArrayList;

public class ReportRodwiseViewActivity extends AbstractBaseActivity {

    private static final String TAG = "ReportRodwiseViewActivity";

    private Job job;
    private ArrayList<Record> records;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_rodwiseview_activity);
        processIntent(getIntent());
        initActionBar(getString(R.string.rodwise_report));
        initView();
    }

    private void processIntent(Intent intent){
        if(intent != null){
            if(intent != null){
                job =  (Job)intent.getSerializableExtra("JOB_ITEM");
                records =  (ArrayList<Record>) intent.getSerializableExtra("RECORD_LIST");
            }
        }
    }

    private void initView() {
        TextView titleTxt = findViewById(R.id.title_txt);
        titleTxt.setText(getString(R.string.rodwise_datapoints));

        RecyclerView recyclerView = findViewById(R.id.record_recycle_view);
        ReportRecordViewAdapter adapter = new ReportRecordViewAdapter(job);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        setData(adapter);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new StickHeaderItemDecoration(adapter));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void setData(ReportRecordViewAdapter adapter) {
        HeaderDataImpl headerData = new HeaderDataImpl(HeaderDataImpl.HEADER_TYPE, R.layout.report_record_view_adapter);
        ArrayList<ReportRecordModel> recordModels = ReportRecordModel.getRodwiseDataModel(records, false, job);
        adapter.setHeaderAndData(recordModels, headerData);
    }
}
