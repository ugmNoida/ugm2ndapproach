package com.ugm.android.ui.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.ugm.android.R;
import com.ugm.android.bluetooth.BluetoothUtils;
import com.ugm.android.ui.adapters.DeviceListAdapter;
import com.ugm.android.utilities.UGMMacros;

import java.util.ArrayList;
import java.util.Set;

public class DeviceListActivity extends AbstractBaseActivity {


    private static final String TAG = "DeviceListActivity";
    public static String EXTRA_DEVICE_ADDRESS = "device_address";
    private BluetoothAdapter mBtAdapter;
    private Set<BluetoothDevice> mPairedDevices;
    private ArrayList<BluetoothDevice> mDeviceList;

    private ListView pairedListView;
    private DeviceListAdapter mDeviceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setup the window
        setContentView(R.layout.activity_device_list);

        // Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);

        // Find and set up the ListView for paired devices
        pairedListView = (ListView) findViewById(R.id.paired_devices);
        pairedListView.setOnItemClickListener(mDeviceClickListener);

        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);

        mBtAdapter = BluetoothUtils.getAdapter(this);
        mDeviceList = new ArrayList<>();

        // Get a set of currently paired devices
        mPairedDevices = mBtAdapter.getBondedDevices();

        if (mPairedDevices.size() > 0) {
            for (BluetoothDevice device : mPairedDevices) {
                /*if(device.getName().equalsIgnoreCase("receiver")){
                    mDeviceList.add(device);
                }*/
                mDeviceList.add(device);
            }
        }

        mDeviceAdapter = new DeviceListAdapter(this, mDeviceList);
        pairedListView.setAdapter(mDeviceAdapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
        doDiscovery();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handleBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleBackPressed();
                break;
        }
        return true;
    }

    private void handleBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Make sure we're not doing discovery anymore
        if (mBtAdapter != null) {
            mBtAdapter.cancelDiscovery();
        }

        // Unregister broadcast listeners
        this.unregisterReceiver(mReceiver);
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private void doDiscovery() {
        Log.d(TAG, "doDiscovery()");

        // Indicate scanning in the title
        setProgressBarIndeterminateVisibility(true);
        setTitle(getString(R.string.scanning));

        // If we're already discovering, stop it
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }

        // Request discover from BluetoothAdapter
        mBtAdapter.startDiscovery();
    }

    /**
     * The on-click listener for all devices in the ListViews
     */
    private AdapterView.OnItemClickListener mDeviceClickListener
            = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
            mBtAdapter.cancelDiscovery();

            TextView nameTextView = v.findViewById(R.id.device_name);
            TextView addTxtView = v.findViewById(R.id.device_address);

            String info = nameTextView.getText().toString();
            String address = addTxtView.getText().toString();

            // Create the result Intent and include the MAC address
            Intent intent = new Intent();
            intent.putExtra(EXTRA_DEVICE_ADDRESS, address);

            // Set result and finish this Activity
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    };


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                setTitle(getString(R.string.select_device));
                if(device.getName() != null && device.getName().equalsIgnoreCase("receiver")){
                    /*if(mDeviceList != null && !mDeviceList.isEmpty()){
                        for(BluetoothDevice pairedDevice:mDeviceList){
                            if(!pairedDevice.getAddress().equalsIgnoreCase(device.getAddress())){
                                mDeviceList.add(device);
                            }
                        }
                    } else{
                        mDeviceList.add(device);
                    }*/
                    ArrayList<BluetoothDevice> bluetoothDevices = new ArrayList<>();
                    bluetoothDevices.addAll(mDeviceList);
                    if (mDeviceList != null && !mDeviceList.isEmpty()) {
                        for (BluetoothDevice pairedDevice : mDeviceList) {
                            if (!pairedDevice.getAddress().equalsIgnoreCase(device.getAddress())) {
                                bluetoothDevices.add(device);
                            }
                        }
                    } else {
                        bluetoothDevices.add(device);
                    }
                    mDeviceList.clear();
                    mDeviceList.addAll(bluetoothDevices);
                }

                //todo = notify adapter
                mDeviceAdapter.setData(mDeviceList);
                mDeviceAdapter.notifyDataSetChanged();

            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                setProgressBarIndeterminateVisibility(false);
                if (mDeviceList.size() == 0) {
                    setTitle(getString(R.string.no_device_found));
                } else {
                    setTitle(getString(R.string.select_device));
                    mDeviceAdapter.notifyDataSetChanged();
                }
            }
        }
    };

}
