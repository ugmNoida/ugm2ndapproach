package com.ugm.android.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ugm.android.R;
import com.ugm.android.ui.activities.ContainerActivity;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;
import com.ugm.android.webservice.JobResponse;
import com.ugm.android.webservice.PostSettingsData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsFragment extends Fragment implements View.OnClickListener {

    private static Logger logger = LoggerFactory.getLogger(SettingsFragment.class);

    private ContainerActivity activity;
    private Switch trackLocation, onlineOfflineSupport;
    private RadioGroup selectGpsRadioGroup;

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ContainerActivity)
            activity = (ContainerActivity)context;
        else{
            logger.error("onAttach context != ContainerActivity.");
            activity = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_settings, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        activity.getSupportActionBar().setTitle(getString(R.string.settings));
        activity.getSupportActionBar().setSubtitle("");
        Button submitBtn = (Button) view.findViewById(R.id.submit);
        submitBtn.setOnClickListener(this);

        trackLocation = (Switch)view.findViewById(R.id.track_location);
        selectGpsRadioGroup = (RadioGroup)view.findViewById(R.id.map_radio_group);
        final RadioButton defaultLocBtn = (RadioButton)selectGpsRadioGroup.findViewById(R.id.default_location);
        final RadioButton garminLocBtn = (RadioButton)selectGpsRadioGroup.findViewById(R.id.garmin_location);
        defaultLocBtn.setEnabled(true);

        boolean trackLoc = UGMUtility.getBooleanPreference(UGMMacros.PREF_TRACK_LOCATION, false);
        trackLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    selectGpsRadioGroup.setVisibility(View.VISIBLE);
                    String selectedLocation = UGMUtility.getStringPreference(UGMMacros.PREF_LOCATION, UGMMacros.VALUE_GPS_LOCATION);
                    if(selectedLocation.equalsIgnoreCase(UGMMacros.VALUE_GPS_LOCATION)) {
                        defaultLocBtn.setChecked(true);
                    }  else {
                        garminLocBtn.setChecked(true);
                    }
                } else {
                    selectGpsRadioGroup.setVisibility(View.GONE);
                }
            }
        });
        trackLocation.setChecked(trackLoc);

        onlineOfflineSupport = (Switch)view.findViewById(R.id.online_offline_switch);
        onlineOfflineSupport.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                UGMUtility.setBooleanPreference(UGMMacros.VALUE_ONLINE_SUPPORT, isChecked);
            }
        });
        onlineOfflineSupport.setChecked(UGMUtility.getBooleanPreference(UGMMacros.VALUE_ONLINE_SUPPORT, true));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        if(R.id.submit == view.getId()) {
            if(UGMUtility.isNetworkAvailable()) {
                boolean isChecked = trackLocation.isChecked();
                String prefLocation = "";
                int gpsSelectedBtn = selectGpsRadioGroup.getCheckedRadioButtonId();
                if (R.id.default_location == gpsSelectedBtn) {
                    prefLocation = UGMMacros.VALUE_GPS_LOCATION;
                } else if (R.id.garmin_location == gpsSelectedBtn) {
                    prefLocation = UGMMacros.VALUE_GARMIN_LOCAION;
                }
                postSettingsPref(isChecked, prefLocation);
            } else {
                activity.showMessage(getString(R.string.network_unavailable), getString(R.string.ok), null);
            }
        }
    }

    private void postSettingsPref(final boolean isLocationEnable, final String selectedLoc) {
        try {
            activity.showProgressDialog();
            final String token = "Bearer " + UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_SESSION_TOKEN, "");
            if (null != activity.getLoggedInUserId()) {
                JsonObject jsonObject = getSettingsJsonObj(isLocationEnable, selectedLoc);
                if (jsonObject != null && UGMUtility.isValidString(token)) {
                    PostSettingsData sendSettingsData = RetrofitClientInstance.getRetrofitInstance().create(PostSettingsData.class);
                    Call<JobResponse> settingsCall = sendSettingsData.sendSettingsData(token, jsonObject);
                    logger.info("#######SettingsFragment:postSettingsPref:Request JSON:"+new Gson().toJson(settingsCall.request()));
                    settingsCall.enqueue(new Callback<JobResponse>() {

                        @Override
                        public void onResponse(Call<JobResponse> call, Response<JobResponse> response) {
                            JobResponse jobResponse = response.body();
                            logger.info("#######SettingsFragment:postSettingsPref:Response JSON:"+new Gson().toJson(jobResponse));
                            activity.hideProgressDialog();
                            if (jobResponse != null) {
                                UGMUtility.setBooleanPreference(UGMMacros.PREF_TRACK_LOCATION, isLocationEnable);
                                UGMUtility.setStringPreference(UGMMacros.PREF_LOCATION, selectedLoc);
                                activity.showMessage(getString(R.string.all_saved), getString(R.string.ok_caps), null);
                            } else if(null != response.errorBody()) {
                                String errorMessage = activity.parseErrorMessage(response.errorBody());
                                activity.showMessage(errorMessage, getString(R.string.ok), null);
                            } else {
                                activity.showMessage(getString(R.string.something_went_wrong), getString(R.string.ok), null);
                            }
                        }

                        @Override
                        public void onFailure(Call<JobResponse> call, Throwable t) {
                            activity.hideProgressDialog();
                            activity.showMessage(getString(R.string.something_went_wrong), getString(R.string.ok), null);
                        }
                    });
                }
            }
        } catch(Exception ex) {
            activity.hideProgressDialog();
            activity.showMessage(getString(R.string.something_went_wrong), getString(R.string.ok), null);
        }
    }

    private JsonObject getSettingsJsonObj(boolean isLocationEnable, String selectedLoc) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", activity.getLoggedInUserId());
        jsonObject.addProperty("requiredLocation", isLocationEnable);
        jsonObject.addProperty("whichLocation", selectedLoc);
        return jsonObject;
    }
}
