package com.ugm.android.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ugm.android.R;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMUtility;
import com.ugm.android.webservice.ForgotPassword;
import com.ugm.android.webservice.GetForgotPasswordData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AbstractBaseActivity implements View.OnClickListener {

    private static Logger logger = LoggerFactory.getLogger(ForgotPasswordActivity.class);

    private TextInputEditText editEmail;
    private TextInputLayout txtEmail;
    private Button sendPassBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initActionBar(getString(R.string.title_activity_fpd));

        editEmail = (TextInputEditText)findViewById(R.id.edit_email);
        txtEmail = (TextInputLayout)findViewById(R.id.text_email);
        sendPassBtn = (Button)findViewById(R.id.forgot_password_btn);
        sendPassBtn.setOnClickListener(this);
    }

    private String doEmailValidation() {
        String emailId = removePrefixEmail(editEmail.getText().toString());
        if(TextUtils.isEmpty(emailId)) {
            txtEmail.setError(getString(R.string.please_enter_email_id));
            emailId = null;
        } else if(!isEmailValid(emailId)) {
            txtEmail.setError(getString(R.string.please_enter_valid_email_if));
            emailId = null;
        }
        return emailId;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        final String emailId = doEmailValidation();
        if(UGMUtility.isValidString(emailId)) {
            if(UGMUtility.isNetworkAvailable()) {
                try {
                    JsonObject request = new JsonObject();
                    request.addProperty("email", emailId);
                    logger.info("EmailId:"+emailId);
                    GetForgotPasswordData service = RetrofitClientInstance.getRetrofitInstance().create(GetForgotPasswordData.class);
                    Call<ForgotPassword> call = service.getForgotPasswordData(request);
                    showProgressDialog();
                    call.enqueue(new Callback<ForgotPassword>() {
                        @Override
                        public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {
                            hideProgressDialog();
                            ForgotPassword login = response.body();
                            logger.info("#######ForgotPassword:Response JSON:"+new Gson().toJson(login));
                            if(login != null){
                                if(response.body().getSuccess() != null && login.getSuccess().equalsIgnoreCase("true")) {
                                    showSuccessDialog(emailId);
                                } else {
                                    logger.info("onResponse:Invalid EmailId");
                                    showMessage(String.format(getString(R.string.invalid_emailid), emailId),
                                            getString(R.string.ok),null);
                                }
                            } else {
                                logger.info("onResponse:Something went wrong.");
                                showMessage(getString(R.string.something_went_wrong), getString(R.string.ok),null);
                            }
                        }
                        @Override
                        public void onFailure(Call<ForgotPassword> call, Throwable t) {
                            logger.info("onFailure:Something went wrong.");
                            hideProgressDialog();
                            showMessage(getString(R.string.something_went_wrong), getString(R.string.ok),null);
                        }
                    });
                } catch(Exception ex) {
                    logger.info("Exception while send forgot password:" + ex.toString());
                }
            } else {
                showMessage(getString(R.string.network_unavailable), getString(R.string.ok), null);
            }
        }
    }

    private void showSuccessDialog(String emailId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_activity_fpd));
        builder.setMessage(String.format(getString(R.string.forgot_password_success_txt), emailId));
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }
}
