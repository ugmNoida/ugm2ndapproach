package com.ugm.android.ui.fragments;

import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.ugm.android.R;
import com.ugm.android.UGMApplication;
import com.ugm.android.database.entity.Record;
import com.ugm.android.listeners.UGMLocationListener;
import com.ugm.android.ui.activities.RecordsOverviewActivity;
import com.ugm.android.utilities.UGMMacros;

import java.util.ArrayList;

public class JobMapViewFragment extends Fragment implements OnMapReadyCallback, UGMLocationListener {

    private static final String TAG = "JobMapViewFragment";

    private OnFragmentInteractionListener mListener;

    private RecordsOverviewActivity mActivity;
    private ArrayList<LatLng> mListLatLng;
    private SupportMapFragment mapFragment;
    private GoogleMap mGoogleMap = null;

    public JobMapViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_job_map_view, container, false);
        view.findViewById(R.id.ugmmap).setVisibility(View.VISIBLE);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.ugmmap);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAllRecords();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (RecordsOverviewActivity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void addMarkersToMap() {
        if (mGoogleMap != null && mListLatLng != null && mListLatLng.size() > 0) {
            double prevLatitude = 0, prevLongitude = 0;

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            int totalSize = mListLatLng.size();
            int index = 0;
            for (int i = 0; i < totalSize; i++) {
                LatLng latLng = mListLatLng.get(i);
                if (i == 0) {
                    mGoogleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.startpointer)).zIndex(totalSize));
                } else if (i == (totalSize - 1)) {
                    mGoogleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.endpointer)).zIndex(totalSize - i));
                } else {
                    mGoogleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.markerpointer)).zIndex(totalSize - i));
                }
                builder.include(latLng);

                if (index > 0 && index < totalSize) {
                    mGoogleMap.addPolyline(new PolylineOptions()
                            .add(new LatLng(prevLatitude, prevLongitude), new LatLng(latLng.latitude, latLng.longitude))
                            .width(5)
                            .color(ResourcesCompat.getColor(getResources(), R.color.map_line_color, null)));
                }

                prevLatitude = latLng.latitude;
                prevLongitude = latLng.longitude;
                index++;
            }
            LatLngBounds bounds = builder.build();
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 40));
        }
    }

    private void getAllRecords() {
        mActivity.showProgressDialog();
        //TODO: Move to local database code
        fetchAllRecords((ArrayList<Record>) UGMApplication.getInstance().getDatabase().getRecordDao()
                .getSortedRecordsByJobId(mActivity.getJobItem().getJobId(), UGMMacros.RECORD_DELETED));
    }

    public void fetchAllRecords(ArrayList<Record> records) {
        if (records != null && records.size() > 0) {
            mListLatLng = null;
            mListLatLng = new ArrayList<>();
            for (Record record : records) {
                Double lat = 0.0, lng = 0.0;
                if (!TextUtils.isEmpty(record.getLatitude()))
                    lat = Double.parseDouble(record.getLatitude());

                if (!TextUtils.isEmpty(record.getLongitude()))
                    lng = Double.parseDouble(record.getLongitude());

                if (lat != 0.0 && lng != 0.0)
                    mListLatLng.add(new LatLng(lat, lng));
                else
                    Log.e(TAG, "Lat and Lng value == 0.0");
            }
        } else {
            mActivity.hideProgressDialog();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mActivity.hideProgressDialog();
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        addMarkersToMap();
    }

    @Override
    public void locationCallBack(Location location) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
