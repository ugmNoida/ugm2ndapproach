package com.ugm.android.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ugm.android.R;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMUtility;
import com.ugm.android.webservice.GetRegisterData;
import com.ugm.android.webservice.RegisterData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateAccountActivity extends AbstractBaseActivity implements View.OnClickListener {

    private static Logger logger = LoggerFactory.getLogger(CreateAccountActivity.class);

    private ArrayList<String> countryList = new ArrayList<>();
    private Spinner countryName;
    private TextView selectCnt;
    private TextInputLayout phoneLayout, fullNameLayout, emailLayout, passLayout, newPassLayout, companyLayout;
    private TextInputEditText etPhoneTxt, etFullName, etEmail, etPassword, etNewPassword, etCompany;
    private Button btnRegister;
    private String selectedCountry;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        initActionBar(getString(R.string.title_activity_ca));
        prepareCountryList();
        initView();
    }

    private void initView() {
        btnRegister = (Button) findViewById(R.id.register_btn);
        btnRegister.setOnClickListener(this);
        selectCnt = (TextView) findViewById(R.id.txt_select_country);
        fullNameLayout = (TextInputLayout) findViewById(R.id.txt_fullname);
        etFullName = (TextInputEditText) findViewById(R.id.edit_fullname);
        emailLayout = (TextInputLayout) findViewById(R.id.text_email);
        etEmail = (TextInputEditText) findViewById(R.id.edit_email);
        passLayout = (TextInputLayout) findViewById(R.id.text_new_password);
        etPassword = (TextInputEditText) findViewById(R.id.edit_new_password);
        newPassLayout = (TextInputLayout) findViewById(R.id.text_conf_new_password);
        etNewPassword = (TextInputEditText) findViewById(R.id.edit_conf_new_password);
        companyLayout = (TextInputLayout) findViewById(R.id.txt_company_name);
        etCompany = (TextInputEditText) findViewById(R.id.edit_company_name);
        phoneLayout = (TextInputLayout) findViewById(R.id.txt_phone_number);
        etPhoneTxt = (TextInputEditText) findViewById(R.id.edit_phone_number);
        countryName = (Spinner) findViewById(R.id.country_spinner);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countryList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryName.setAdapter(adapter);
        countryName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String localValue = (String) parent.getItemAtPosition(position);
                if (!localValue.equalsIgnoreCase(getString(R.string.select_country))) {
                    selectCnt.setTextColor(Color.parseColor("#61000000"));
                    selectCnt.setVisibility(View.VISIBLE);
                    selectedCountry = parseCountryName(localValue);
                } else {
                    selectedCountry = null;
                    selectCnt.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    //(+43) - Austria to Austria
    private String parseCountryName(String cntryName) {
        String actCntryName = "";
        if(!TextUtils.isEmpty(cntryName)) {
            int index = cntryName.indexOf("-");
            actCntryName = cntryName.substring(index+1, cntryName.length());
        }
        return actCntryName.trim();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.register_btn) {
            if(UGMUtility.isNetworkAvailable()) {
                JsonObject request = validateAllFields();
                if(null != request) {
                    try {
                        GetRegisterData service = RetrofitClientInstance.getRetrofitInstance().create(GetRegisterData.class);
                        Call<RegisterData> call = service.getRegisterData(request);
                        showProgressDialog();
                        call.enqueue(new Callback<RegisterData>() {
                            @Override
                            public void onResponse(Call<RegisterData> call, Response<RegisterData> response) {
                                hideProgressDialog();
                                RegisterData register = response.body();
                                logger.info("#######Create Account:Response JSON:"+new Gson().toJson(register));
                                if(register != null){
                                    if(response.body().getSuccess() != null && register.getSuccess().equalsIgnoreCase("true")) {
                                        showSuccessDialog();
                                    } else {
                                        logger.info("onResponse:RegisterData:" + register.getMessage());
                                        showMessage(register.getMessage(), getString(R.string.ok),null);
                                    }
                                } else {
                                    if(null != response && null != response.message()) {
                                        showMessage(response.message(), getString(R.string.ok),null);
                                    } else {
                                        logger.info("onResponse:Something went wrong.");
                                        showMessage(getString(R.string.something_went_wrong), getString(R.string.ok), null);
                                    }
                                }
                            }
                            @Override
                            public void onFailure(Call<RegisterData> call, Throwable t) {
                                logger.info("onFailure:Something went wrong.");
                                hideProgressDialog();
                                showMessage(getString(R.string.something_went_wrong), getString(R.string.ok),null);
                            }
                        });
                    } catch(Exception ex) {

                    }
                }
            } else {
                showMessage(getString(R.string.network_unavailable), getString(R.string.ok), null);
            }
        }
    }

    private void showSuccessDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_activity_ca));
        builder.setMessage(getString(R.string.create_account_success_txt));
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private JsonObject validateAllFields() {
        JsonObject request = null;
        String fullName = etFullName.getText().toString();
        fullNameLayout.setError(null);
        fullNameLayout.setErrorEnabled(false);
        String emailId = removePrefixEmail(etEmail.getText().toString());
        emailLayout.setError(null);
        emailLayout.setErrorEnabled(false);
        String password = etPassword.getText().toString();
        passLayout.setError(null);
        passLayout.setErrorEnabled(false);
        String confPassword = etNewPassword.getText().toString();
        newPassLayout.setError(null);
        newPassLayout.setErrorEnabled(false);
        String company = etCompany.getText().toString();
        companyLayout.setError(null);
        companyLayout.setErrorEnabled(false);
        String phoneNumber = etPhoneTxt.getText().toString();
        phoneLayout.setError(null);
        phoneLayout.setErrorEnabled(false);

        if(TextUtils.isEmpty(fullName)) {
            request = null;
            fullNameLayout.setErrorEnabled(true);
            fullNameLayout.setError(getString(R.string.please_enter_fullname));
        } else if(TextUtils.isEmpty(emailId)) {
            request = null;
            emailLayout.setErrorEnabled(true);
            emailLayout.setError(getString(R.string.please_enter_email_id));
        } else if(!isEmailValid(emailId)) {
            request = null;
            emailLayout.setErrorEnabled(true);
            emailLayout.setError(getString(R.string.please_enter_valid_email_if));
        } else if(TextUtils.isEmpty(password)) {
            request = null;
            passLayout.setErrorEnabled(true);
            passLayout.setError(getString(R.string.please_enter_valid_password));
        } else if(!isPasswordValid(password)) {
            request = null;
            passLayout.setErrorEnabled(true);
            passLayout.setError(getString(R.string.password_must_greater_five));
        } else if(TextUtils.isEmpty(confPassword)) {
            request = null;
            newPassLayout.setErrorEnabled(true);
            newPassLayout.setError(getString(R.string.please_enter_valid_password));
        } else if(!password.equals(confPassword)) {
            request = null;
            passLayout.setErrorEnabled(true);
            passLayout.setError(getString(R.string.password_mismatch));
        } else if(TextUtils.isEmpty(company)) {
            request = null;
            companyLayout.setErrorEnabled(true);
            companyLayout.setError(getString(R.string.please_enter_company));
        } else if (TextUtils.isEmpty(selectedCountry)){
            selectCnt.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            selectCnt.setVisibility(View.VISIBLE);
        } else if(phoneNumber.length() < 10 || phoneNumber.length() > 15) {
            request = null;
            phoneLayout.setErrorEnabled(true);
            phoneLayout.setError(getString(R.string.please_enter_phone));
        } else {
            request = new JsonObject();
            request.addProperty("fullName", fullName);
            request.addProperty("email", emailId);
            request.addProperty("password", password);
            request.addProperty("companyName", company);
            request.addProperty("countryName", selectedCountry);
            request.addProperty("contactNumber", phoneNumber);
            request.addProperty("role", "user");
        }
        return request;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void prepareCountryList() {
        if(0 == countryList.size()) {
            countryList.add(getString(R.string.select_country));
            countryList.add("(+355) - Albanien");
            countryList.add("(+376) - Andorra");
            countryList.add("(+374) - Armenien");
            countryList.add("(+43) - Austria");
            countryList.add("(+61) - Australia");
            countryList.add("(+880) - Bangladesh");
            countryList.add("(+375) - Belarus");
            countryList.add("(+32) - Belgien");
            countryList.add("(+387) - Bosnien och Hercegovina");
            countryList.add("(+359) - Bulgarien");
            countryList.add("(+1) - Canada");
            countryList.add("(+357) - Cypern");

            //countryList.add("(+86) - China");

            countryList.add("(+45) - Denmark");
            countryList.add("(+372) - Estland");
            countryList.add("(+372) - Estonia");
            countryList.add("(+298) - Färöarna");
            countryList.add("(+358) - Finland");
            countryList.add("(+32) - France Belgium");
            countryList.add("(+33) - Frankrike");
            countryList.add("(+49) - Germany");
            countryList.add("(+350) - Gibraltar");
            countryList.add("(+30) - Grekland");
            countryList.add("(+36) - Hungary");
            countryList.add("(+91) - India");
            countryList.add("(+353) - Ireland");
            countryList.add("(+354) - Island");
            countryList.add("(+39) - Italien");
            countryList.add("(+39) - Italy");
            countryList.add("(+381) - Kosovo");
            countryList.add("(+385) - Kroatien");
            countryList.add("(+371) - Latvia");
            countryList.add("(+371) - Lettland");
            countryList.add("(+423) - Liechtenstein");
            countryList.add("(+370) - Litauen");
            countryList.add("(+370) - Lithuania");
            countryList.add("(+352) - Luxemburg");
            countryList.add("(+389) - Makedonien");
            countryList.add("(+356) - Malta");
            countryList.add("(+52) - Mexico");
            countryList.add("(+373) - Moldavien");
            countryList.add("(+377) - Monaco");
            countryList.add("(+382) - Montenegro");
            countryList.add("(+31) - Nederländerna");
            countryList.add("(+31) - Netherlands");
            countryList.add("(+64) - New Zealand");
            countryList.add("(+47) - Norge");
            countryList.add("(+47) - Norway");
            countryList.add("(+43) - Österrike");
            countryList.add("(+48) - Poland");
            countryList.add("(+48) - Polen");
            countryList.add("(+351) - Portugal");
            countryList.add("(+40) - Romania");
            countryList.add("(+40) - Rumänien");
            countryList.add("(+34) - Russia");
            countryList.add("(+7) - Ryssland");
            countryList.add("(+378) - San Marino");
            countryList.add("(+41) - Schweiz");
            countryList.add("(+381) - Serbien");
            countryList.add("(+421) - Slovakia");
            countryList.add("(+421) - Slovakien");
            countryList.add("(+386) - Slovenien");
            countryList.add("(+7) - Spain");
            countryList.add("(+34) - Spanien");
            countryList.add("(+44) - Storbritannien och Nordirland");
            countryList.add("(+46) - Sverige");
            countryList.add("(+46) - Sweden");
            countryList.add("(+41) - Switzerland");
            countryList.add("(+420) - Tjeckien");
            countryList.add("(+90) - Turkiet");
            countryList.add("(+49) - Tyskland");
            countryList.add("(+971) - UAE");
            countryList.add("(+380) - Ukraina");
            countryList.add("(+380) - Ukraine");
            countryList.add("(+36) - Ungern");
            countryList.add("(+44) - United Kingdom");
            countryList.add("(+1) - USA");
            countryList.add("(+39) - Vatikanstaten");
            countryList.add("(+375) - Vitryssland");
        }
    }
}
