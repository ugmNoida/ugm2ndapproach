package com.ugm.android.ui.adapters;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ugm.android.R;
import com.ugm.android.database.entity.Job;
import com.ugm.android.datamodel.HeaderDataImpl;
import com.ugm.android.datamodel.ReportRecordModel;
import com.ugm.android.ui.view.stickyheader.StickHeaderRecyclerView;
import com.ugm.android.utilities.UGMUtility;

public class ReportRecordViewAdapter extends StickHeaderRecyclerView<ReportRecordModel, HeaderDataImpl> {

    private Job job;

    public ReportRecordViewAdapter(Job job) {
        this.job = job;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case HeaderDataImpl.HEADER_TYPE:
                return new HeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.report_record_view_adapter, parent, false));
            default:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.report_record_view_adapter, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bindData(position);
        } else if (holder instanceof HeaderViewHolder){
            ((HeaderViewHolder) holder).bindData(position);
        }
    }

    @Override
    public void bindHeaderData(View header, int headerPosition) {
        TextView rodNumber, rodLength, boreLength, xDistance, pitch, relDepth, depth, relElevation, recordDate;
        rodNumber = (TextView) header.findViewById(R.id.rod_number);
        rodLength = (TextView) header.findViewById(R.id.rod_length);
        boreLength = (TextView) header.findViewById(R.id.bore_length);
        xDistance = (TextView) header.findViewById(R.id.x_distance);
        pitch = (TextView) header.findViewById(R.id.pitch);
        relDepth = (TextView) header.findViewById(R.id.relative_depth);
        depth = (TextView) header.findViewById(R.id.depth);
        relElevation = (TextView) header.findViewById(R.id.relative_elevation);
        recordDate = (TextView) header.findViewById(R.id.record_created_dttm);

        header.setBackgroundColor(Color.parseColor("#ececec"));
        rodNumber.setTextColor(Color.parseColor("#b82d46"));
        rodNumber.setTypeface(rodNumber.getTypeface(), Typeface.BOLD);
        rodLength.setTextColor(Color.parseColor("#b82d46"));
        rodLength.setTypeface(rodLength.getTypeface(), Typeface.BOLD);
        boreLength.setTextColor(Color.parseColor("#b82d46"));
        boreLength.setTypeface(boreLength.getTypeface(), Typeface.BOLD);
        xDistance.setTextColor(Color.parseColor("#b82d46"));
        xDistance.setTypeface(xDistance.getTypeface(), Typeface.BOLD);
        pitch.setTextColor(Color.parseColor("#b82d46"));
        pitch.setTypeface(pitch.getTypeface(), Typeface.BOLD);
        relDepth.setTextColor(Color.parseColor("#b82d46"));
        relDepth.setTypeface(relDepth.getTypeface(), Typeface.BOLD);
        depth.setTextColor(Color.parseColor("#b82d46"));
        depth.setTypeface(depth.getTypeface(), Typeface.BOLD);
        relElevation.setTextColor(Color.parseColor("#b82d46"));
        relElevation.setTypeface(relElevation.getTypeface(), Typeface.BOLD);
        recordDate.setTextColor(Color.parseColor("#b82d46"));
        recordDate.setTypeface(recordDate.getTypeface(), Typeface.BOLD);

        ReportRecordModel model = ReportRecordModel.getHeaderText(job);
        rodNumber.setText(model.getRodNumber());
        rodLength.setText(model.getRodLength());
        boreLength.setText(model.getBoreLength());
        xDistance.setText(model.getxDistance());
        pitch.setText(model.getAvgPitch());
        relDepth.setText(model.getRelativeDepth());
        depth.setText(model.getDepthInPdf());
        relElevation.setText(model.getRelativeElevation());
        recordDate.setText(model.getRecordCreatedDTTM());
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView rodNumber, rodLength, boreLength, xDistance, pitch, relDepth, depth, relElevation, recordDate;

        HeaderViewHolder(View itemView) {
            super(itemView);
            rodNumber = (TextView) itemView.findViewById(R.id.rod_number);
            rodLength = (TextView) itemView.findViewById(R.id.rod_length);
            boreLength = (TextView) itemView.findViewById(R.id.bore_length);
            xDistance = (TextView) itemView.findViewById(R.id.x_distance);
            pitch = (TextView) itemView.findViewById(R.id.pitch);
            relDepth = (TextView) itemView.findViewById(R.id.relative_depth);
            depth = (TextView) itemView.findViewById(R.id.depth);
            relElevation = (TextView) itemView.findViewById(R.id.relative_elevation);
            recordDate = (TextView) itemView.findViewById(R.id.record_created_dttm);
        }

        void bindData(int position) {
            ReportRecordModel model = ReportRecordModel.getHeaderText(job);
            rodNumber.setText(model.getRodNumber());
            rodLength.setText(model.getRodLength());
            boreLength.setText(model.getBoreLength());
            xDistance.setText(model.getxDistance());
            pitch.setText(model.getAvgPitch());
            relDepth.setText(model.getRelativeDepth());
            depth.setText(model.getDepthInPdf());
            relElevation.setText(model.getRelativeElevation());
            recordDate.setText(model.getRecordCreatedDTTM());
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView rodNumber, rodLength, boreLength, xDistance, pitch, relDepth, depth, relElevation, recordDate;

        ViewHolder(View itemView) {
            super(itemView);
            rodNumber = (TextView) itemView.findViewById(R.id.rod_number);
            rodLength = (TextView) itemView.findViewById(R.id.rod_length);
            boreLength = (TextView) itemView.findViewById(R.id.bore_length);
            xDistance = (TextView) itemView.findViewById(R.id.x_distance);
            pitch = (TextView) itemView.findViewById(R.id.pitch);
            relDepth = (TextView) itemView.findViewById(R.id.relative_depth);
            depth = (TextView) itemView.findViewById(R.id.depth);
            relElevation = (TextView) itemView.findViewById(R.id.relative_elevation);
            recordDate = (TextView) itemView.findViewById(R.id.record_created_dttm);
        }

        void bindData(int position) {
            if(!getDataInPosition(position).isHeader()) {
                ReportRecordModel recordModel = getDataInPosition(position);
                rodNumber.setText(recordModel.getRodNumber());
                rodLength.setText(recordModel.getRodLength());
                boreLength.setText(recordModel.getBoreLength());
                recordDate.setText(recordModel.getRecordCreatedDTTM());
                if(UGMUtility.isValidFloatString(recordModel.getPitch())) {
                    xDistance.setText(recordModel.getxDistance());
                    pitch.setText(recordModel.getAvgPitch());
                    relDepth.setText(recordModel.getRelativeDepth());
                    depth.setText(recordModel.getDepthInPdf());
                    relElevation.setText(recordModel.getRelativeElevation());
                } else {
                    xDistance.setText("");
                    pitch.setText("");
                    relDepth.setText("");
                    depth.setText("");
                    relElevation.setText("");
                }
            }
        }
    }
}
