package com.ugm.android.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ugm.android.R;
import com.ugm.android.bluetooth.BluetoothUtils;
import com.ugm.android.bluetooth.GPSDevice;
import com.ugm.android.bluetooth.LocatorDevice;
import com.ugm.android.database.entity.Record;
import com.ugm.android.ui.activities.RecordCreateActivity;
import com.ugm.android.ui.activities.RecordsOverviewActivity;
import com.ugm.android.ui.adapters.RecordsViewAdapter;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;
import com.ugm.android.webservice.JobResponse;
import com.ugm.android.webservice.SendRecordData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecordsViewFragment extends Fragment implements View.OnClickListener, RecordsViewAdapter.OnListItemtInteractionListener {

    private static Logger logger = LoggerFactory.getLogger(RecordsViewFragment.class);

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private Button addRecordBtn;
    private RecyclerView recyclerView;
    private RecordsViewAdapter mAdapter;
    private RecordsOverviewActivity mActivity;
    private BluetoothAdapter mBluetoothAdapter = null;

    public RecordsViewFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static RecordsViewFragment newInstance(int columnCount) {
        RecordsViewFragment fragment = new RecordsViewFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        mBluetoothAdapter = BluetoothUtils.getAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recodsview_list, container, false);
        addRecordBtn = (Button) view.findViewById(R.id.add_new_record_btn);
        // Set the adapter
        Context context = view.getContext();
        mAdapter = null;
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        if (UGMMacros.JOB_COMPLETED.equalsIgnoreCase(mActivity.getJobItem().getStatus())) {
            addRecordBtn.setVisibility(View.GONE);
        } else {
            addRecordBtn.setVisibility(View.VISIBLE);
            addRecordBtn.setOnClickListener(this);
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getAllRecords();
    }

    private void getAllRecords() {
        List<Record> records = mActivity.getLocalDB().getRecordDao().getRecordsByJobIdNotStatus(mActivity.getJobItem().getJobId(), UGMMacros.RECORD_DELETED);
        if (null != records && 0 < records.size()) {
            mActivity.setRecords(records);
            mAdapter = new RecordsViewAdapter(mActivity.getRecords(), this, mActivity);
            recyclerView.setAdapter(mAdapter);
            /*if (mAdapter == null) {
                mAdapter = new RecordsViewAdapter(mActivity.getRecords(), this, mActivity);
                recyclerView.setAdapter(mAdapter);
            } else {
                mAdapter.refreshListData(mActivity.getRecords());
                mAdapter.notifyDataSetChanged();
            }*/
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (RecordsOverviewActivity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.add_new_record_btn) {
            if (null != mBluetoothAdapter && !mBluetoothAdapter.isEnabled()) {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, 1);
                // Otherwise, setup the comm session
            } else {
                Intent intent = new Intent(getActivity(), RecordCreateActivity.class);
                intent.putExtra("JOB_ITEM", mActivity.getJobItem());
                startActivity(intent);
            }
        }
    }

    @Override
    public void onDeleteRecord(final Record record) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(getString(R.string.are_you_deleting_rod_number) + " " + record.getRodNumber() + " " + getString(R.string.please_confirm));
        builder.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                record.setStatus(UGMMacros.RECORD_DELETED);
                record.setSynced(false);
                mActivity.getLocalDB().getRecordDao().update(record);
                if (UGMUtility.isNetworkAvailable() && UGMUtility.getBooleanPreference(UGMMacros.VALUE_ONLINE_SUPPORT, true)) {
                    deleteRecord(record);
                } else {
                    logger.info("RecordViewFragment:Internet is not available to upload to server for RodNumber:" + record.getRodNumber());
                    mActivity.showMessage(getString(R.string.rod_number_text) + record.getRodNumber()
                            + " " + getString(R.string.is_deleted_successfully), getString(R.string.ok_caps), null);
                }
                getAllRecords();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    @Override
    public void onEditRecord(Record record) {
        editRecord(record);
    }

    private void editRecord(final Record record) {
        boolean isLastRecord = false;
        AlertDialog.Builder alertDialogBldr = new AlertDialog.Builder(mActivity);
        alertDialogBldr.setTitle(getString(R.string.edit_rodwise_details));
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.edit_record_dialog, null);
        final TextInputEditText pitchTxt = (TextInputEditText) view.findViewById(R.id.pitch_txt);
        pitchTxt.setText(record.getPitch());
        //final TextInputLayout pitchLayout = (TextInputLayout)view.findViewById(R.id.pitch_layout);
        //validateErrorCheck(pitchLayout);
        final TextInputEditText depthTxt = (TextInputEditText) view.findViewById(R.id.depth_txt);
        if (!TextUtils.isEmpty(record.getDepth()))
            //if(!record.isOnlyPitch())
            depthTxt.setText(record.getDepth());
        //final TextInputLayout depthLayout = (TextInputLayout)view.findViewById(R.id.depth_layout);
        //validateErrorCheck(depthLayout);
        final TextInputEditText rollTxt = (TextInputEditText) view.findViewById(R.id.roll_txt);
        if (!TextUtils.isEmpty(record.getRoll()))
            //if(!record.isOnlyPitch())
            rollTxt.setText(record.getRoll());
        //final TextInputLayout rollLayout = (TextInputLayout)view.findViewById(R.id.roll_layout);
        //validateErrorCheck(rollLayout);

        final TextInputEditText tempTxt = (TextInputEditText) view.findViewById(R.id.temp_txt);
        if (!TextUtils.isEmpty(record.getTemperature()))
            tempTxt.setText(record.getTemperature());
        //final TextInputLayout tempLayout = (TextInputLayout)view.findViewById(R.id.temp_layout);

        final TextInputLayout relElevLayout = (TextInputLayout) view.findViewById(R.id.rel_elev_layout);
        final TextInputEditText relElevTxt = (TextInputEditText) view.findViewById(R.id.rel_elev_txt);

        final TextInputEditText defaultRodTxt = (TextInputEditText) view.findViewById(R.id.default_rod_txt);
        defaultRodTxt.setText(record.getRodLength());
        final TextInputLayout defaultRodLayout = (TextInputLayout) view.findViewById(R.id.default_rod_layout);
        //validateErrorCheck(defaultRodLayout);
        if (mActivity.getRecords().size() - 1 == record.getRodNumber() || record.getRodNumber() == 1) {
            defaultRodLayout.setVisibility(View.VISIBLE);
            isLastRecord = true;
        } else {
            defaultRodLayout.setVisibility(View.GONE);
            isLastRecord = false;
        }
        if (record.getRodNumber() == 0) {
            relElevLayout.setVisibility(View.VISIBLE);
            relElevTxt.setText(record.getRelativeElevation());
        } else {
            relElevLayout.setVisibility(View.GONE);
        }
        RelativeLayout latlongLayout = (RelativeLayout) view.findViewById(R.id.latlong_layout);
        final TextInputLayout latitudeLayout = (TextInputLayout) view.findViewById(R.id.latitude_layout);
        latitudeLayout.setEnabled(false);
        final TextInputLayout longitudeLayout = (TextInputLayout) view.findViewById(R.id.longitude_layout);
        longitudeLayout.setEnabled(false);
        final TextInputEditText latiTxt = (TextInputEditText) view.findViewById(R.id.latitude_txt);
        final TextInputEditText longiTxt = (TextInputEditText) view.findViewById(R.id.longitude_txt);
        ImageButton latlongClear = (ImageButton) view.findViewById(R.id.latlong_clear);
        if (UGMUtility.isGoogleMapEnabled()) {
            latlongLayout.setVisibility(View.VISIBLE);
            latiTxt.setText(record.getLatitude());
            longiTxt.setText(record.getLongitude());
            if (!UGMUtility.isValidString(latiTxt.getText().toString()) ||
                    !UGMUtility.isValidString(longiTxt.getText().toString())) {
                latlongLayout.setVisibility(View.GONE);
            } else {
                latlongClear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        latitudeLayout.setError(getString(R.string.deleted_lat_cannot_recovered));
                        longitudeLayout.setError(getString(R.string.deleted_long_cannot_be_recovered));
                        latiTxt.setText("");
                        longiTxt.setText("");
                    }
                });
            }
        } else {
            latlongLayout.setVisibility(View.GONE);
        }

        /*if(record.isOnlyPitch()) {
            depthLayout.setEnabled(false);
            depthTxt.setEnabled(false);
            rollLayout.setEnabled(false);
            rollTxt.setEnabled(false);
            defaultRodLayout.setEnabled(false);
            defaultRodTxt.setEnabled(false);
        }*/
        alertDialogBldr.setView(view);
        alertDialogBldr.setNegativeButton(getString(R.string.cancel_caps), null);
        alertDialogBldr.setPositiveButton(getString(R.string.ok_caps), null);
        final AlertDialog alertDialog = alertDialogBldr.create();
        final boolean finalIsLastRecord = isLastRecord;
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button button = ((AlertDialog) alertDialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String pitchStr = pitchTxt.getText().toString();
                        String depthStr = depthTxt.getText().toString();
                        String rollStr = rollTxt.getText().toString();
                        String tempStr = tempTxt.getText().toString();
                        String relElevStr = relElevTxt.getText().toString();
                        String rodLengthStr = defaultRodTxt.getText().toString();
                        String latiTxtStr = latiTxt.getText().toString();
                        String longiTxtStr = longiTxt.getText().toString();
                        /*if(!UGMUtility.isValidFloatString(pitchStr)) {
                            setInputLayoutError(pitchLayout);
                        } else if(record.isOnlyPitch()) {
                            record.setPitch(pitchStr);
                            record.setEdited(true);
                            UGMApplication.getInstance().getDatabase().getRecordDao().updateRecord(record, RecordsViewFragment.this);
                            alertDialog.dismiss();
                        } /*else if(!UGMUtility.isValidFloatString(depthStr)) {
                            setInputLayoutError(depthLayout);
                        } else if(!UGMUtility.isValidFloatString(rollStr)) {
                            setInputLayoutError(rollLayout);
                        } else*/

                        if (finalIsLastRecord && !UGMUtility.isValidFloatString(rodLengthStr)) {
                            setInputLayoutError(defaultRodLayout);
                        } else {
                            record.setPitch(pitchStr);
                            record.setDepth(depthStr);
                            record.setRoll(rollStr);
                            record.setTemperature(tempStr);

                            if (finalIsLastRecord) {
                                record.setRodLength(rodLengthStr);
                            }
                            if (View.VISIBLE == relElevLayout.getVisibility()) {
                                record.setRelativeElevation(relElevStr);
                            }
                            if (View.VISIBLE == latitudeLayout.getVisibility()) {
                                record.setLatitude(latiTxtStr);
                            }
                            if (View.VISIBLE == longitudeLayout.getVisibility()) {
                                record.setLongitude(longiTxtStr);
                            }
                            record.setEdited(true);
                            record.setSynced(false);
                            mActivity.getLocalDB().getRecordDao().update(record);
                            mActivity.showMessage(getString(R.string.rod_number_text) + record.getRodNumber()
                                    + " " + getString(R.string.is_updated_successfully), getString(R.string.ok_caps), null);
                            getAllRecords();
                            if (UGMUtility.isNetworkAvailable() && UGMUtility.getBooleanPreference(UGMMacros.VALUE_ONLINE_SUPPORT, true)) {
                                postRecords(record);
                            } else {
                                logger.info("RecordViewFragment:Internet is not available to upload to server for RodNumber:" + record.getRodNumber());
                            }
                            alertDialog.dismiss();
                        }
                    }
                });
            }
        });
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void validateErrorCheck(final TextInputLayout inputLayout) {
        inputLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() < 1) {
                    inputLayout.setErrorEnabled(true);
                    setInputLayoutError(inputLayout);
                }
                if (s.length() > 0) {
                    inputLayout.setError(null);
                    inputLayout.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setInputLayoutError(TextInputLayout inputLayout) {
        if (R.id.pitch_layout == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_pitch));
        } else if (R.id.depth_layout == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_depth));
        } else if (R.id.roll_layout == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_roll));
        } else if (R.id.default_rod_layout == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_rod_length));
        }
    }

    private void deleteRecord(final Record delRecord) {
        logger.info("RecordViewFragment:deleteRecord:Initiated");
        mActivity.showProgressDialog();
        String token = "Bearer " + UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_SESSION_TOKEN, "");
        JsonArray jsonArray = new JsonArray();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", delRecord.getRecordId());
        jsonArray.add(jsonObject);

        SendRecordData sendRecordData = RetrofitClientInstance.getRetrofitInstance().create(SendRecordData.class);
        Call<JobResponse> sendRecordCall = sendRecordData.deleteJobData(token, jsonArray);
        logger.info("#######RecordsViewFragment:deleteRecords:Request JSON:" + new Gson().toJson(jsonArray));
        sendRecordCall.enqueue(new Callback<JobResponse>() {
            @Override
            public void onResponse(Call<JobResponse> responseCall, Response<JobResponse> sendresponse) {
                JobResponse jobResponse = sendresponse.body();
                logger.info("#######RecordsViewFragment:deleteRecords:Response JSON:" + new Gson().toJson(jobResponse));
                if (jobResponse != null && jobResponse.getStatus().equalsIgnoreCase("success")) {
                    logger.info("RecordViewFragment:deleteRecords to server successfully.RodNumber:" + delRecord.getRodNumber());
                    mActivity.getLocalDB().getRecordDao().delete(delRecord);
                    mActivity.hideProgressDialog();
                    mActivity.showMessage(getString(R.string.rod_number_text) + delRecord.getRodNumber()
                            + " " + getString(R.string.is_deleted_successfully), getString(R.string.ok_caps), null);
                }
            }

            @Override
            public void onFailure(Call<JobResponse> call, Throwable t) {
                logger.info("RecordViewFragment:postRecords to server Failed.");
                mActivity.hideProgressDialog();
            }
        });
    }

    private void postRecords(final Record recentAddedRecord) {
        logger.info("RecordViewFragment:postRecords:Initiated");
        mActivity.showProgressDialog();
        String token = "Bearer " + UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_SESSION_TOKEN, "");
        final List<Record> records = mActivity.getLocalDB().getRecordDao().getRecordsBySyncedAndRodNumber(false, recentAddedRecord.getRodNumber());
        records.add(recentAddedRecord);
        JsonArray jsonArray = getAllRecordsJsonArray(records);
        if (jsonArray != null && jsonArray.size() > 0) {
            SendRecordData sendRecordData = RetrofitClientInstance.getRetrofitInstance().create(SendRecordData.class);
            Call<JobResponse> sendRecordCall = sendRecordData.sendJobData(token, jsonArray);
            logger.info("#######RecordsViewFragment:postRecords:Request JSON:" + new Gson().toJson(jsonArray));
            sendRecordCall.enqueue(new Callback<JobResponse>() {
                @Override
                public void onResponse(Call<JobResponse> responseCall, Response<JobResponse> sendresponse) {
                    JobResponse jobResponse = sendresponse.body();
                    logger.info("#######RecordsViewFragment:postRecords:Response JSON:" + new Gson().toJson(jobResponse));
                    if (jobResponse != null && jobResponse.getStatus().equalsIgnoreCase("success")) {
                        for (Record record : records) {
                            record.setSynced(true);
                            mActivity.getLocalDB().getRecordDao().update(record);
                            logger.info("RecordViewFragment:postRecords to server successfully.RodNumber:" + record.getRodNumber());
                        }
                        mActivity.hideProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<JobResponse> call, Throwable t) {
                    logger.info("RecordViewFragment:postRecords to server Failed.");
                    mActivity.hideProgressDialog();
                }
            });
        }
    }

    private JsonArray getAllRecordsJsonArray(List<Record> records) {
        JsonArray jsonArray = new JsonArray();
        for (Record record : records) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", record.getRecordId());
            jsonObject.addProperty("job_id", record.getJobId());
            jsonObject.addProperty("latitude", record.getLatitude());
            jsonObject.addProperty("longitude", record.getLongitude());
            jsonObject.addProperty("onlypitch", record.isOnlyPitch());
            jsonObject.addProperty("rodNumber", record.getRodNumber());
            jsonObject.addProperty("rodLength", record.getRodLength());
            jsonObject.addProperty("pitch", record.getPitch());
            jsonObject.addProperty("depth", record.getDepth());
            jsonObject.addProperty("roll", record.getRoll());
            jsonObject.addProperty("temperature", record.getTemperature());
            jsonObject.addProperty("status", record.getStatus());
            jsonObject.addProperty("createdDate", String.valueOf(record.getCreatedTime()));
            jsonObject.addProperty("updatedDate", String.valueOf(record.getUpdatedTime()));
            jsonObject.addProperty("relativeElevation", String.valueOf(record.getRelativeElevation()));
            jsonObject.addProperty("isEdited", record.isEdited());

            jsonArray.add(jsonObject);
        }
        logger.info("RecordViewFragment:okhttp getAllRecordsJsonArray json array = " + jsonArray.toString());
        return jsonArray;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a comm session
                    LocatorDevice.getInstance(getActivity());
                    GPSDevice.getInstance(getActivity());
                    // Launch the DeviceListActivity to see devices and do scan
                    Intent intent = new Intent(getActivity(), RecordCreateActivity.class);
                    intent.putExtra("JOB_ITEM", mActivity.getJobItem());
                    startActivity(intent);
                }
                break;
        }
    }
}
