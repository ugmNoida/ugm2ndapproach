package com.ugm.android.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ugm.android.BuildConfig;
import com.ugm.android.R;
import com.ugm.android.UGMApplication;
import com.ugm.android.bluetooth.BluetoothUtils;
import com.ugm.android.bluetooth.DeviceThreadControl;
import com.ugm.android.bluetooth.LocatorDevice;
import com.ugm.android.database.entity.Job;
import com.ugm.android.database.entity.Record;
import com.ugm.android.ui.fragments.DeletedJobListFragment;
import com.ugm.android.ui.fragments.InfoFragment;
import com.ugm.android.ui.fragments.JobListFragment;
import com.ugm.android.ui.fragments.SettingsFragment;
import com.ugm.android.utilities.Constants;
import com.ugm.android.utilities.LogbackConfigure;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;
import com.ugm.android.webservice.GetJobData;
import com.ugm.android.webservice.GetJobWithJobDetails;
import com.ugm.android.webservice.GetRecordData;
import com.ugm.android.webservice.JobData;
import com.ugm.android.webservice.JobResponse;
import com.ugm.android.webservice.RecordData;
import com.ugm.android.webservice.SendJobData;
import com.ugm.android.webservice.SendRecordData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;


public class ContainerActivity extends AbstractBaseActivity implements View.OnClickListener {

    private static Logger logger = LoggerFactory.getLogger(ContainerActivity.class);

    private LinearLayout myJobsLayout, deleteLayout, settingsLayout, feedbackLaout, infoLayout, logoutLayout;
    private TextView versionText, emailId, fullName;//, resetPassword;
    private CommandCommThread mCommandCommThread;
    private boolean mThreadStopF = false;
    private String loggedInUserId;
    private ArrayList<String> jobSyncIdList, recordSyncIdList;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        logger.info("ContainerActivity:onCreate");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        mContext = this;
        init();
        doAppDataSync();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        logger.info("ContainerActivity:onNewIntent");
        init();
    }

    private void init() {
        registerReceiver();
        initUI();
        openMyJobFragment();


        if (LocatorDevice.getInstance(getApplicationContext()).getState() == DeviceThreadControl.STATE_CONNECTED) {
            logger.info("ContainerActivity: BluetoothCommService STATE_CONNECTED");
            if (mCommandCommThread != null) {
                mCommandCommThread = null;
                mThreadStopF = true;
            }
            logger.info("ContainerActivity: mCommandCommThread start...");
            mCommandCommThread = new CommandCommThread();
            mCommandCommThread.start();
        } else {
            logger.info("ContainerActivity: BluetoothCommService NOT CONNECTED.");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void registerReceiver() {
        logger.info("ContainerActivity:registerReceiver.");
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_VALUE_DEVICE_CONNECTED);
        filter.addAction(Constants.ACTION_VALUE_CONNECTION_LOST);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    public BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(Constants.ACTION_VALUE_DEVICE_CONNECTED)) {
                logger.info("ContainerActivity:BroadcastReceiver:ACTION_VALUE_DEVICE_CONNECTED");
                if (mCommandCommThread != null) {
                    mCommandCommThread = null;
                    mThreadStopF = true;
                }
                showMessage(getString(R.string.device_connected), getString(R.string.ok), null);
                mCommandCommThread = new CommandCommThread();
                mCommandCommThread.start();
            } else if (intent.getAction().equalsIgnoreCase(Constants.ACTION_VALUE_CONNECTION_LOST)) {
                logger.info("ContainerActivity:BroadcastReceiver:ACTION_VALUE_CONNECTION_LOST");
                if (mCommandCommThread != null) {
                    mCommandCommThread = null;
                    mThreadStopF = true;
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mThreadStopF = true;
        mCommandCommThread = null;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        logger.info("ContainerActivity:onDestroy");
    }

    private void initUI() {
        logger.info("ContainerActivity:InitUI");
        myJobsLayout = (LinearLayout) findViewById(R.id.myjob_layout);
        myJobsLayout.setOnClickListener(this);
        deleteLayout = (LinearLayout) findViewById(R.id.delete_layout);
        deleteLayout.setOnClickListener(this);
        settingsLayout = (LinearLayout) findViewById(R.id.settings_layout);
        settingsLayout.setOnClickListener(this);
        feedbackLaout = (LinearLayout) findViewById(R.id.feedback_layout);
        feedbackLaout.setOnClickListener(this);
        infoLayout = (LinearLayout) findViewById(R.id.info_layout);
        infoLayout.setOnClickListener(this);
        logoutLayout = (LinearLayout) findViewById(R.id.logout_layout);
        logoutLayout.setOnClickListener(this);
        versionText = (TextView) findViewById(R.id.version_text);
        versionText.setText(getString(R.string.build_version) + BuildConfig.VERSION_NAME);
        logger.info("ContainerActivity:Build Version - " + BuildConfig.VERSION_NAME);
        emailId = (TextView) findViewById(R.id.email_id);
        String emailIdStr = UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_EMAIL, null);
        emailId.setText(emailIdStr);
        fullName = (TextView) findViewById(R.id.full_Name);
        fullName.setText(UGMUtility.getStringPreference(PREFS_FULL_NAME, ""));
        //resetPassword = (TextView)findViewById(R.id.reset_password);
        //resetPassword.setOnClickListener(this);
    }

    public String getLoggedInUserId() {
        if (null == loggedInUserId) {
            loggedInUserId = UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_USERID, null);
        }
        return loggedInUserId;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.myjob_layout) {
            openMyJobFragment();
        } else if (id == R.id.delete_layout) {
            openDeleteFragment();
        } else if (id == R.id.settings_layout) {
            openSettingsFragment();
        } else if (id == R.id.feedback_layout) {
            sendFeedback();
        } else if (id == R.id.info_layout) {
            openInfoFragment();
        } else if (id == R.id.reset_password) {
            Intent resetIntent = new Intent(this, ResetPasswordActivity.class);
            startActivity(resetIntent);
        } else if (id == R.id.logout_layout) {
            logout();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void sendFeedback() {
        logger.info("ContainerActivity:sendFeedback");
        new AsyncTask<Void, Void, Uri>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressDialog();
            }

            @Override
            protected Uri doInBackground(Void... voids) {
                Uri sharedFileUri = null;
                LogbackConfigure logbackConfigure = LogbackConfigure.getInstance(getUGMApplication());
                File logFiles = logbackConfigure.getZipLogFolder();
                if (null != logFiles && logFiles.exists()) {
                    sharedFileUri = FileProvider.getUriForFile(ContainerActivity.this, BuildConfig.APPLICATION_ID, logFiles);
                }
                return sharedFileUri;
            }

            @Override
            protected void onPostExecute(Uri sharedFileUri) {
                super.onPostExecute(sharedFileUri);
                hideProgressDialog();
                if (null != sharedFileUri) {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("plain/text");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, "support@ummaps.com");
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "UGM - Feedback");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "PFA...");
                    emailIntent.putExtra(Intent.EXTRA_STREAM, sharedFileUri);
                    emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(emailIntent);
                }
            }
        }.execute();
    }

    private void openInfoFragment() {
        logger.info("ContainerActivity:Open InfoFragment");
        InfoFragment infoFragment = InfoFragment.newInstance(null, null);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container_fragment, infoFragment, "infoFragment");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void openJobCreateActivity() {
        logger.info("ContainerActivity:Open JobCreateActivity");
        Intent intent = new Intent(ContainerActivity.this, JobCreateActivity.class);
        startActivity(intent);
    }

    private void openMyJobFragment() {
        logger.info("ContainerActivity:Open JobListFragment");
        JobListFragment jobListFragment = JobListFragment.newInstance();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container_fragment, jobListFragment, "JobListFragment");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openDeleteFragment() {
        logger.info("ContainerActivity:Open DeletedJobListFragment");
        DeletedJobListFragment deletedJobListFragment = DeletedJobListFragment.newInstance();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container_fragment, deletedJobListFragment, "DeletedJobListFragment");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openSettingsFragment() {
        logger.info("ContainerActivity:Open SettingsFragment");
        SettingsFragment settingsFragment = SettingsFragment.newInstance();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container_fragment, settingsFragment, "SettingsFragment");
        transaction.addToBackStack(null);
        transaction.commit();
    }


    private class CommandCommThread extends Thread {

        public CommandCommThread() {
            mThreadStopF = false;
        }

        @Override
        public void run() {
            try {
                logger.info("ContainerActivity:mThreadStopF:" + mThreadStopF);

                while (!mThreadStopF) {
                    if (UGMApplication.getInstance().getCount() == 0) {
                        getPitchValidDataFromUGM();
                        sleep(1000);
                        getPitchDataFromUGM();
                        sleep(1000);
                        getDistanceDataFromUGM();
                        sleep(1000);
                        getTempDataFromUGM();
                        sleep(1000);
                        getROLLDataFromUGM();
                    }
                }
                logger.info("ContainerActivity:########## command OUT OF THREAD WHILE ##########");
            } catch (Exception ex) {
                logger.error("ContainerActivity:@@@@@@@ command THREAD EXCEPTION @@@@@@@ " + ex.toString());
            } finally {
                logger.info("ContainerActivity:########## command THREAD TERMINATED ##########");
            }
        }
    }

    private void getPitchValidDataFromUGM() {
        try {
            String[] dataCommands = {Constants.PITCH_VALID_CMD};//, Constants.DISTANCE_CMD, Constants.TEMPERATURE_CMD};
            for (String string : dataCommands) {
                byte[] command = string.getBytes();
                byte[] newLine = "\r\n".getBytes();
                logger.info("ContainerActivity:command PITCH going to write from activity ..." + string + " , readCount = " + UGMApplication.getInstance().getCount());
                byte[] cmd = BluetoothUtils.concat(command, newLine);
                LocatorDevice.getInstance(this).write(cmd);
            }
        } catch (Exception ex) {
            logger.error("ContainerActivity:command exception e = " + ex.toString());
        } finally {
            //UGMApplication.getInstance().addRC();
            //readCount++;
        }
    }

    private void getPitchDataFromUGM() {
        try {
            String[] dataCommands = {Constants.PITCH_CMD};//, Constants.DISTANCE_CMD, Constants.TEMPERATURE_CMD};
            for (String string : dataCommands) {
                byte[] command = string.getBytes();
                byte[] newLine = "\r\n".getBytes();
                logger.info("ContainerActivity:command PITCH going to write from activity ..." + string + " , readCount = " + UGMApplication.getInstance().getCount());
                byte[] cmd = BluetoothUtils.concat(command, newLine);
                LocatorDevice.getInstance(this).write(cmd);
            }
        } catch (Exception ex) {
            logger.error("ContainerActivity:command exception e = " + ex.toString());
        } finally {
            //UGMApplication.getInstance().addRC();
            //readCount++;
        }
    }

    private void getTempDataFromUGM() {
        try {
            String[] dataCommands = {Constants.TEMPERATURE_CMD};//, Constants.DISTANCE_CMD, Constants.TEMPERATURE_CMD};
            for (String string : dataCommands) {
                byte[] command = string.getBytes();
                byte[] newLine = "\r\n".getBytes();
                logger.info("ContainerActivity:command TEMP going to write from activity ..." + string + " , readCount = " + UGMApplication.getInstance().getCount());
                byte[] cmd = BluetoothUtils.concat(command, newLine);
                LocatorDevice.getInstance(this).write(cmd);
            }
        } catch (Exception ex) {
            logger.error("ContainerActivity:command exception e = " + ex.toString());
        } finally {
            //UGMApplication.getInstance().addRC();
            //readCount++;
        }
    }

    private void getDistanceDataFromUGM() {
        try {
            String[] dataCommands = {Constants.DISTANCE_CMD};//, Constants.DISTANCE_CMD, Constants.TEMPERATURE_CMD};
            for (String string : dataCommands) {
                byte[] command = string.getBytes();
                byte[] newLine = "\r\n".getBytes();
                logger.info("ContainerActivity:command DISTANCE going to write from activity ..." + string + " , readCount = " + UGMApplication.getInstance().getCount());
                byte[] cmd = BluetoothUtils.concat(command, newLine);
                LocatorDevice.getInstance(this).write(cmd);
            }
        } catch (Exception ex) {
            logger.error("ContainerActivity:command exception e = " + ex.toString());
        } finally {
            //UGMApplication.getInstance().addRC();
            //readCount++;
        }
    }

    private void getROLLDataFromUGM() {
        try {
            String[] dataCommands = {Constants.ROLL_CMD};//, Constants.DISTANCE_CMD, Constants.TEMPERATURE_CMD};
            for (String string : dataCommands) {
                byte[] command = string.getBytes();
                byte[] newLine = "\r\n".getBytes();
                logger.info("ContainerActivity:command ROLL going to write from activity ..." + string + " , readCount = " + UGMApplication.getInstance().getCount());
                byte[] cmd = BluetoothUtils.concat(command, newLine);
                LocatorDevice.getInstance(this).write(cmd);
            }
        } catch (Exception ex) {
            logger.error("ContainerActivity:command exception = " + ex.toString());
        } finally {
            //UGMApplication.getInstance().addRC();
            //readCount++;
        }
    }

    public ArrayList<String> getActiveJobStatuses() {
        ArrayList<String> statuses = new ArrayList<String>();
        statuses.add(UGMMacros.JOB_CREATED);
        statuses.add(UGMMacros.JOB_INPROGRESS);
        statuses.add(UGMMacros.JOB_COMPLETED);
        return statuses;
    }

    public ArrayList<String> getSoftDeleteJobStatuses() {
        ArrayList<String> statuses = new ArrayList<String>();
        statuses.add(UGMMacros.JOB_SOFT_DELETED);
        return statuses;
    }

    private void postJobs(final String token) {
        try {
            logger.info("ContainerActivity:postJobs:Initiating.");
            if (null != getLoggedInUserId()) {
                //showProgressDialog();
                JsonArray jsonArray = getAllJobsJsonArray();
                if (jsonArray != null && jsonArray.size() > 0) {
                    SendJobData sendJobData = RetrofitClientInstance.getRetrofitInstance().create(SendJobData.class);
                    Call<JobResponse> sendJobCall = sendJobData.sendJobData(token, jsonArray);
                    logger.info("#######ContainerActivity:postJobs:Request JSON:" + new Gson().toJson(jsonArray));
                    Response<JobResponse> sendresponse = sendJobCall.execute();
                    JobResponse jobResponse = sendresponse.body();
                    logger.info("#######ContainerActivity:postJobs:Response JSON:" + jobResponse);
                    if (jobResponse != null && jobResponse.getStatus().equalsIgnoreCase("success")) {
                        if (jobSyncIdList != null && jobSyncIdList.size() > 0) {
                            for (String jobID : jobSyncIdList) {
                                com.ugm.android.database.entity.Job dbJob = getLocalDB().getJobDao().getJobByJobId(jobID);
                                dbJob.setSynced(true);
                                getLocalDB().getJobDao().update(dbJob);
                            }
                            jobSyncIdList.clear();
                        }
                    } else if (null != sendresponse.errorBody()) {
                        String errorMessage = parseErrorMessage(sendresponse.errorBody());
                        logger.info("ContainerActivity:postJobs:Response Error Message:" + errorMessage);
                        if (EXPIRED_TOKEN.equalsIgnoreCase(errorMessage)) {
                            logout();
                            return;
                        } else {
                            showMessage(errorMessage, getString(R.string.ok), null);
                        }
                    } else {
                        logger.info("ContainerActivity:postJobs:Response Something went wrong.");
                        showMessage("Something went wrong...Please try later!", "Ok", null);
                    }
                }
            }
        } catch (Exception ex) {
            logger.info("ContainerActivity:postJobs:Exception:" + ex.toString());
            showMessage(getString(R.string.something_went_wrong), getString(R.string.ok), null);
        } finally {
            //getJobs(token);
            postRecords(token);
        }
    }

    private void getJobs(final String token) {
        try {
            logger.info("ContainerActivity:getJobs:Initiated");
            GetJobData service = RetrofitClientInstance.getRetrofitInstance().create(GetJobData.class);
            Call<List<JobData>> call = service.getJobData(token);
            Response<List<JobData>> response = call.execute();
            if (response.body() != null) {
                List<JobData> serverJobs = response.body();
                if (serverJobs != null && serverJobs.size() > 0) {
                    for (JobData jobData : serverJobs) {
                        com.ugm.android.database.entity.Job dbJob = new com.ugm.android.database.entity.Job();
                        logger.info("ContainerActivity:getJobs:Response:JobId" + jobData.getId());
                        dbJob.setJobId(jobData.getId());
                        dbJob.setUserId(jobData.getUserID());
                        dbJob.setJobName(jobData.getJobName());
                        dbJob.setJobDescription(jobData.getJobDescription());
                        dbJob.setClientName(jobData.getClientName());
                        dbJob.setCompanyName(jobData.getCompanyName());
                        dbJob.setDefaultRodLength(jobData.getDefaultRodLength());
                        dbJob.setFirstRodLength(jobData.getFirstRodLength());
                        dbJob.setLocationName(jobData.getLocationName());
                        dbJob.setSynced(true);
                        dbJob.setPrefPitch(jobData.getPrefPitch());
                        dbJob.setPrefDepth(jobData.getPrefDepth());
                        //todo = need to remove hard core value once data available from API
                        dbJob.setStatus(jobData.getStatus());
                        if (!TextUtils.isEmpty(jobData.getCreatedDate()))
                            dbJob.setCreatedTime(Long.parseLong(jobData.getCreatedDate()));
                        if (!TextUtils.isEmpty(jobData.getUpdatedDate()))
                            dbJob.setUpdatedTime(Long.parseLong(jobData.getUpdatedDate()));
                        insertOrUpdateJob(dbJob);
                    }
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent(Constants.ACTION_JOB_SYNCED));
                }
            } else if (null != response.errorBody()) {
                String errorMessage = parseErrorMessage(response.errorBody());
                logger.info("ContainerActivity:getJobs:Response Error Message:" + errorMessage);
                if (EXPIRED_TOKEN.equalsIgnoreCase(errorMessage)) {
                    logout();
                    return;
                } else {
                    showMessage(errorMessage, getString(R.string.ok), null);
                }
            } else {
                logger.info("ContainerActivity:getJobs:Response Something went wrong.");
                showMessage(getString(R.string.something_went_wrong), getString(R.string.ok), null);
            }
        } catch (Exception ex) {
            logger.info("ContainerActivity:getJobs: Exception:" + ex.toString());
            showMessage(getString(R.string.something_went_wrong), getString(R.string.ok), null);
        } finally {
            postRecords(token);
        }
    }

    private void postRecords(final String token) {
        logger.info("ContainerActivity:postRecords:Initiated");
        try {
            JsonArray jsonArray = getAllRecordsJsonArray();
            if (jsonArray != null && jsonArray.size() > 0) {
                SendRecordData sendRecordData = RetrofitClientInstance.getRetrofitInstance().create(SendRecordData.class);
                Call<JobResponse> sendRecordCall = sendRecordData.sendJobData(token, jsonArray);
                logger.info("#######ContainerActivity:postRecords:Request JSON:" + new Gson().toJson(jsonArray));
                Response<JobResponse> sendresponse = sendRecordCall.execute();
                JobResponse jobResponse = sendresponse.body();
                logger.info("#######ContainerActivity:postRecords:Response JSON:" + new Gson().toJson(jobResponse));
                if (jobResponse != null && jobResponse.getStatus().equalsIgnoreCase("success")) {
                    if (recordSyncIdList != null && recordSyncIdList.size() > 0) {
                        for (String recordID : recordSyncIdList) {
                            Record dbRecord = getLocalDB().getRecordDao().getRecordByRecordId(recordID);
                            logger.info("ContainerActivity:postRecords:RecordId:" + recordID);
                            if (UGMMacros.RECORD_DELETED.equalsIgnoreCase(dbRecord.getStatus())) {
                                getLocalDB().getRecordDao().delete(dbRecord);
                            } else {
                                dbRecord.setSynced(true);
                                getLocalDB().getRecordDao().update(dbRecord);
                            }
                        }
                        recordSyncIdList.clear();
                    }
                } else if (null != sendresponse.errorBody()) {
                    String errorMessage = parseErrorMessage(sendresponse.errorBody());
                    logger.info("ContainerActivity:postRecords:Response Error Body:" + errorMessage);
                    if (EXPIRED_TOKEN.equalsIgnoreCase(errorMessage)) {
                        logout();
                        return;
                    } else {
                        showMessage(errorMessage, getString(R.string.ok), null);
                    }
                } else {
                    logger.info("ContainerActivity:postRecords:Response Something went wrong");
                    showMessage(getString(R.string.something_went_wrong), getString(R.string.ok), null);
                }
            }
        } catch (Exception ex) {
            logger.info("ContainerActivity:postRecords:Exception:" + ex.toString());
            showMessage(getString(R.string.something_went_wrong), getString(R.string.ok), null);
        } finally {
            //getRecords(token);
            getJobsWithRecords(token);
        }
    }

    private void getJobsWithRecords(String token) {
        try {
            logger.info("ContainerActivity:getJobsWithRecords:Initiated");
            GetJobWithJobDetails service = RetrofitClientInstance.getRetrofitInstance().create(GetJobWithJobDetails.class);
            Call<List<JobData>> call = service.getJobWithRecords(token);
            logger.info("#######ContainerActivity:getJobsWithRecords:Request JSON:" + new Gson().toJson(call.request()));
            Response<List<JobData>> response = call.execute();
            List<JobData> jobDataList = response.body();
            logger.info("#######ContainerActivity:getJobsWithRecords:Response JSON:" + new Gson().toJson(jobDataList));
            if (jobDataList != null) {
                if (jobDataList != null && jobDataList.size() > 0) {
                    for (JobData jobData : jobDataList) {
                        com.ugm.android.database.entity.Job dbJob = new com.ugm.android.database.entity.Job();
                        logger.info("ContainerActivity:getJobsWithRecords:Response:JobId" + jobData.getId());
                        dbJob.setJobId(jobData.getId());
                        dbJob.setUserId(jobData.getUserID());
                        dbJob.setJobName(jobData.getJobName());
                        dbJob.setJobDescription(jobData.getJobDescription());
                        dbJob.setClientName(jobData.getClientName());
                        dbJob.setCompanyName(jobData.getCompanyName());
                        dbJob.setDefaultRodLength(jobData.getDefaultRodLength());
                        dbJob.setFirstRodLength(jobData.getFirstRodLength());
                        dbJob.setLocationName(jobData.getLocationName());
                        dbJob.setSynced(true);
                        dbJob.setPrefPitch(jobData.getPrefPitch());
                        dbJob.setPrefDepth(jobData.getPrefDepth());
                        //todo = need to remove hard core value once data available from API
                        dbJob.setStatus(jobData.getStatus());
                        if (!TextUtils.isEmpty(jobData.getCreatedDate()))
                            dbJob.setCreatedTime(Long.parseLong(jobData.getCreatedDate()));
                        if (!TextUtils.isEmpty(jobData.getUpdatedDate()))
                            dbJob.setUpdatedTime(Long.parseLong(jobData.getUpdatedDate()));
                        insertOrUpdateJob(dbJob);
                        List<RecordData> serverRecords = jobData.getRecords();
                        if (serverRecords != null && serverRecords.size() > 0) {
                            for (RecordData recordData : serverRecords) {
                                logger.info("ContainerActivity:getJobsWithRecords:Response RecordId:" + recordData.getId());
                                Record dbRecord = new Record();
                                dbRecord.setRecordId(recordData.getId());
                                dbRecord.setJobId(recordData.getJobID());
                                dbRecord.setLatitude(recordData.getLatitude());
                                dbRecord.setLongitude(recordData.getLongitude());
                                dbRecord.setOnlyPitch(recordData.getOnlypitch());
                                if (!TextUtils.isEmpty(recordData.getRodNumber()))
                                    dbRecord.setRodNumber(Integer.parseInt(recordData.getRodNumber()));
                                dbRecord.setRodLength(recordData.getRodLength());
                                dbRecord.setPitch(recordData.getPitch());
                                dbRecord.setDepth(recordData.getDepth());
                                dbRecord.setRoll(recordData.getRoll());
                                dbRecord.setTemperature(recordData.getTemperature());
                                dbRecord.setStatus(recordData.getStatus());
                                if (!TextUtils.isEmpty(recordData.getCreatedDate()))
                                    dbRecord.setCreatedTime(Long.parseLong(recordData.getCreatedDate()));
                                if (!TextUtils.isEmpty(recordData.getUpdatedDate()))
                                    dbRecord.setUpdatedTime(Long.parseLong(recordData.getUpdatedDate()));
                                dbRecord.setRoll(recordData.getRoll());
                                dbRecord.setEdited(recordData.getIsEdited());
                                dbRecord.setRelativeElevation(recordData.getRelativeElevation());
                                dbRecord.setSynced(true);
                                insertOrUpdateRecord(dbRecord);
                            }
                        }
                    }
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent(Constants.ACTION_JOB_SYNCED));
                }
            } else if (null != response.errorBody()) {
                String errorMessage = parseErrorMessage(response.errorBody());
                logger.info("ContainerActivity:getJobsWithRecords:Response Error Message:" + errorMessage);
                if (EXPIRED_TOKEN.equalsIgnoreCase(errorMessage)) {
                    logout();
                    return;
                } else {
                    showMessage(errorMessage, "Ok", null);
                }
            } else {
                logger.info("ContainerActivity:getJobsWithRecords:Response Something went wrong.");
                showMessage("Something went wrong...Please try later!", "Ok", null);
            }
        } catch (Exception ex) {
            logger.info("ContainerActivity:getJobsWithRecords: Exception:" + ex.toString());
            showMessage("Something went wrong...Please try later!", "Ok", null);
        } finally {
        }
    }

    private void getRecords(String token) {
        try {
            logger.info("ContainerActivity:getRecords:Initiated");
            List<Job> jobList = getLocalDB().getJobDao().getAllJobsByUserId(getLoggedInUserId());
            if (null != jobList && 0 < jobList.size()) {
                for (Job job : jobList) {
                    GetRecordData recordService = RetrofitClientInstance.getRetrofitInstance().create(GetRecordData.class);
                    Call<List<RecordData>> recordCall = recordService.getRecordData(token, job.getJobId());
                    Response<List<RecordData>> response = recordCall.execute();
                    if (response.body() != null) {
                        List<RecordData> serverRecords = response.body();
                        if (serverRecords != null && serverRecords.size() > 0) {
                            for (RecordData recordData : serverRecords) {
                                logger.info("ContainerActivity:getRecords:Response JobId:" + recordData.getJobID());
                                logger.info("ContainerActivity:getRecords:Response RecordId:" + recordData.getId());
                                Record dbRecord = new Record();
                                dbRecord.setRecordId(recordData.getId());
                                dbRecord.setJobId(recordData.getJobID());
                                dbRecord.setLatitude(recordData.getLatitude());
                                dbRecord.setLongitude(recordData.getLongitude());
                                dbRecord.setOnlyPitch(recordData.getOnlypitch());
                                if (!TextUtils.isEmpty(recordData.getRodNumber()))
                                    dbRecord.setRodNumber(Integer.parseInt(recordData.getRodNumber()));
                                dbRecord.setRodLength(recordData.getRodLength());
                                dbRecord.setPitch(recordData.getPitch());
                                dbRecord.setDepth(recordData.getDepth());
                                dbRecord.setRoll(recordData.getRoll());
                                dbRecord.setTemperature(recordData.getTemperature());
                                dbRecord.setStatus(recordData.getStatus());
                                if (!TextUtils.isEmpty(recordData.getCreatedDate()))
                                    dbRecord.setCreatedTime(Long.parseLong(recordData.getCreatedDate()));
                                if (!TextUtils.isEmpty(recordData.getUpdatedDate()))
                                    dbRecord.setUpdatedTime(Long.parseLong(recordData.getUpdatedDate()));
                                dbRecord.setRoll(recordData.getRoll());
                                dbRecord.setEdited(recordData.getIsEdited());
                                dbRecord.setRelativeElevation(recordData.getRelativeElevation());
                                dbRecord.setSynced(true);
                                insertOrUpdateRecord(dbRecord);
                            }
                        }
                    } else if (null != response.errorBody()) {
                        String errorMessage = parseErrorMessage(response.errorBody());
                        logger.info("ContainerActivity:getRecords Response ErrorBody:" + errorMessage + "of Job Id:" + job.getJobId());
                        if (EXPIRED_TOKEN.equalsIgnoreCase(errorMessage)) {
                            logout();
                            return;
                        } else {
                            showMessage(errorMessage + " of the Job " + job.getJobName(), "Ok", null);
                        }
                    } else {
                        logger.info("ContainerActivity:getRecords Something went wrong of Job Id:" + job.getJobId());
                        showMessage("Something went wrong...Please try later!", "Ok", null);
                    }
                }
            }
        } catch (Exception ex) {
            logger.info("ContainerActivity:getRecords Exception occurred:" + ex.toString());
            showMessage(getString(R.string.something_went_wrong), getString(R.string.ok), null);
        } finally {
            //hideProgressDialog();
        }
    }


    public void doAppDataSync() {
        if (UGMUtility.isNetworkAvailable()) {
            logger.info("ContainerActivity:doAppDataSync strated...");
            final String token = "Bearer " + UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_SESSION_TOKEN, "");
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    showProgressDialog();
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    logger.info("ContainerActivity:postJobs token" + token);
                    postJobs(token);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    hideProgressDialog();
                }
            }.execute();
        } else {
            logger.info("ContainerActivity:doAppDataSync : Network is not available.");
            showMessage(getString(R.string.network_unavailable), getString(R.string.ok), null);
        }
    }

    private JsonArray getAllJobsJsonArray() {
        List<com.ugm.android.database.entity.Job> jobs = new ArrayList<>();
        JsonArray jsonArray = new JsonArray();

        if (jobSyncIdList == null)
            jobSyncIdList = new ArrayList<>();
        jobSyncIdList.clear();

        if (null != getLoggedInUserId()) {
            jobs = getLocalDB().getJobDao().getJobsByUserIdAndSyncStatus(getLoggedInUserId(), false);
            for (com.ugm.android.database.entity.Job job : jobs) {
                jobSyncIdList.add(job.getJobId());
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("id", job.getJobId());
                jsonObject.addProperty("user_id", getLoggedInUserId());
                jsonObject.addProperty("jobName", job.getJobName());
                jsonObject.addProperty("jobDescription", job.getJobDescription());
                jsonObject.addProperty("clientName", job.getClientName());
                jsonObject.addProperty("companyName", job.getCompanyName());
                jsonObject.addProperty("defaultRodLength", job.getDefaultRodLength());
                jsonObject.addProperty("firstRodLength", job.getFirstRodLength());
                jsonObject.addProperty("locationName", job.getLocationName());
                jsonObject.addProperty("status", job.getStatus());
                jsonObject.addProperty("createdDate", String.valueOf(job.getCreatedTime()));
                jsonObject.addProperty("updatedDate", String.valueOf(job.getUpdatedTime()));
                jsonObject.addProperty("prefDepth", job.getPrefDepth());
                jsonObject.addProperty("prefPitch", job.getPrefPitch());
                jsonArray.add(jsonObject);
            }
        }
        logger.info("ContainerActivity:okhttp getAllJobsJsonArray json array = " + jsonArray.toString());
        return jsonArray;
    }


    private JsonArray getAllRecordsJsonArray() {
        HashMap<String, List<Record>> recordMap = new HashMap<>();
        List<Record> records = new ArrayList<>();
        JsonArray jsonArray = new JsonArray();
        if (recordSyncIdList == null)
            recordSyncIdList = new ArrayList<>();
        recordSyncIdList.clear();
        records = getLocalDB().getRecordDao().getRecordsBySynced(false);
        for (Record record : records) {
            recordSyncIdList.add(record.getRecordId());
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", record.getRecordId());
            jsonObject.addProperty("job_id", record.getJobId());
            jsonObject.addProperty("latitude", record.getLatitude());
            jsonObject.addProperty("longitude", record.getLongitude());
            jsonObject.addProperty("onlypitch", record.isOnlyPitch());
            jsonObject.addProperty("rodNumber", record.getRodNumber());
            jsonObject.addProperty("rodLength", record.getRodLength());
            jsonObject.addProperty("pitch", record.getPitch());
            jsonObject.addProperty("depth", record.getDepth());
            jsonObject.addProperty("roll", record.getRoll());
            jsonObject.addProperty("temperature", record.getTemperature());
            jsonObject.addProperty("status", record.getStatus());
            jsonObject.addProperty("createdDate", String.valueOf(record.getCreatedTime()));
            jsonObject.addProperty("updatedDate", String.valueOf(record.getUpdatedTime()));
            jsonObject.addProperty("relativeElevation", String.valueOf(record.getRelativeElevation()));
            jsonObject.addProperty("isEdited", record.isEdited());

            jsonArray.add(jsonObject);
        }
        logger.info("ContainerActivity:okhttp getAllRecordsJsonArray json array = " + jsonArray.toString());
        return jsonArray;
    }

    private void insertOrUpdateJob(com.ugm.android.database.entity.Job job) {
        try {
            List<com.ugm.android.database.entity.Job> jobs = getLocalDB().getJobDao().getJobsByUserIdAndJobName(getLoggedInUserId(), job.getJobName());
            if (null != jobs && 0 < jobs.size()) {
                logger.info(job.getJobName() + " is already exist. Update JobData");
                getLocalDB().getJobDao().update(job);
            } else {
                getLocalDB().getJobDao().insert(job);
                logger.info(job.getJobName() + " is already inserted in database.");
            }
        } catch (Exception ex) {
            logger.error("Exception while creating new JobData." + ex.toString());
            showMessage(getString(R.string.unexpected_error), getString(R.string.ok), null);
        }
    }

    private void insertOrUpdateRecord(Record record) {
        try {
            Record dbrecord = getLocalDB().getRecordDao().getRecordByRecordId(record.getRecordId());
            if (null != dbrecord) {
                logger.info(dbrecord.getRodNumber() + " is already exist. Update RecordData");
                getLocalDB().getRecordDao().update(record);
            } else {
                getLocalDB().getRecordDao().insert(record);
                logger.info(record.getRodNumber() + " is already inserted in database.");
            }
        } catch (Exception ex) {
            logger.error("Exception while creating new RecordData." + ex.toString());
            showMessage(getString(R.string.unexpected_error), getString(R.string.ok), null);
        }
    }

    public String parseErrorMessage(ResponseBody responseBody) {
        String errorMsg = getString(R.string.un_expected_error);
        try {
            Gson gson = new Gson();
            JobResponse errorResponse = gson.fromJson(responseBody.charStream(), JobResponse.class);
            errorMsg = errorResponse.getMessage();
        } catch (Exception ex) {
            logger.error("ContainerActivity:parseErrorMessage:" + ex.toString());
        }
        return errorMsg;
    }
}
