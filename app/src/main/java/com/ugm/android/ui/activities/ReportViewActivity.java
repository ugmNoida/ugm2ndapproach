package com.ugm.android.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itextpdf.text.Document;
import com.ugm.android.BuildConfig;
import com.ugm.android.R;
import com.ugm.android.database.entity.Job;
import com.ugm.android.database.entity.Record;
import com.ugm.android.datamodel.ReportPDFViewModel;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;

import java.io.File;
import java.util.ArrayList;

public class ReportViewActivity extends AbstractBaseActivity {
    private static final String TAG = "ReportViewActivity";
    private LinearLayout jobViewLayout;
    private TextView jobNameTxt, jobNameValue, createdDateTxt, createdDateValue, updatedDateTxt, updatedDateValue;
    private TextView locationTxt, locationValue;//, deviceSerialTxt, deviceSerialValue;
    private TextView firstRodTxt, firstRodValue, companyNameTxt, companyNameValue;
    private TextView defaultRodTxt, defaultRodValue, clientNameTxt, clientNameValue;
    private TextView dataPointTxt, dataPointValue, depthTxt, depthValue, pitchTxt, pitchValue;
    private TextView descriptionTxt, descriptionValue;
    private Job job;
    private ArrayList<Record> records;
    private RelativeLayout mapLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_view_activity);
        processIntent(getIntent());
        initActionBar(getString(R.string.report_text));
        initView();
    }

    private void processIntent(Intent intent){
        if(intent != null){
            job =  (Job)intent.getSerializableExtra("JOB_ITEM");
            records =  (ArrayList<Record>) intent.getSerializableExtra("RECORD_LIST");
        }
    }

    private void initView() {
        jobViewLayout = findViewById(R.id.job_view_layout);
        jobNameTxt = findViewById(R.id.job_name_txt);
        jobNameTxt.setText(getString(R.string.job_data_name)+":");
        jobNameValue = findViewById(R.id.job_name_value);
        jobNameValue.setText(job.getJobName());
        createdDateTxt = findViewById(R.id.created_date_txt);
        createdDateTxt.setText(getString(R.string.created_date)+":");
        createdDateValue = findViewById(R.id.created_date_value);
        createdDateValue.setText(UGMUtility.covertLongToDFormattedDate(job.getCreatedTime(), UGMMacros.FORMATTER1));
        updatedDateTxt = findViewById(R.id.updated_date_txt);
        updatedDateTxt.setText(getString(R.string.updated_date)+":");
        updatedDateValue = findViewById(R.id.updated_date_value);
        updatedDateValue.setText(UGMUtility.covertLongToDFormattedDate(job.getUpdatedTime(), UGMMacros.FORMATTER1));
        locationTxt = findViewById(R.id.location_txt);
        locationTxt.setText(getString(R.string.location)+":");
        locationValue = findViewById(R.id.location_value);
        locationValue.setText(job.getLocationName());
        //deviceSerialTxt = findViewById(R.id.device_serial_txt);
        //deviceSerialTxt.setText(getString(R.string.device_serial_no)+":");
        //deviceSerialValue = findViewById(R.id.device_serial_value);
        //deviceSerialValue.setText(UGMUtility.getStringPreference(PREFS_FULL_NAME, ""));
        firstRodTxt = findViewById(R.id.first_rod_txt);
        firstRodTxt.setText(getString(R.string.first_rod_length)+":");
        firstRodValue = findViewById(R.id.first_rod_value);
        firstRodValue.setText(job.getFirstRodLength());
        companyNameTxt = findViewById(R.id.company_name_txt);
        companyNameTxt.setText(getString(R.string.company_name)+":");
        companyNameValue = findViewById(R.id.company_name_value);
        companyNameValue.setText(job.getCompanyName());
        defaultRodTxt = findViewById(R.id.default_rod_txt);
        defaultRodTxt.setText(getString(R.string.default_rod_length)+":");
        defaultRodValue = findViewById(R.id.default_rod_value);
        defaultRodValue.setText(job.getDefaultRodLength());
        clientNameTxt = findViewById(R.id.client_name_txt);
        clientNameTxt.setText(getString(R.string.client_name)+":");
        clientNameValue = findViewById(R.id.client_name_value);
        clientNameValue.setText(job.getClientName());
        dataPointTxt = findViewById(R.id.data_points_txt);
        dataPointTxt.setText(getString(R.string.data_points)+":");
        dataPointValue = findViewById(R.id.data_points_value);
        if(null != records) {
            dataPointValue.setText(String.valueOf(records.size()));
        } else {
            dataPointValue.setText("0");
        }
        depthTxt = findViewById(R.id.depth_txt);
        depthTxt.setText(getString(R.string.text_depth)+":");
        depthValue = findViewById(R.id.depth_value);
        depthValue.setText(job.getPrefDepth());

        if(UGMMacros.VALUE_DEPTH_METER.equalsIgnoreCase(job.getPrefDepth())) {
            depthValue.setText(getString(R.string.meter));
        } else {
            depthValue.setText(getString(R.string.feet));
        }

        pitchTxt = findViewById(R.id.pitch_txt);
        pitchTxt.setText(getString(R.string.text_pitch)+":");
        pitchValue = findViewById(R.id.pitch_value);
        pitchValue.setText(job.getPrefPitch());

        if(UGMMacros.VALUE_PITCH_PERCENTAGE.equalsIgnoreCase(job.getPrefPitch())) {
            pitchValue.setText(getString(R.string.percentage));
        } else {
            pitchValue.setText(getString(R.string.degree_text));
        }

        descriptionTxt = findViewById(R.id.description_txt);
        descriptionTxt.setText(getString(R.string.description));
        descriptionValue = findViewById(R.id.description_value);
        descriptionValue.setText(job.getJobDescription());
        mapLayout = findViewById(R.id.map_layout);
        if(UGMUtility.isGoogleMapEnabled()){
            mapLayout.setVisibility(View.VISIBLE);
        } else {
            mapLayout.setVisibility(View.GONE);
        }
    }

    public void rodwiseViewClick(View view) {
        if(null != records && 0 < records.size()) {
            Intent intent = new Intent(ReportViewActivity.this, ReportRodwiseViewActivity.class);
            intent.putExtra("JOB_ITEM", job);
            intent.putExtra("RECORD_LIST", records);
            startActivity(intent);
        } else {
            showMessage(getString(R.string.please_create_records_for_view), getString(R.string.ok_caps), null);
        }
    }

    public void chartViewClick(View view) {
        if(null != records && 0 < records.size()) {
            Intent intent = new Intent(ReportViewActivity.this, ReportChartViewActivity.class);
            intent.putExtra("JOB_ITEM", job);
            intent.putExtra("RECORD_LIST", records);
            startActivity(intent);
        } else {
            showMessage(getString(R.string.please_create_records_for_view), getString(R.string.ok_caps), null);
        }
    }


    public void mapViewClick(View view) {
        //Toast.makeText(this, "MapView Clicked", Toast.LENGTH_SHORT).show();
        if(null != records && 0 < records.size()) {
            Intent intent = new Intent(ReportViewActivity.this, ReportMapViewActivity.class);
            intent.putExtra("JOB_ITEM", job);
            intent.putExtra("RECORD_LIST", records);
            startActivity(intent);
        } else {
            showMessage(getString(R.string.please_create_records_for_view), getString(R.string.ok_caps), null);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.generate_pdf_report_menu:
                File pdfFile = ReportPDFViewModel.getPDFFile(ReportViewActivity.this, getReportName(job));
                Document pdfDocument = ReportPDFViewModel.openDocument(pdfFile);
                int recordCnt = 0;
                if(null != records) {
                    recordCnt = records.size();
                }
                Document jobContentDocument = ReportPDFViewModel.writeJobContentDocument(pdfDocument, job, recordCnt);
                Document recordContentDocument = ReportPDFViewModel.writeRecordContentDocument(jobContentDocument, records, job);
                Document chartDocument = ReportPDFViewModel.addChartScreen(ReportViewActivity.this, recordContentDocument, job);
                Document mapDocument = ReportPDFViewModel.addMapScreen(ReportViewActivity.this, chartDocument, job);
                ReportPDFViewModel.closeDocument(mapDocument);

                shareReport(job);

                break;
        }
        return true;
    }

    private String getReportName(Job job) {
        StringBuilder builder = new StringBuilder();
        builder.append(job.getJobName());
        builder.append("_");
        builder.append(job.getLocationName());
        builder.append("_Report.pdf");
        return builder.toString();
    }

    private void shareReport(Job job) {
        File rootFolder = getExternalFilesDir(Environment.MEDIA_SHARED);
        File pdfFolder = new File(rootFolder, "UGMReport");
        if (pdfFolder.exists()) {
            File pdfFile = new File(rootFolder, getReportName(job));
            if(pdfFile.exists()) {
                Uri sharedFileUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, pdfFile);
                if(null != sharedFileUri) {
                    Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sendIntent.setType("application/pdf");
                    sendIntent.putExtra(Intent.EXTRA_STREAM, sharedFileUri);
                    startActivity(sendIntent);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.generate_report_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
