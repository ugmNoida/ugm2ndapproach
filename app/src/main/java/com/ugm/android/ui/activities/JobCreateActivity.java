package com.ugm.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ugm.android.R;
import com.ugm.android.database.entity.Job;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;
import com.ugm.android.webservice.JobResponse;
import com.ugm.android.webservice.SendJobData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobCreateActivity extends AbstractBaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private static Logger logger = LoggerFactory.getLogger(JobCreateActivity.class);
    private Button nextButton;
    private TextInputLayout jobNameLayout, locLayout, firstRodLayout, defaultRodLayout, companyLayout, clientLayout, descLayout;
    private TextInputEditText jobName, jobLocation, firstRodLen, defaultRodLen, companyName, clientName, description;
    private RadioGroup depthRadioGroup;
    private RadioGroup pitchRadioGroup;
    private String loggedInUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jobcreate_activity);
        loggedInUserId = UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_USERID, null);
        initActionBar(getString(R.string.create_new_job));
        initView();
    }

    private void initView() {
        logger.info("Initializing the view");
        jobName = (TextInputEditText)findViewById(R.id.name);
        jobNameLayout = (TextInputLayout)findViewById(R.id.job_name);
        validateErrorCheck(jobNameLayout);
        jobLocation = (TextInputEditText)findViewById(R.id.location);
        locLayout = (TextInputLayout)findViewById(R.id.job_location);
        validateErrorCheck(locLayout);
        firstRodLen = (TextInputEditText)findViewById(R.id.first_rod_length);
        firstRodLayout = (TextInputLayout)findViewById(R.id.job_first_rod_length);
        validateErrorCheck(firstRodLayout);
        defaultRodLen = (TextInputEditText)findViewById(R.id.default_rod_length);
        defaultRodLayout = (TextInputLayout)findViewById(R.id.job_default_rod_length);
        validateErrorCheck(defaultRodLayout);
        companyName = (TextInputEditText)findViewById(R.id.company_name);
        companyLayout = (TextInputLayout)findViewById(R.id.job_company_name);
        validateErrorCheck(companyLayout);
        clientName = (TextInputEditText)findViewById(R.id.client_name);
        clientLayout = (TextInputLayout)findViewById(R.id.job_client_name);
        validateErrorCheck(clientLayout);
        description = (TextInputEditText)findViewById(R.id.description);
        descLayout = (TextInputLayout)findViewById(R.id.job_description);
        validateErrorCheck(descLayout);
        nextButton = (Button)findViewById(R.id.next);
        nextButton.setOnClickListener(this);

        TextView depthTxt = (TextView)findViewById(R.id.depth_txt);
        depthTxt.setText(getString(R.string.text_depth));
        TextView pitchTxt = (TextView)findViewById(R.id.pitch_txt);
        pitchTxt.setText(getString(R.string.text_pitch));
        depthRadioGroup = (RadioGroup)findViewById(R.id.depth_radio_group);
        depthRadioGroup.setOnCheckedChangeListener(this);
        RadioButton meterBtn = (RadioButton)depthRadioGroup.findViewById(R.id.depth_meter);
        RadioButton feetBtn = (RadioButton)depthRadioGroup.findViewById(R.id.depth_feet);
        pitchRadioGroup = (RadioGroup)findViewById(R.id.pitch_radio_group);
        pitchRadioGroup.setOnCheckedChangeListener(this);
        RadioButton percentageBtn = (RadioButton)pitchRadioGroup.findViewById(R.id.pitch_percentage);
        RadioButton degreeBtn = (RadioButton)pitchRadioGroup.findViewById(R.id.pitch_degree);
        if(UGMUtility.getCurrentLocaleCountry(JobCreateActivity.this).equalsIgnoreCase(UGMMacros.LOCALE_COUNTRY_CODE_IN)) {
            meterBtn.setChecked(true);
            percentageBtn.setChecked(true);
            firstRodLayout.setHint(getString(R.string.first_rod_length)+" "+UGMUtility.getDefaultDepth(true));
            defaultRodLayout.setHint(getString(R.string.default_rod_length)+" "+UGMUtility.getDefaultDepth(true));
        } else {
            feetBtn.setChecked(true);
            degreeBtn.setChecked(true);
            firstRodLayout.setHint(getString(R.string.first_rod_length)+" "+UGMUtility.getDefaultDepth(false));
            defaultRodLayout.setHint(getString(R.string.default_rod_length)+" "+UGMUtility.getDefaultDepth(false));
        }
    }

    private Job getViewJob() {
        return Job.createOrUpdateJob(loggedInUserId, jobName.getText().toString().trim(), jobLocation.getText().toString(),
                companyName.getText().toString(), clientName.getText().toString(),
                description.getText().toString(), firstRodLen.getText().toString(),
                defaultRodLen.getText().toString(), null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        //int depthSelectedBtn = depthRadioGroup.getCheckedRadioButtonId();
        if(R.id.depth_meter == checkedId) {
            firstRodLayout.setHint(getString(R.string.first_rod_length)+" "+UGMUtility.getDefaultDepth(true));
            defaultRodLayout.setHint(getString(R.string.default_rod_length)+" "+UGMUtility.getDefaultDepth(true));
        } else if(R.id.depth_feet == checkedId) {
            firstRodLayout.setHint(getString(R.string.first_rod_length)+" "+UGMUtility.getDefaultDepth(false));
            defaultRodLayout.setHint(getString(R.string.default_rod_length)+" "+UGMUtility.getDefaultDepth(false));
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.next){
            boolean isErrorExist = false;
            Job job = getViewJob();
            if(null == job.getJobName() || job.getJobName().isEmpty()) {
                setInputLayoutError(jobNameLayout);
                isErrorExist = true;
            }
            else if(null == job.getLocationName() || job.getLocationName().isEmpty()) {
                setInputLayoutError(locLayout);
                isErrorExist = true;
            }
            else if(null == job.getFirstRodLength() || job.getFirstRodLength().isEmpty()) {
                setInputLayoutError(firstRodLayout);
                isErrorExist = true;
            }
            else if(null == job.getDefaultRodLength() || job.getDefaultRodLength().isEmpty()) {
                setInputLayoutError(defaultRodLayout);
                isErrorExist = true;
            }
            else if(null == job.getCompanyName() || job.getCompanyName().isEmpty()) {
                setInputLayoutError(companyLayout);
                isErrorExist = true;
            }
            else if(null == job.getClientName() || job.getClientName().isEmpty()) {
                setInputLayoutError(clientLayout);
                isErrorExist = true;
            }
            else if(null == job.getJobDescription() || job.getJobDescription().isEmpty()) {
                setInputLayoutError(descLayout);
                isErrorExist = true;
            }
            if(!isErrorExist) {
                createJob(job);
            }
        }
    }

    private void validateErrorCheck(final TextInputLayout inputLayout) {
        inputLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() < 1) {
                    inputLayout.setErrorEnabled(true);
                    setInputLayoutError(inputLayout);
                }
                if (s.length() > 0) {
                    inputLayout.setError(null);
                    inputLayout.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setInputLayoutError(TextInputLayout inputLayout) {
        if(R.id.job_name == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_job_name));
        } else if(R.id.job_location == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_location));
        } else if(R.id.job_first_rod_length == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_first_rod_length));
        } else if(R.id.job_default_rod_length == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_default_rod_length));
        } else if(R.id.job_company_name == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_company));
        } else if(R.id.job_client_name == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_client));
        } else if(R.id.job_description == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_description));
        } else {
            inputLayout.setError(getString(R.string.please_enter_value));
        }
    }

    private void createJob(Job job) {
        try {
            List<Job> jobs = getLocalDB().getJobDao().getJobsByUserIdAndJobName(loggedInUserId, job.getJobName());
            if (null != jobs && 0 < jobs.size()) {
                logger.info(job.getJobName() + " is already exist.");
                showMessage(job.getJobName() + " "+getString(R.string.is_already_exists), getString(R.string.ok_caps), null);
            } else {
                int depthSelectedBtn = depthRadioGroup.getCheckedRadioButtonId();
                if(R.id.depth_meter == depthSelectedBtn) {
                    job.setPrefDepth(UGMMacros.VALUE_DEPTH_METER);
                } else if(R.id.depth_feet == depthSelectedBtn) {
                    job.setPrefDepth(UGMMacros.VALUE_DEPTH_FEET);
                }
                int pitchSelectedBtn = pitchRadioGroup.getCheckedRadioButtonId();
                if(R.id.pitch_percentage == pitchSelectedBtn) {
                    job.setPrefPitch(UGMMacros.VALUE_PITCH_PERCENTAGE);
                } else if(R.id.pitch_degree == pitchSelectedBtn) {
                    job.setPrefPitch(UGMMacros.VALUE_PITCH_DEGREE);
                }
                getLocalDB().getJobDao().insert(job);
                logger.info(job.getJobName() + " is inserted in database successfully.");
                if(UGMUtility.isNetworkAvailable() && UGMUtility.getBooleanPreference(UGMMacros.VALUE_ONLINE_SUPPORT, true)) {
                    postJob(job);
                } else {
                    openRecordCreateActivity(job);
                }
            }
        } catch(Exception ex){
            logger.error("Exception while creating new JobData." + ex.toString());
            showMessage(getString(R.string.unexpected_error), getString(R.string.ok), null);
        }
    }

    private void openRecordCreateActivity(Job job) {
        Intent intent = new Intent(this,RecordCreateActivity.class);
        intent.putExtra("JOB_ITEM",job);
        startActivity(intent);
        finish();
    }

    private void postJob(final Job job) {
        showProgressDialog();
        JsonArray jsonArray = new JsonArray();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id",job.getJobId());
        jsonObject.addProperty("user_id", loggedInUserId);
        jsonObject.addProperty("jobName",job.getJobName());
        jsonObject.addProperty("jobDescription",job.getJobDescription());
        jsonObject.addProperty("clientName",job.getClientName());
        jsonObject.addProperty("companyName",job.getCompanyName());
        jsonObject.addProperty("defaultRodLength",job.getDefaultRodLength());
        jsonObject.addProperty("firstRodLength",job.getFirstRodLength());
        jsonObject.addProperty("locationName",job.getLocationName());
        jsonObject.addProperty("status",job.getStatus());
        jsonObject.addProperty("createdDate",String.valueOf(job.getCreatedTime()));
        jsonObject.addProperty("updatedDate",String.valueOf(job.getUpdatedTime()));
        jsonObject.addProperty("prefDepth",job.getPrefDepth());
        jsonObject.addProperty("prefPitch",job.getPrefPitch());
        jsonArray.add(jsonObject);

        final String token = "Bearer " + UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_SESSION_TOKEN, "");
        SendJobData sendJobData = RetrofitClientInstance.getRetrofitInstance().create(SendJobData.class);
        Call<JobResponse> sendJobCall = sendJobData.sendJobData(token, jsonArray);
        logger.info("#######JobCreateActivity:postJob:Request JSON:"+new Gson().toJson(jsonArray));
        sendJobCall.enqueue(new Callback<JobResponse>() {
            @Override
            public void onResponse(Call<JobResponse> responseCall, Response<JobResponse> sendresponse) {
                JobResponse jobResponse = sendresponse.body();
                logger.info("#######JobCreateActivity:postJob:Response JSON:"+new Gson().toJson(jobResponse));
                if (jobResponse != null && jobResponse.getStatus().equalsIgnoreCase("success")) {
                    job.setSynced(true);
                    getLocalDB().getJobDao().update(job);
                    logger.info(job.getJobName() + " Job uploaded to server successfully.");
                }
                hideProgressDialog();
                openRecordCreateActivity(job);
            }

            @Override
            public void onFailure(Call<JobResponse> call, Throwable t) {
                logger.info("Job uploaded to server unsuccessfully.");
                hideProgressDialog();
                openRecordCreateActivity(job);
            }
        });
    }
}
