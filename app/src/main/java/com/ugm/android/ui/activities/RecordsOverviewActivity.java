package com.ugm.android.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.ugm.android.R;
import com.ugm.android.database.entity.Job;
import com.ugm.android.database.entity.Record;
import com.ugm.android.ui.adapters.RecordPagerAdapter;
import com.ugm.android.ui.fragments.JobMapViewFragment;
import com.ugm.android.utilities.UGMMacros;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class RecordsOverviewActivity extends AbstractBaseActivity implements ViewPager.OnPageChangeListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RecordPagerAdapter viewPagerAdapter;
    private Job mJob;
    private ArrayList<Record> records;
    private BottomSheetDialog bottomSheetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records_overview);
        mJob = processIntent(getIntent());
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPagerAdapter = new RecordPagerAdapter(getSupportFragmentManager(), mJob);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(this);
        viewPager.setOffscreenPageLimit(0);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(mJob.getJobName());
        toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public Job getJobItem(){
        return mJob;
    }

    public void setRecords(List<Record> records) {
        this.records = new ArrayList<>(records);
    }

    public ArrayList<Record> getRecords() {
        if(null == records) {
            records = new ArrayList<Record>();
        }
        return records;
    }

    private Job processIntent(Intent intent){
        if(intent != null){
            return (Job)intent.getSerializableExtra("JOB_ITEM");
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.record_overview_menu, menu);
        View view = getLayoutInflater().inflate(R.layout.job_bottomsheet_layout, null);
        LinearLayout saveLayout = (LinearLayout)view.findViewById(R.id.save_layout);
        if(UGMMacros.JOB_COMPLETED.equalsIgnoreCase(mJob.getStatus())) {
            saveLayout.setVisibility(View.GONE);
        } else {
            saveLayout.setVisibility(View.VISIBLE);
        }
        bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(view);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        bottomSheetDialog.show();
        return true;
    }

    public void saveJob(View view) {
        bottomSheetDialog.dismiss();
        mJob.setSynced(false);
        mJob.setStatus(UGMMacros.JOB_COMPLETED);
        mJob.setUpdatedTime(Calendar.getInstance().getTimeInMillis());
        getLocalDB().getJobDao().update(mJob);
        showMessage(getString(R.string.job_completed_successfully), getString(R.string.ok), this);
    }

    public void generateReport(View view) {
        bottomSheetDialog.dismiss();
        Intent reportIntent = new Intent(RecordsOverviewActivity.this, ReportViewActivity.class);
        reportIntent.putExtra("JOB_ITEM", mJob);
        reportIntent.putExtra("RECORD_LIST", records);
        startActivity(reportIntent);
    }

    public void deleteJob(View view) {
        bottomSheetDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.are_you_deleting_job)+" " + mJob.getJobName() + " "+getString(R.string.please_confirm));
        builder.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                mJob.setSynced(false);
                mJob.setStatus(UGMMacros.JOB_SOFT_DELETED);
                mJob.setUpdatedTime(Calendar.getInstance().getTimeInMillis());
                getLocalDB().getJobDao().update(mJob);
                showMessage(getString(R.string.job_moved_to_bin_successfully), getString(R.string.ok), RecordsOverviewActivity.this);
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        Log.d("Adapter","OnPageSelected  = "+i);
        if(viewPagerAdapter.getCurrFragment() instanceof JobMapViewFragment){
            Log.d("Adapter","OnPageSelected instanceof  JobMapViewFragment = ");
        } else {
            Log.d("Adapter","OnPageSelected  = "+i);
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
