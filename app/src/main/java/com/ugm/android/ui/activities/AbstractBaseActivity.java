package com.ugm.android.ui.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.ugm.android.R;
import com.ugm.android.UGMApplication;
import com.ugm.android.bluetooth.LocatorDevice;
import com.ugm.android.database.UGMDatabase;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractBaseActivity extends AppCompatActivity implements UGMMacros {

    private static Logger logger = LoggerFactory.getLogger(AbstractBaseActivity.class);

    private UGMApplication ugmApplication;
    private ProgressDialog progressDialog;
    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ugmApplication = UGMApplication.getInstance();
        mContext = this;
    }

    public UGMApplication getUGMApplication() {
        if (null == ugmApplication) {
            ugmApplication = (UGMApplication) getApplicationContext();
        }
        return ugmApplication;
    }

    public UGMDatabase getLocalDB() {
        return getUGMApplication().getDatabase();
    }

    public void initActionBar(String title) {
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
        Window window = this.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    public void logout() {
        logger.info("AbstractBaseActivity:User LogOut");
        LocatorDevice.getInstance(this).disconnect();
        RetrofitClientInstance.clearAllObjects();
        UGMUtility.setStringPreference(PREFS_LOGGEDIN_USERID, null);
        UGMUtility.setStringPreference(PREFS_LOGGEDIN_SESSION_TOKEN, null);
        UGMUtility.setStringPreference(PREFS_LOGGEDIN_EMAIL, null);
        Intent mIntent = new Intent(AbstractBaseActivity.this, LoginActivity.class);
        finishAffinity();
        startActivity(mIntent);
    }

    public void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.loading_text));
        }
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void hideProgressDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception ex) {
            logger.info("AbstractBaseActivity:hideProgressDialog:" + ex.toString());
        }
    }

    public void showMessage(String message, String action, final Activity toBeFinish) {
        int duration = 2000;
        if (null == toBeFinish) {
            duration = 5000;
        }
        final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), message, duration);
        if (null != action && !action.isEmpty()) {
            snackbar.setAction(action, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            });
            snackbar.addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                @Override
                public void onDismissed(Snackbar transientBottomBar, int event) {
                    super.onDismissed(transientBottomBar, event);
                    if (null != toBeFinish) {
                        finish();
                    }
                }

                @Override
                public void onShown(Snackbar transientBottomBar) {
                    super.onShown(transientBottomBar);
                }
            });
        }
        snackbar.show();
    }

    public boolean isGPSEnable() {
        LocationManager locManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        return locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /*public void showGpsDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(0);
        locationRequest.setFastestInterval(0);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        SettingsClient client = LocationServices.getSettingsClient(AbstractBaseActivity.this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                //No need to do anything
                logger.info("AbstractBaseActivity:showGpsDialog: GPS Enable");
            }
        });
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        logger.info("AbstractBaseActivity:showGpsDialog:Location Setting GPS asking dialog to user");
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(AbstractBaseActivity.this, UGMMacros.REQUEST_CODE_GPS_LOCATION_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        logger.info("AbstractBaseActivity:showGpsDialog:Exception while enable GPS dialog");
                    }
                }
            }
        });
    }*/

    /*public boolean checkPlayServices() {
        boolean playServicesAvailable = false;
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(getUGMApplication());
        if (connectionStatusCode != ConnectionResult.SUCCESS) {
            String errorText = apiAvailability.getErrorString(connectionStatusCode);
            if (!(apiAvailability.isUserResolvableError(connectionStatusCode))) {
                errorText = getString(R.string.play_service_not_supported);
            }
            //TODO: Send in-app notification to user.
            //sendGooglePlayServicesNotification(errorText);
            playServicesAvailable = false;
        }
        playServicesAvailable = true;
        logger.info("AbstractBaseActivity:playServicesAvailable");
        return playServicesAvailable;
    }*/

    public String removePrefixEmail(String emailId) {
        RetrofitClientInstance.clearAllObjects();
        if (emailId.startsWith(UGMMacros.UGM_PREFIX)) {
            int index = emailId.indexOf(UGMMacros.UGM_PREFIX);
            emailId = emailId.substring(index + 1, emailId.length());
            UGMUtility.setBooleanPreference(PREFS_IS_PRODUCTION, false);
            logger.info("URL is pointing to Staging.");
        } else {
            UGMUtility.setBooleanPreference(PREFS_IS_PRODUCTION, true);
            logger.info("URL is pointing to Production.");
        }
        return emailId;
    }

    public boolean isEmailValid(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isPasswordValid(String password) {
        return password.length() > 5;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == UGMMacros.REQUEST_CODE_GPS_LOCATION_SETTINGS) {
                logger.info("AbstractBaseActivity:onActivityResult:Came back from Location Settings");
                if (isGPSEnable()) {
                    //showGpsDialog();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
