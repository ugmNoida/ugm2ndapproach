package com.ugm.android.ui.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.ugm.android.R;
import com.ugm.android.database.entity.Job;
import com.ugm.android.datamodel.JobItemListModel;
import com.ugm.android.ui.activities.ContainerActivity;
import com.ugm.android.ui.adapters.RecyclerDeletedJobListAdapter;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;
import com.ugm.android.webservice.DeleteJobData;
import com.ugm.android.webservice.JobResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeletedJobListFragment extends Fragment implements RecyclerDeletedJobListAdapter.JobItemMenuClickListener {

    private static Logger logger = LoggerFactory.getLogger(DeletedJobListFragment.class);

    private ContainerActivity activity;
    private RecyclerView recyclerView;
    private RelativeLayout jobListEmptyView;
    private RecyclerView.LayoutManager recycleLayoutManager;
    private RecyclerDeletedJobListAdapter jobListAdapter;
    private BottomSheetDialog bottomSheetDialog;
    private String loggedInUserId;

    public DeletedJobListFragment() {
        // Required empty public constructor
    }

    public static DeletedJobListFragment newInstance() {
        DeletedJobListFragment fragment = new DeletedJobListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ContainerActivity)
            activity = (ContainerActivity)context;
        else{
            logger.error("onAttach context != ContainerActivity ***");
            activity = null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loggedInUserId = UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_USERID, null);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_deleted_job_list, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        activity.getSupportActionBar().setTitle(getString(R.string.bin));
        activity.getSupportActionBar().setSubtitle(getString(R.string.zero_jobs_found));
        jobListEmptyView = (RelativeLayout)view.findViewById(R.id.joblist_empty_view);
        jobListEmptyView.setVisibility(View.VISIBLE);
        recyclerView = (RecyclerView) view.findViewById(R.id.joblist_recycler_view);
        recyclerView.setVisibility(View.GONE);
        getAllDeletedJobs();
    }

    private void getAllDeletedJobs() {
        List<Job> jobs = activity.getLocalDB().getJobDao().getJobsByUserId(loggedInUserId, activity.getSoftDeleteJobStatuses());
        refreshList(jobs);
    }

    private void refreshList(List<Job> jobs) {
        if(null != jobs && 0 < jobs.size()) {
            if(null != jobs && 0 < jobs.size()) {
                if(10 < jobs.size()) {
                    activity.getSupportActionBar().setSubtitle(getString(R.string.ten_jobs_found));
                } else {
                    activity.getSupportActionBar().setSubtitle(jobs.size() + getString(R.string.jobs_found));
                }
                jobListEmptyView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView.setHasFixedSize(true);
                recycleLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(recycleLayoutManager);
                ArrayList<JobItemListModel> binJobList = JobItemListModel.getDeletedJobItemList(jobs);
                jobListAdapter = new RecyclerDeletedJobListAdapter(binJobList, DeletedJobListFragment.this);
                recyclerView.setAdapter(jobListAdapter);
            } else {
                jobListEmptyView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        } else {
            activity.getSupportActionBar().setSubtitle(jobs.size() + " jobs found");
            jobListEmptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onJobItemMenuClick(final Job jobItem) {
        View view = getLayoutInflater().inflate(R.layout.bin_bottomsheet_layout, null);
        LinearLayout moveToJobList = (LinearLayout)view.findViewById(R.id.move_to_job_list);
        moveToJobList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                jobItem.setSynced(false);
                jobItem.setStatus(UGMMacros.JOB_INPROGRESS);
                jobItem.setUpdatedTime(Calendar.getInstance().getTimeInMillis());
                activity.getLocalDB().getJobDao().update(jobItem);
                getAllDeletedJobs();
                activity.showMessage(getString(R.string.job_data)+" " + jobItem.getJobName() + " "+getString(R.string.jobs_has_been_moved_success), getString(R.string.ok), null);
            }
        });
        LinearLayout jobDelete = (LinearLayout)view.findViewById(R.id.delete_job);
        jobDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                deleteJob(jobItem);
            }
        });
        bottomSheetDialog = new BottomSheetDialog(activity);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.show();
    }

    private void deleteJob(final Job job) {
        bottomSheetDialog.dismiss();
        if(UGMUtility.isNetworkAvailable()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage(getString(R.string.are_you_deleting_job)+" " + job.getJobName() + " "+getString(R.string.please_confirm));
            builder.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    deleteJobAndRecord(job);
                }
            });
            builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.create();
            builder.show();
        } else {
            activity.showMessage(getString(R.string.network_unavailable), getString(R.string.ok), null);
        }
    }

    @Override
    public void onEmptyBinAll() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(getString(R.string.you_are_about_to_delete_all_data));
        builder.setPositiveButton(getString(R.string.ok_caps), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setNegativeButton(getString(R.string.cancel_caps), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void deleteJobAndRecord(final Job job) {
        final String token = "Bearer " + UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_SESSION_TOKEN, "");
        if(null != job && UGMUtility.isValidString(token)) {
            activity.showProgressDialog();
            try {
                DeleteJobData deleteJobData = RetrofitClientInstance.getRetrofitInstance().create(DeleteJobData.class);
                Call<JobResponse> deleteCall = deleteJobData.deleteJobData(token, job.getJobId());
                logger.info("#######DeletedJobListFragment:deleteJobAndRecord:Request JSON:"+new Gson().toJson(deleteCall.request()));
                deleteCall.enqueue(new Callback<JobResponse>() {
                    @Override
                    public void onResponse(Call<JobResponse> call, Response<JobResponse> response) {
                        JobResponse jobResponse = response.body();
                        logger.info("#######DeletedJobListFragment:deleteJobAndRecord:Response JSON:"+new Gson().toJson(jobResponse));
                        activity.hideProgressDialog();
                        if (jobResponse != null) {
                            activity.getLocalDB().getJobDao().delete(job);
                            activity.showMessage(job.getJobName() + " "+getString(R.string.has_been_deleted), getString(R.string.ok), null);
                            getAllDeletedJobs();
                        }
                    }

                    @Override
                    public void onFailure(Call<JobResponse> call, Throwable t) {
                        activity.hideProgressDialog();
                    }
                });
            } catch (Exception ex) {
                activity.hideProgressDialog();
            }
        }
    }
}
