package com.ugm.android.ui.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ugm.android.R;
import com.ugm.android.bluetooth.BluetoothUtils;
import com.ugm.android.bluetooth.DeviceThreadControl;
import com.ugm.android.bluetooth.LocatorDevice;
import com.ugm.android.database.entity.Job;
import com.ugm.android.database.entity.Record;
import com.ugm.android.datamodel.ReportRecordModel;
import com.ugm.android.listeners.UGMLocationListener;
import com.ugm.android.location.UGMLocationManager;
import com.ugm.android.utilities.Constants;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;
import com.ugm.android.webservice.JobResponse;
import com.ugm.android.webservice.SendJobData;
import com.ugm.android.webservice.SendRecordData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecordCreateActivity extends AbstractBaseActivity implements View.OnClickListener, UGMLocationListener, EasyPermissions.PermissionCallbacks {

    private static Logger logger = LoggerFactory.getLogger(RecordCreateActivity.class);

    private TextView old_pitch_label, pitch_label, old_depth_label, depth_label, lat_label, log_label, temp_label;
    private TextView old_rodNoTxv, old_latTxv, old_longTxv, old_pitTxv, old_depthTxv, old_tempTxv, old_lat_labelTxv, old_log_labelTxv;
    private TextView pitchTxv, depthTxv, tempTxv, longTxv, latTxv, rodNoTxv, rodLengthTxv, rollTxv;
    private Button addRecordBtn, getAddDataBtn, getPitchBtn;
    private Job mJob = null;
    private Record mRecord = null;
    private Context mContext = null;
    private String latitude = "";
    private String longitude = "";
    private AlertDialog alertDialog;
    private boolean isPitchValid;

    private BluetoothAdapter mBluetoothAdapter = null;

    /*private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    private static final int REQUEST_ENABLE_BT_FAB = 4;
    private static final int REQUEST_ENABLE_BT_SYNC = 5;*/


    private Button bPitchDepth;
    private Button bPitchDepthLatLong;
    private boolean isPitchDepth = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logger.info("RecordCreateActivity:OnCreate():Start");
        setContentView(R.layout.activity_record_create);
        mContext = this;

        if (getIntent() != null)
            mJob = (Job) getIntent().getSerializableExtra("JOB_ITEM");
        logger.info("RecordCreateActivity:OnCreate():Job Name:" + mJob.getJobName());
        mRecord = Record.createRecord(mJob.getJobId(), 0, null, null, null, null, null, null, null, false);

        rodNoTxv = (TextView) findViewById(R.id.curr_no_val_txv);
        rodLengthTxv = (TextView) findViewById(R.id.rod_len_val_txv);
        pitchTxv = (TextView) findViewById(R.id.pit_val_txv);
        pitch_label = (TextView) findViewById(R.id.pitch_title_txv);

        bPitchDepth = findViewById(R.id.get_pitch_depth);
        bPitchDepth.setOnClickListener(this);

        bPitchDepthLatLong = findViewById(R.id.get_pitch_depth_latlong);
        bPitchDepthLatLong.setOnClickListener(this);

        mBluetoothAdapter = BluetoothUtils.getAdapter(mContext);
        if (mBluetoothAdapter == null) {
            if (mContext != null) {
                Toast.makeText(mContext, getString(R.string.bluetooth_is_not_available), Toast.LENGTH_SHORT).show();
            }
        }

        if (UGMMacros.VALUE_PITCH_PERCENTAGE.equalsIgnoreCase(mJob.getPrefPitch())) {
            pitch_label.setText(getString(R.string.text_pitch) + UGMUtility.getDefaultPitch(true));
        } else {
            pitch_label.setText(getString(R.string.text_pitch) + UGMUtility.getDefaultPitch(false));
        }

        depthTxv = (TextView) findViewById(R.id.dep_val_txv);
        depth_label = findViewById(R.id.depth_title_txv);
        temp_label = findViewById(R.id.temp_title_txv);
        if (UGMMacros.VALUE_DEPTH_METER.equalsIgnoreCase(mJob.getPrefDepth())) {
            depth_label.setText(getString(R.string.text_depth) + UGMUtility.getDefaultDepth(true));
            temp_label.setText(getString(R.string.text_temperature) + "(C)");
        } else {
            depth_label.setText(getString(R.string.text_depth) + UGMUtility.getDefaultDepth(false));
            temp_label.setText(getString(R.string.text_temperature) + "(F)");
        }
        tempTxv = (TextView) findViewById(R.id.temp_len_val_txv);

        lat_label = (TextView) findViewById(R.id.lat_title_txv);
        log_label = (TextView) findViewById(R.id.lon_title_txv);
        old_lat_labelTxv = (TextView) findViewById(R.id.latitude_txv);
        old_log_labelTxv = (TextView) findViewById(R.id.longitude_txv);

        latTxv = (TextView) findViewById(R.id.latitude_val_txv);
        longTxv = (TextView) findViewById(R.id.longitude_val_txv);
        old_latTxv = (TextView) findViewById(R.id.lat_val_txv);
        old_longTxv = (TextView) findViewById(R.id.lon_val_txv);


        rollTxv = (TextView) findViewById(R.id.roll_val_txv);
        old_rodNoTxv = (TextView) findViewById(R.id.rod_no_val_txv);
        old_pitTxv = (TextView) findViewById(R.id.pitch_val_txv);
        old_pitch_label = (TextView) findViewById(R.id.pitch_txv);
        if (UGMMacros.VALUE_PITCH_PERCENTAGE.equalsIgnoreCase(mJob.getPrefPitch())) {
            old_pitch_label.setText(getString(R.string.text_pitch) + UGMUtility.getDefaultPitch(true));
        } else {
            old_pitch_label.setText(getString(R.string.text_pitch) + UGMUtility.getDefaultPitch(false));
        }

        old_depthTxv = (TextView) findViewById(R.id.depth_val_txv);
        old_depth_label = (TextView) findViewById(R.id.depth_txv);
        if (UGMMacros.VALUE_DEPTH_METER.equalsIgnoreCase(mJob.getPrefDepth())) {
            old_depth_label.setText(getString(R.string.text_depth) + UGMUtility.getDefaultDepth(true));
        } else {
            old_depth_label.setText(getString(R.string.text_depth) + UGMUtility.getDefaultDepth(false));
        }
        old_tempTxv = (TextView) findViewById(R.id.temp_val_txv);

        addRecordBtn = (Button) findViewById(R.id.add_record);
        addRecordBtn.setOnClickListener(this);

        //getAddDataBtn = findViewById(R.id.get_all_data);
        //getAddDataBtn.setOnClickListener(this);

        getPitchBtn = findViewById(R.id.get_pitch_data);
        getPitchBtn.setOnClickListener(this);

        /*if (UGMUtility.isGoogleMapEnabled()) {
            latTxv.setVisibility(View.VISIBLE);
            longTxv.setVisibility(View.VISIBLE);
            old_latTxv.setVisibility(View.VISIBLE);
            old_longTxv.setVisibility(View.VISIBLE);

            lat_label.setVisibility(View.VISIBLE);
            log_label.setVisibility(View.VISIBLE);
            old_lat_labelTxv.setVisibility(View.VISIBLE);
            old_log_labelTxv.setVisibility(View.VISIBLE);
        } else {
            latTxv.setVisibility(View.GONE);
            longTxv.setVisibility(View.GONE);
            old_latTxv.setVisibility(View.GONE);
            old_longTxv.setVisibility(View.GONE);

            lat_label.setVisibility(View.GONE);
            log_label.setVisibility(View.GONE);
            old_lat_labelTxv.setVisibility(View.GONE);
            old_log_labelTxv.setVisibility(View.GONE);
        }*/

        getSupportActionBar().setTitle(mJob.getJobName());
        getSupportActionBar().setSubtitle(mJob.getLocationName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

        getLastRecord();

        if (LocatorDevice.getInstance(mContext).getState() != DeviceThreadControl.STATE_CONNECTED) {
            showDeviceDisconnectionDialog();
        }
        logger.info("RecordCreateActivity:OnCreate():End");
        //getMagAndLatLongData();
    }

    @Override
    public void onBackPressed() {
        logger.info("RecordCreateActivity:OnBackPresse():Call");
        finish();
    }

    private void getLastRecord() {
        Record record = getLocalDB().getRecordDao().getLastRecord(mJob.getJobId(), UGMMacros.RECORD_DELETED);
        if (record != null) {
            fetchLastRecord(record);
        } else {
            fetchLastRecord(null);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_CMD_VALUE_PITCH);
        filter.addAction(Constants.ACTION_CMD_VALUE_DEPTH);
        filter.addAction(Constants.ACTION_CMD_VALUE_TEMP);
        filter.addAction(Constants.ACTION_CMD_VALUE_ROLL);
        filter.addAction(Constants.ACTION_GPS);
        filter.addAction(Constants.ACTION_VALUE_DEVICE_CONNECTED);
        filter.addAction(Constants.ACTION_VALUE_CONNECTION_LOST);
        filter.addAction(Constants.ACTION_CMD_VALUE_PITCH_VALID);
        LocalBroadcastManager.getInstance(this).registerReceiver(commandReceiver, filter);
        logger.info("RecordCreateActivity:onStart():Registered commandReceiver.");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (UGMUtility.isGoogleMapEnabled())
            doStartLocation();
    }

    private void doStartLocation() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (!EasyPermissions.hasPermissions(this, perms)) {
            logger.info("RecordCreateActivity:doStartLocation():asking permission");
            requestLocationPermission(perms);
        } else if (!isGPSEnable()) {
            logger.info("RecordCreateActivity:doStartLocation():asking showGpsDialog");
            //    showGpsDialog();
        } else {
            logger.info("RecordCreateActivity:doStartLocation():asking startLocation");
            UGMLocationManager.getInstance().startLocation(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        logger.info("RecordCreateActivity:onPause()");
        /*if (UGMLocationManager.getInstance().isLocationStarted()) {
            UGMLocationManager.getInstance().stopLocation();
        }*/
    }

    @Override
    protected void onStop() {
        super.onStop();
        logger.info("RecordCreateActivity:onStop()");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(commandReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logger.info("RecordCreateActivity:onDestroy()");
    }

    public BroadcastReceiver commandReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {

                if (intent != null) {
                    if (intent.getAction().equalsIgnoreCase(Constants.ACTION_CMD_VALUE_PITCH_VALID)) {
                        logger.info("RecordCreateActivity:commandReceiver:pitch_valid:" + intent.getFloatExtra(Constants.PITCH_CMD_CODE_VALID, 0));
                        isPitchValid = intent.getFloatExtra(Constants.PITCH_CMD_CODE_VALID, 0) != 0;
                    } else if (intent.getAction().equalsIgnoreCase(Constants.ACTION_CMD_VALUE_PITCH)) {
                        String pitch = "-";
                        if (isPitchValid && intent.getFloatExtra(Constants.PITCH_CMD_CODE, 0) != 0) {
                            float fpitch = intent.getFloatExtra(Constants.PITCH_CMD_CODE, 0);
                            //String selectedPitch = UGMUtility.getStringPreference(UGMMacros.PREFS_PITCH, UGMMacros.VALUE_PITCH_PERCENTAGE);

                            if (UGMMacros.VALUE_PITCH_DEGREE.equalsIgnoreCase(mJob.getPrefPitch())) {
                                fpitch = (float) (Math.toDegrees(Math.atan(fpitch / 100)));
                            }
                            pitch = UGMUtility.floatFormat(fpitch);
                            logger.info("RecordCreateActivity:commandReceiver:pitch:" + pitch + " : For Rod Number:" + mRecord.getRodNumber());
                        }
                        pitchTxv.setText(pitch);
                        mRecord.setPitch(pitch);
                    } else if (intent.getAction().equalsIgnoreCase(Constants.ACTION_CMD_VALUE_TEMP)) {
                        String temp = "-";
                        if (isPitchValid && intent.getFloatExtra(Constants.TEMPERATURE_CMD_CODE, 0) != 0) {
                            float ftemp = intent.getFloatExtra(Constants.TEMPERATURE_CMD_CODE, 0);
                            if (UGMMacros.VALUE_DEPTH_FEET.equalsIgnoreCase(mJob.getPrefDepth())) {
                                int tempInt = (int) ((ftemp * 9 / 5) + 32);
                                temp = String.valueOf(tempInt);
                            } else {
                                temp = UGMUtility.floatFormat(ftemp);
                            }
                            logger.info("RecordCreateActivity:commandReceiver:temperature:" + temp + " : For Rod Number:" + mRecord.getRodNumber());
                        }
                        if (!mRecord.isOnlyPitch()) {
                            tempTxv.setText(temp);
                            mRecord.setTemperature(temp);
                        } else {
                            //tempTxv.setText("");
                            mRecord.setTemperature("");
                        }

                    } else if (intent.getAction().equalsIgnoreCase(Constants.ACTION_CMD_VALUE_DEPTH)) {
                        String depth = "-";
                        if (isPitchValid && intent.getFloatExtra(Constants.DISTANCE_CMD_CODE, 0) != 0) {
                            float fdepth = intent.getFloatExtra(Constants.DISTANCE_CMD_CODE, 0);
                            if (UGMMacros.VALUE_DEPTH_FEET.equalsIgnoreCase(mJob.getPrefDepth())) {
                                fdepth = (float) (fdepth / 0.3048);
                            }
                            depth = UGMUtility.floatFormat(fdepth);
                            logger.info("RecordCreateActivity:commandReceiver:depth:" + depth + " : For Rod Number:" + mRecord.getRodNumber());
                        }
                        if (!mRecord.isOnlyPitch()) {
                            depthTxv.setText(depth);
                            mRecord.setDepth(depth);
                        } else {
                            //depthTxv.setText("");
                            mRecord.setDepth("");
                        }
                    } else if (intent.getAction().equalsIgnoreCase(Constants.ACTION_CMD_VALUE_ROLL)) {
                        String roll = "-";
                        if (isPitchValid && intent.getFloatExtra(Constants.ROLL_CMD_CODE, 0) != 0) {
                            float froll = intent.getFloatExtra(Constants.ROLL_CMD_CODE, 0);
                            //depth = String.valueOf(intent.getFloatExtra(Constants.DISTANCE_CMD_CODE,0));
                            roll = UGMUtility.floatFormat(froll);
                            logger.info("RecordCreateActivity:commandReceiver:roll:" + roll + " : For Rod Number:" + mRecord.getRodNumber());
                        }
                        if (!mRecord.isOnlyPitch()) {
                            rollTxv.setText(roll);
                            mRecord.setRoll(roll);
                        } else {
                            //rollTxv.setText("");
                            mRecord.setRoll("");
                        }

                    } else if (intent.getAction().equalsIgnoreCase(Constants.ACTION_VALUE_CONNECTION_LOST)) {
                        logger.info("RecordCreateActivity:commandReceiver:Disconnected:" + intent.getAction());
                        if (mRecord != null)
                            mRecord.setOnlyPitch(false);

                        showDeviceDisconnectionDialog();
                    } else if (intent.getAction().equalsIgnoreCase(Constants.ACTION_VALUE_DEVICE_CONNECTED)) {
                        logger.info("RecordCreateActivity:commandReceiver:Connected:" + intent.getAction());
                        if (mRecord != null)
                            mRecord.setOnlyPitch(false);
                    } else if (intent.getAction().equalsIgnoreCase(Constants.ACTION_GPS)) {

                        logger.info("RecordCreateActivity:commandReceiver:GPSDevice updateUI:" + intent.getAction());
                        double longitude = intent.getDoubleExtra(Constants.ACTION_GPS_LONGITUDE, 0);
                        double latitude = intent.getDoubleExtra(Constants.ACTION_GPS_LATITUDE, 0);
                        double altitude = intent.getDoubleExtra(Constants.ACTION_GPS_ALTITUDE, 0);

                        DecimalFormat decimalFormat8 = new DecimalFormat(".00000000");//padding 0's.

                        longTxv.setText(decimalFormat8.format(longitude));
                        mRecord.setLongitude(decimalFormat8.format(longitude));

                        latTxv.setText(decimalFormat8.format(latitude));
                        mRecord.setLatitude(decimalFormat8.format(latitude));

                    }
                }
            } catch (Exception ex) {
                logger.info("RecordCreateActivity:Exception commandListener ex = " + ex.toString());
            }
        }
    };

    private void addRodAllData() {
        isPitchDepth = false;

        mRecord.setOnlyPitch(false);
        createRecord();
    }

    public void showDeviceDisconnectionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.device_got_disconnected));
        builder.setPositiveButton(getString(R.string.ok_caps), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (alertDialog != null)
                    alertDialog.dismiss();
                logger.info("RecordCreateActivity:showDeviceDisconnectionDialog:OK button click");
                Intent a = new Intent(mContext, ContainerActivity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(a);
            }
        });

        builder.setNegativeButton(getString(R.string.cancel_caps), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (alertDialog != null)
                    alertDialog.dismiss();
                logger.info("RecordCreateActivity:showDeviceDisconnectionDialog:Cancel button click");
            }
        });


        alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    private void ResetUI() {
        int newRodNo = mRecord.getRodNumber() + 1;
        old_rodNoTxv.setText(String.valueOf(mRecord.getRodNumber()));
        if (UGMUtility.isValidDoubleString(mRecord.getLatitude()) && !mRecord.isOnlyPitch() && !isPitchDepth) {
            old_latTxv.setText(mRecord.getLatitude());
        } else {
            old_latTxv.setText("");
        }
        if (UGMUtility.isValidDoubleString(mRecord.getLongitude()) && !mRecord.isOnlyPitch() && !isPitchDepth) {
            old_longTxv.setText(mRecord.getLongitude());
        } else {
            old_longTxv.setText("");
        }

        old_pitTxv.setText(mRecord.getPitch());
        if (mRecord.isOnlyPitch()) {
            old_depthTxv.setText("");
            rollTxv.setText("");
            old_tempTxv.setText("");

        } else if (!mRecord.isOnlyPitch() && isPitchDepth) {
            rollTxv.setText("");
            old_depthTxv.setText(mRecord.getDepth());
            old_tempTxv.setText("");
        } else {
            old_depthTxv.setText(mRecord.getDepth());
            old_tempTxv.setText(mRecord.getTemperature());
            rollTxv.setText(mRecord.getRoll());
        }

        mRecord = null;
        mRecord = Record.createRecord(mJob.getJobId(), newRodNo, mJob.getDefaultRodLength(), null, latitude, longitude, null, null, null, false);

        rodNoTxv.setText(String.valueOf(newRodNo));

        if (newRodNo == 1) {
            rodLengthTxv.setText(mJob.getFirstRodLength());
            mRecord.setRodLength(mJob.getFirstRodLength());
        } else {
            rodLengthTxv.setText(mJob.getDefaultRodLength());
            mRecord.setRodLength(mJob.getDefaultRodLength());
        }

        if (UGMUtility.isValidDoubleString(mRecord.getLongitude())) {
            latTxv.setText(mRecord.getLatitude());
        } else {
            latTxv.setText("");
        }
        if (UGMUtility.isValidDoubleString(mRecord.getLongitude())) {
            longTxv.setText(mRecord.getLongitude());
        } else {
            longTxv.setText("");
        }
        logger.info("RecordCreateActivity:ResetUI:New Rod Number:" + mRecord + ", For Job Name:" + mJob.getJobName());
        //pitchTxv.setText("");
        //depthTxv.setText("");
        //tempTxv.setText("");
        //rollTxv.setText("");
    }

    private void fetchLastRecord(Record record) {
        hideProgressDialog();
        if (record != null) {
            old_rodNoTxv.setText(String.valueOf(record.getRodNumber()));
            if (UGMUtility.isValidDoubleString(record.getLatitude())) {
                old_latTxv.setText(record.getLatitude());
            } else {
                old_latTxv.setText(record.getLatitude());
            }
            if (UGMUtility.isValidDoubleString(record.getLongitude())) {
                old_longTxv.setText(record.getLongitude());
            } else {
                old_longTxv.setText(record.getLongitude());
            }

            old_pitTxv.setText(record.getPitch());
            if (!record.isOnlyPitch()) {
                old_depthTxv.setText(record.getDepth());
                old_tempTxv.setText(record.getRoll());
            }

            setNewRecord(false, record.getRodNumber() + 1);
        } else {
            //First Record case
            old_rodNoTxv.setText("");
            old_latTxv.setText("");
            old_longTxv.setText("");
            old_pitTxv.setText("");
            old_depthTxv.setText("");
            old_tempTxv.setText("");
            setNewRecord(true, 0);
        }
    }

    private void setNewRecord(boolean isFirstRecord, int rodNo) {
        mRecord.setRodNumber(rodNo);
        rodNoTxv.setText(String.valueOf(rodNo));

        if (isFirstRecord) {
            rodLengthTxv.setText("0");
            mRecord.setRodLength("0");
            mRecord.setOnlyPitch(true);
        } else {
            mRecord.setOnlyPitch(false);
            if (rodNo == 1) {
                rodLengthTxv.setText(mJob.getFirstRodLength());
                mRecord.setRodLength(mJob.getFirstRodLength());
            } else {
                rodLengthTxv.setText(mJob.getDefaultRodLength());
                mRecord.setRodLength(mJob.getDefaultRodLength());
            }
        }
        mRecord.setLatitude(latitude);
        mRecord.setLongitude(longitude);
        latTxv.setText(latitude);
        longTxv.setText(longitude);
        logger.info("RecordCreateActivity:setNewRecord():Rod Number:" + mRecord.getRodNumber() + ", For Job Name:" + mJob.getJobName());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.get_pitch_data) {
            isPitchDepth = false;
            mRecord.setOnlyPitch(true);
            mRecord.setDepth("");
            mRecord.setTemperature("");
            mRecord.setRoll("");
            createRecord();
        } else if (view.getId() == R.id.get_pitch_depth) {

            if (mRecord != null && mRecord.getRodNumber() == 0) {
                showMessage(getString(R.string.only_pitch_add), getString(R.string.ok), null);
                return;
            }

            isPitchDepth = true;
            // mRecord.setDepth("");
            mRecord.setOnlyPitch(false);
            mRecord.setTemperature("");
            mRecord.setRoll("");
            createRecord();

        } else if (view.getId() == R.id.get_pitch_depth_latlong) {

            if (mRecord != null && mRecord.getRodNumber() == 0) {
                showMessage(getString(R.string.only_pitch_add), getString(R.string.ok), null);
                return;
            }
            addRodAllData();
        }
    }


    private void createRecord() {
        try {
            List<Record> records = getLocalDB().getRecordDao().getRecordsByJobIdAndRodNoAndStatus(
                    mJob.getJobId(), mRecord.getRodNumber(), UGMMacros.RECORD_DELETED);
            if (null != records && 0 < records.size()) {
                showMessage(getString(R.string.rod_no_already_exist), getString(R.string.ok), null);
            } else {
                if (mRecord.getRodNumber() == 0 || mRecord.isOnlyPitch()) {
                    String relElev = ReportRecordModel.getRelativeElevation(mRecord.getPitch(), mRecord.getDepth(), mRecord.getRodLength(), mJob);
                    mRecord.setRelativeElevation(relElev);
                }
                if (isPitchDepth) {
                    String relElev = ReportRecordModel.getPitchAndDepth(mRecord.getPitch(), mRecord.getDepth(), mRecord.getRodLength(), mJob);
                    mRecord.setRelativeElevation(relElev);
                }
                getLocalDB().getRecordDao().insert(mRecord);
                logger.info("RecordCreateActivity:CreateRecord:For Job Name:" + mJob.getJobName() + ", And JobId:" + mRecord.getJobId());
                logger.info("RecordCreateActivity:CreateRecord:" + mRecord.toString());
                if (null != mJob && UGMMacros.JOB_CREATED.equals(mJob.getStatus())) {
                    mJob.setStatus(UGMMacros.JOB_INPROGRESS);
                    mJob.setSynced(false);
                    mJob.setUpdatedTime(Calendar.getInstance().getTimeInMillis());
                    getLocalDB().getJobDao().update(mJob);
                    if (UGMUtility.isNetworkAvailable() && UGMUtility.getBooleanPreference(UGMMacros.VALUE_ONLINE_SUPPORT, true)) {
                        postJob(mJob);
                    } else {
                        logger.info("RecordCreateActivity:Internet is not available to upload to server for JaobName:" + mJob.getJobName());
                    }
                }
                if (UGMUtility.isNetworkAvailable() && UGMUtility.getBooleanPreference(UGMMacros.VALUE_ONLINE_SUPPORT, true)) {
                    postRecords(mRecord);
                } else {
                    logger.info("RecordCreateActivity:Internet is not available to upload to server for RodNumber:" + mRecord.getRodNumber());
                }
                ResetUI();
                showMessage(getString(R.string.rod_added_success), getString(R.string.ok), null);
            }
        } catch (Exception ex) {
            logger.info("RecordCreateActivity:CreateRecord:Exception" + ex.toString());
            showMessage(getString(R.string.unexpected_error), getString(R.string.ok), null);
        }
    }


    @Override
    public void locationCallBack(Location location) {
         /*mRecord.setLatitude(String.valueOf(location.getLatitude()));
          mRecord.setLongitude(String.valueOf(location.getLongitude()));
        latitude = mRecord.getLatitude();
        longitude = mRecord.getLongitude();

        latTxv.setText(latitude);
        longTxv.setText(longitude);*/
    }

    @AfterPermissionGranted(REQUEST_CODE_PERMISSION_LOCATION)
    public void requestLocationPermission(String[] perms) {
        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, getString(R.string.location_premission_required),
                    REQUEST_CODE_PERMISSION_LOCATION, perms);
        } else {
            UGMLocationManager.getInstance().startLocation(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        logger.info("RecordCreateActivity:onPermissionsGranted:" + requestCode + ":" + perms.size());
        UGMLocationManager.getInstance().startLocation(this);
        if (!isGPSEnable()) {
            // showGpsDialog();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        logger.info("RecordCreateActivity:onPermissionsDenied:" + requestCode + ":" + perms.size());
        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    private void postJob(final Job job) {
        logger.info("RecordCreateActivity:postJob:Initiated for Job Name:" + job.getJobName());
        showProgressDialog();
        JsonArray jsonArray = new JsonArray();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", job.getJobId());
        jsonObject.addProperty("user_id", job.getUserId());
        jsonObject.addProperty("jobName", job.getJobName());
        jsonObject.addProperty("jobDescription", job.getJobDescription());
        jsonObject.addProperty("clientName", job.getClientName());
        jsonObject.addProperty("companyName", job.getCompanyName());
        jsonObject.addProperty("defaultRodLength", job.getDefaultRodLength());
        jsonObject.addProperty("firstRodLength", job.getFirstRodLength());
        jsonObject.addProperty("locationName", job.getLocationName());
        jsonObject.addProperty("status", job.getStatus());
        jsonObject.addProperty("createdDate", String.valueOf(job.getCreatedTime()));
        jsonObject.addProperty("updatedDate", String.valueOf(job.getUpdatedTime()));
        jsonObject.addProperty("prefDepth", job.getPrefDepth());
        jsonObject.addProperty("prefPitch", job.getPrefPitch());
        jsonArray.add(jsonObject);

        final String token = "Bearer " + UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_SESSION_TOKEN, "");
        SendJobData sendJobData = RetrofitClientInstance.getRetrofitInstance().create(SendJobData.class);
        Call<JobResponse> sendJobCall = sendJobData.sendJobData(token, jsonArray);
        logger.info("#######RecordCreateActivity:postJobs:Request JSON:" + new Gson().toJson(jsonArray));
        sendJobCall.enqueue(new Callback<JobResponse>() {
            @Override
            public void onResponse(Call<JobResponse> responseCall, Response<JobResponse> sendresponse) {
                JobResponse jobResponse = sendresponse.body();
                logger.info("#######RecordCreateActivity:postJobs:Response JSON:" + new Gson().toJson(jobResponse));
                if (jobResponse != null && jobResponse.getStatus().equalsIgnoreCase("success")) {
                    job.setSynced(true);
                    getLocalDB().getJobDao().update(job);
                    logger.info(job.getJobName() + " updated job uploaded to server successfully.");
                }
                hideProgressDialog();
            }

            @Override
            public void onFailure(Call<JobResponse> call, Throwable t) {
                logger.info("Updated Job uploaded to server unsuccessfully.");
                hideProgressDialog();
            }
        });
    }

    private void postRecords(final Record recentAddedRecord) {
        logger.info("RecordCreateActivity:postRecords:Initiated for Record Number:" + recentAddedRecord.getRodNumber());
        showProgressDialog();
        String token = "Bearer " + UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_SESSION_TOKEN, "");
        final List<Record> records = getLocalDB().getRecordDao().getRecordsBySyncedAndRodNumber(false, recentAddedRecord.getRodNumber());
        //records.add(recentAddedRecord);
        JsonArray jsonArray = getAllRecordsJsonArray(records);
        if (jsonArray != null && jsonArray.size() > 0) {
            SendRecordData sendRecordData = RetrofitClientInstance.getRetrofitInstance().create(SendRecordData.class);
            Call<JobResponse> sendRecordCall = sendRecordData.sendJobData(token, jsonArray);
            logger.info("#######RecordCreateActivity:postRecords:Request JSON:" + new Gson().toJson(jsonArray));
            sendRecordCall.enqueue(new Callback<JobResponse>() {
                @Override
                public void onResponse(Call<JobResponse> responseCall, Response<JobResponse> sendresponse) {
                    JobResponse jobResponse = sendresponse.body();
                    logger.info("#######RecordCreateActivity:postRecords:Response JSON:" + new Gson().toJson(jobResponse));
                    if (jobResponse != null && jobResponse.getStatus().equalsIgnoreCase("success")) {
                        for (Record record : records) {
                            record.setSynced(true);
                            getLocalDB().getRecordDao().update(record);
                            logger.info("RecordCreateActivity:postRecords to server successfully. RodNumber:" + record.getRodNumber());
                        }
                        hideProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<JobResponse> call, Throwable t) {
                    logger.info("RecordCreateActivity:postRecords to server Failed.");
                    hideProgressDialog();
                }
            });
        }
    }

    private JsonArray getAllRecordsJsonArray(List<Record> records) {
        JsonArray jsonArray = new JsonArray();
        for (Record record : records) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", record.getRecordId());
            jsonObject.addProperty("job_id", record.getJobId());
            jsonObject.addProperty("latitude", record.getLatitude());
            jsonObject.addProperty("longitude", record.getLongitude());
            jsonObject.addProperty("onlypitch", record.isOnlyPitch());
            jsonObject.addProperty("rodNumber", record.getRodNumber());
            jsonObject.addProperty("rodLength", record.getRodLength());
            jsonObject.addProperty("pitch", record.getPitch());
            jsonObject.addProperty("depth", record.getDepth());
            jsonObject.addProperty("roll", record.getRoll());
            jsonObject.addProperty("temperature", record.getTemperature());
            jsonObject.addProperty("status", record.getStatus());
            jsonObject.addProperty("createdDate", String.valueOf(record.getCreatedTime()));
            jsonObject.addProperty("updatedDate", String.valueOf(record.getUpdatedTime()));
            jsonObject.addProperty("relativeElevation", String.valueOf(record.getRelativeElevation()));
            jsonObject.addProperty("isEdited", record.isEdited());
            jsonArray.add(jsonObject);
        }
        logger.info("RecordCreateActivity:okhttp getAllRecordsJsonArray json array = " + jsonArray.toString());
        return jsonArray;
    }

    /*private void latLong() {
        if (!TextUtils.isEmpty(UGMUtility.getStringPreference(UGMMacros.LAT_LONG, ""))) {
            String latLong = UGMUtility.getStringPreference(UGMMacros.LAT_LONG, "");
            String[] split = latLong.split(",");
            mRecord.setLatitude(split[0]);
            mRecord.setLongitude(split[1]);
            latitude = mRecord.getLatitude();
            longitude = mRecord.getLongitude();

            latTxv.setText(latitude);
            longTxv.setText(longitude);
        }
    }*/
}
