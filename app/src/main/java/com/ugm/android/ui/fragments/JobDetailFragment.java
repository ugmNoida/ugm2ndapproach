package com.ugm.android.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ugm.android.R;
import com.ugm.android.database.entity.Job;
import com.ugm.android.ui.activities.RecordsOverviewActivity;
import com.ugm.android.utilities.RetrofitClientInstance;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;
import com.ugm.android.webservice.JobResponse;
import com.ugm.android.webservice.SendJobData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobDetailFragment extends Fragment implements View.OnClickListener {

    private static Logger logger = LoggerFactory.getLogger(JobDetailFragment.class);

    private RecordsOverviewActivity mActivity;
    private Button nextButton;
    private TextInputLayout jobNameLayout, prefDepthLayout, prefPitchLayout, locLayout, firstRodLayout, defaultRodLayout, companyLayout, clientLayout, descLayout;
    private TextInputEditText jobName, prefDepth, prefPitch, jobLocation, firstRodLen, defaultRodLen, companyName, clientName, description;
    private Job mJob;
    private String loggedInUserId;

    public JobDetailFragment() {
        // Required empty public constructor
    }

    public static JobDetailFragment newInstance() {
        JobDetailFragment fragment = new JobDetailFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (RecordsOverviewActivity) context;
        loggedInUserId = UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_USERID, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_job_detail, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mJob = mActivity.getJobItem();
        jobName = (TextInputEditText)view.findViewById(R.id.name);
        jobName.setText(mJob.getJobName());
        jobNameLayout = (TextInputLayout)view.findViewById(R.id.job_name);
        validateErrorCheck(jobNameLayout);
        prefDepth = (TextInputEditText)view.findViewById(R.id.prefDepth);
        //prefDepth.setText(mJob.getPrefDepth());

        if(UGMMacros.VALUE_DEPTH_METER.equalsIgnoreCase(mJob.getPrefDepth())) {
            prefDepth.setText(getString(R.string.meter));
        } else {
            prefDepth.setText(getString(R.string.feet));
        }

        prefDepthLayout = (TextInputLayout)view.findViewById(R.id.job_prefDepth);
        validateErrorCheck(prefDepthLayout);
        prefPitch = (TextInputEditText)view.findViewById(R.id.prefPitch);
        //prefPitch.setText(mJob.getPrefPitch());

        if(UGMMacros.VALUE_PITCH_PERCENTAGE.equalsIgnoreCase(mJob.getPrefPitch())) {
            prefPitch.setText(getString(R.string.percentage));
        } else {
            prefPitch.setText(getString(R.string.degree_text));
        }

        prefPitchLayout = (TextInputLayout)view.findViewById(R.id.job_prefPitch);
        validateErrorCheck(prefPitchLayout);
        jobLocation = (TextInputEditText)view.findViewById(R.id.location);
        jobLocation.setText(mJob.getLocationName());
        locLayout = (TextInputLayout)view.findViewById(R.id.job_location);
        validateErrorCheck(locLayout);
        firstRodLen = (TextInputEditText)view.findViewById(R.id.first_rod_length);
        firstRodLen.setText(mJob.getFirstRodLength());
        firstRodLayout = (TextInputLayout)view.findViewById(R.id.job_first_rod_length);
        if(UGMMacros.VALUE_DEPTH_METER.equalsIgnoreCase(mJob.getPrefDepth())){
            firstRodLayout.setHint(getString(R.string.first_rod_length)+" "+UGMUtility.getDefaultDepth(true));
        } else {
            firstRodLayout.setHint(getString(R.string.first_rod_length)+" "+UGMUtility.getDefaultDepth(false));
        }

        validateErrorCheck(firstRodLayout);
        defaultRodLen = (TextInputEditText)view.findViewById(R.id.default_rod_length);
        defaultRodLen.setText(mJob.getDefaultRodLength());
        defaultRodLayout = (TextInputLayout)view.findViewById(R.id.job_default_rod_length);
        if(UGMMacros.VALUE_DEPTH_METER.equalsIgnoreCase(mJob.getPrefDepth())) {
            defaultRodLayout.setHint(getString(R.string.default_rod_length)+" " + UGMUtility.getDefaultDepth(true));
        } else {
            defaultRodLayout.setHint(getString(R.string.default_rod_length)+" " + UGMUtility.getDefaultDepth(false));
        }
        validateErrorCheck(defaultRodLayout);
        companyName = (TextInputEditText)view.findViewById(R.id.company_name);
        companyName.setText(mJob.getCompanyName());
        companyLayout = (TextInputLayout)view.findViewById(R.id.job_company_name);
        validateErrorCheck(companyLayout);
        clientName = (TextInputEditText)view.findViewById(R.id.client_name);
        clientName.setText(mJob.getClientName());
        clientLayout = (TextInputLayout)view.findViewById(R.id.job_client_name);
        validateErrorCheck(clientLayout);
        description = (TextInputEditText)view.findViewById(R.id.description);
        description.setText(mJob.getJobDescription());
        descLayout = (TextInputLayout)view.findViewById(R.id.job_description);
        validateErrorCheck(descLayout);
        setEnableView(false);
        nextButton = (Button)view.findViewById(R.id.edit_btn);
        if(UGMMacros.JOB_COMPLETED.equals(mJob.getStatus())) {
            nextButton.setVisibility(View.GONE);
        } else {
            nextButton.setVisibility(View.VISIBLE);
            nextButton.setText(getString(R.string.edit_caps));
            nextButton.setOnClickListener(this);
        }
    }

    private void setEnableView(boolean isEnable) {
        jobName.setEnabled(false);
        jobLocation.setEnabled(isEnable);
        jobLocation.requestFocus();
        firstRodLen.setEnabled(isEnable);
        defaultRodLen.setEnabled(isEnable);
        companyName.setEnabled(isEnable);
        clientName.setEnabled(isEnable);
        description.setEnabled(isEnable);
    }

    private void validateErrorCheck(final TextInputLayout inputLayout) {
        inputLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() < 1) {
                    inputLayout.setErrorEnabled(true);
                    setInputLayoutError(inputLayout);
                }
                if (s.length() > 0) {
                    inputLayout.setError(null);
                    inputLayout.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private Job getViewJob() {
        return Job.createOrUpdateJob(loggedInUserId, jobName.getText().toString().trim(), jobLocation.getText().toString(),
                companyName.getText().toString(), clientName.getText().toString(),
                description.getText().toString(), firstRodLen.getText().toString(),
                defaultRodLen.getText().toString(), mJob);
    }

    private void setInputLayoutError(TextInputLayout inputLayout) {
        if(R.id.job_name == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_job_name));
        } else if(R.id.job_location == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_location));
        } else if(R.id.job_first_rod_length == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_first_rod_length));
        } else if(R.id.job_default_rod_length == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_default_rod_length));
        } else if(R.id.job_company_name == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_company));
        } else if(R.id.job_client_name == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_client));
        } else if(R.id.job_description == inputLayout.getId()) {
            inputLayout.setError(getString(R.string.please_enter_description));
        } else {
            inputLayout.setError(getString(R.string.please_enter_value));
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.edit_btn) {
            Button jobBtn = (Button) view;
            if (getString(R.string.edit_caps).equalsIgnoreCase(jobBtn.getText().toString())) {
                setEnableView(true);
                jobBtn.setText(getString(R.string.save_caps));
            } else if (getString(R.string.save_caps).equalsIgnoreCase(jobBtn.getText().toString())) {
                boolean isErrorExist = false;
                Job job = getViewJob();
                if (null == job.getJobName() || job.getJobName().isEmpty()) {
                    setInputLayoutError(jobNameLayout);
                    isErrorExist = true;
                } else if (null == job.getLocationName() || job.getLocationName().isEmpty()) {
                    setInputLayoutError(locLayout);
                    isErrorExist = true;
                } else if (null == job.getFirstRodLength() || job.getFirstRodLength().isEmpty()) {
                    setInputLayoutError(firstRodLayout);
                    isErrorExist = true;
                } else if (null == job.getDefaultRodLength() || job.getDefaultRodLength().isEmpty()) {
                    setInputLayoutError(defaultRodLayout);
                    isErrorExist = true;
                } else if (null == job.getCompanyName() || job.getCompanyName().isEmpty()) {
                    setInputLayoutError(companyLayout);
                    isErrorExist = true;
                } else if (null == job.getClientName() || job.getClientName().isEmpty()) {
                    setInputLayoutError(clientLayout);
                    isErrorExist = true;
                } else if (null == job.getJobDescription() || job.getJobDescription().isEmpty()) {
                    setInputLayoutError(descLayout);
                    isErrorExist = true;
                }
                if (!isErrorExist) {
                    jobBtn.setText(getString(R.string.edit_caps));
                    setEnableView(false);
                    updateJob(job);
                }
            }
        }
    }

    private void updateJob(Job job) {
        try {
            mActivity.getLocalDB().getJobDao().update(job);
            mActivity.showMessage(job.getJobName() + " "+getString(R.string.is_updated_successfully), getString(R.string.ok_caps), null);
            if(UGMUtility.isNetworkAvailable() && UGMUtility.getBooleanPreference(UGMMacros.VALUE_ONLINE_SUPPORT, true)) {
                postJob(job);
            }
        } catch(Exception ex){
            mActivity.showMessage(getString(R.string.unexpected_error), getString(R.string.ok), null);
        }
    }

    private void postJob(final Job job) {
        mActivity.showProgressDialog();
        JsonArray jsonArray = new JsonArray();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id",job.getJobId());
        jsonObject.addProperty("user_id", loggedInUserId);
        jsonObject.addProperty("jobName",job.getJobName());
        jsonObject.addProperty("jobDescription",job.getJobDescription());
        jsonObject.addProperty("clientName",job.getClientName());
        jsonObject.addProperty("companyName",job.getCompanyName());
        jsonObject.addProperty("defaultRodLength",job.getDefaultRodLength());
        jsonObject.addProperty("firstRodLength",job.getFirstRodLength());
        jsonObject.addProperty("locationName",job.getLocationName());
        jsonObject.addProperty("status",job.getStatus());
        jsonObject.addProperty("createdDate",String.valueOf(job.getCreatedTime()));
        jsonObject.addProperty("updatedDate",String.valueOf(job.getUpdatedTime()));
        jsonObject.addProperty("prefDepth",job.getPrefDepth());
        jsonObject.addProperty("prefPitch",job.getPrefPitch());
        jsonArray.add(jsonObject);

        final String token = "Bearer " + UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_SESSION_TOKEN, "");
        SendJobData sendJobData = RetrofitClientInstance.getRetrofitInstance().create(SendJobData.class);
        Call<JobResponse> sendJobCall = sendJobData.sendJobData(token, jsonArray);
        logger.info("#######JobDetailFragment:postJob:Request JSON:"+new Gson().toJson(jsonArray));
        sendJobCall.enqueue(new Callback<JobResponse>() {
            @Override
            public void onResponse(Call<JobResponse> responseCall, Response<JobResponse> sendresponse) {
                JobResponse jobResponse = sendresponse.body();
                logger.info("#######JobDetailFragment:postJob:Response JSON:"+new Gson().toJson(jobResponse));
                if (jobResponse != null && jobResponse.getStatus().equalsIgnoreCase("success")) {
                    job.setSynced(true);
                    mActivity.getLocalDB().getJobDao().update(job);
                    logger.info(job.getJobName() + " updated job uploaded to server successfully.");
                }
                mActivity.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<JobResponse> call, Throwable t) {
                logger.info("Updated Job uploaded to server unsuccessfully.");
                mActivity.hideProgressDialog();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
