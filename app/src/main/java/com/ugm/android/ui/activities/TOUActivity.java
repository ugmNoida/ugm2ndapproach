package com.ugm.android.ui.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;

import com.ugm.android.R;

public class TOUActivity extends AbstractBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tou);
        initActionBar(getString(R.string.title_activity_tou));
        WebView termsWebView = (WebView)findViewById(R.id.terms_webview);
        termsWebView.loadUrl("file:///android_asset/tou.html");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }
}
