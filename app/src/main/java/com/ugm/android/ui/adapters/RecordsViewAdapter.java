package com.ugm.android.ui.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ugm.android.R;
import com.ugm.android.UGMApplication;
import com.ugm.android.database.entity.Record;
import com.ugm.android.ui.activities.RecordsOverviewActivity;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;

import java.util.ArrayList;

public class RecordsViewAdapter extends RecyclerView.Adapter<RecordsViewAdapter.ViewHolder> {

    private ArrayList<Record> records;
    private final OnListItemtInteractionListener mListener;
    private RecordsOverviewActivity mActivity;

    public interface OnListItemtInteractionListener {
        void onDeleteRecord(Record record);
        void onEditRecord(Record record);
    }

    public RecordsViewAdapter(ArrayList<Record> items, OnListItemtInteractionListener listener, RecordsOverviewActivity activity) {
        records = items;
        mListener = listener;
        mActivity = activity;
    }

    public void refreshListData(ArrayList<Record> items){
        records = items;
        //notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_recodsview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if(position <= records.size()){
            holder.record = records.get(position);
            holder.mRodNoTxv.setText(String.valueOf(holder.record.getRodNumber()));
            holder.mLatitudeTxv.setText(holder.record.getLatitude());
            holder.mLongitudeTxv.setText(holder.record.getLongitude());
            holder.mPitchTxv.setText(holder.record.getPitch());
            /*if(!holder.record.isOnlyPitch()){
                holder.mDepthView.setText(holder.record.getDepth());
                holder.mTempTxv.setText(holder.record.getTemperature());
                holder.mRollTxv.setText(holder.record.getRoll());
            } else {
                holder.mDepthView.setText("");
                holder.mTempTxv.setText("");
                holder.mRollTxv.setText("");
            }*/
            if(!TextUtils.isEmpty(holder.record.getDepth()))
                holder.mDepthView.setText(holder.record.getDepth());
            else
                holder.mDepthView.setText("");

            if(!TextUtils.isEmpty(holder.record.getTemperature()))
                holder.mTempTxv.setText(holder.record.getTemperature());
            else
                holder.mTempTxv.setText("");

            if(!TextUtils.isEmpty(holder.record.getRoll()))
                holder.mRollTxv.setText(holder.record.getRoll());
            else
                holder.mRollTxv.setText("");

            if(holder.record.isEdited()) {
                holder.mPitchTxv.setTextColor(Color.parseColor("#0000FF"));
                //if(!holder.record.isOnlyPitch()) {
                    holder.mDepthView.setTextColor(Color.parseColor("#0000FF"));
                    holder.mRollTxv.setTextColor(Color.parseColor("#0000FF"));
                    holder.mTempTxv.setTextColor(Color.parseColor("#0000FF"));
                //}
            }
            if(!UGMMacros.JOB_COMPLETED.equalsIgnoreCase(mActivity.getJobItem().getStatus())) {
                if (position == 0) {
                    holder.deleteRecord.setVisibility(View.VISIBLE);
                    holder.deleteRecord.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (null != mListener) {
                                mListener.onDeleteRecord(holder.record);
                            }
                        }
                    });
                } else {
                    holder.deleteRecord.setVisibility(View.INVISIBLE);
                    holder.deleteRecord.setOnClickListener(null);
                }
                holder.editRecord.setVisibility(View.VISIBLE);
                holder.editRecord.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (null != mListener) {
                            mListener.onEditRecord(holder.record);
                        }
                    }
                });
            } else {
                holder.editRecord.setVisibility(View.GONE);
                holder.editRecord.setOnClickListener(null);

                holder.deleteRecord.setVisibility(View.GONE);
                holder.deleteRecord.setOnClickListener(null);
            }
        }
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mRodNoTxv,mLatitudeTxv,mLongitudeTxv,mPitchTxv,mDepthView,mTempTxv,mRollTxv;
        public final TextView pitchHint, depthHint, tempHint;
        public final ImageView deleteRecord, editRecord;
        public Record record;

        public ViewHolder(View view) {
            super(view);
            mRodNoTxv = (TextView) view.findViewById(R.id.rod_no_value_txv_new);
            mLatitudeTxv = (TextView) view.findViewById(R.id.latitude_val_txv);
            mLongitudeTxv = (TextView) view.findViewById(R.id.longitude_val_txv);
            mPitchTxv = (TextView) view.findViewById(R.id.pitch_val_txv);
            mDepthView = (TextView) view.findViewById(R.id.depth_val_txv);
            mTempTxv = (TextView) view.findViewById(R.id.temp_val_txv);
            mRollTxv = (TextView)view.findViewById(R.id.roll_val_txv);
            deleteRecord = (ImageView) view.findViewById(R.id.cancel_icon_new);
            editRecord = (ImageView) view.findViewById(R.id.img_edit_icon_new);
            pitchHint = (TextView)view.findViewById(R.id.pitch_txv);
            if(UGMMacros.VALUE_PITCH_PERCENTAGE.equalsIgnoreCase(mActivity.getJobItem().getPrefPitch())) {
                pitchHint.setText(UGMApplication.getInstance().getString(R.string.text_pitch) + UGMUtility.getDefaultPitch(true));
            } else {
                pitchHint.setText(UGMApplication.getInstance().getString(R.string.text_pitch) + UGMUtility.getDefaultPitch(false));
            }

            depthHint = (TextView)view.findViewById(R.id.depth_txv);
            tempHint = (TextView)view.findViewById(R.id.temp_txv);
            if(UGMMacros.VALUE_DEPTH_METER.equalsIgnoreCase(mActivity.getJobItem().getPrefDepth())) {
                depthHint.setText(UGMApplication.getInstance().getString(R.string.text_depth)+UGMUtility.getDefaultDepth(true));
                tempHint.setText(UGMApplication.getInstance().getString(R.string.text_temperature)+"(C)");
            } else {
                depthHint.setText(UGMApplication.getInstance().getString(R.string.text_depth)+UGMUtility.getDefaultDepth(false));
                tempHint.setText(UGMApplication.getInstance().getString(R.string.text_temperature)+"(F)");
            }

            if(UGMMacros.JOB_COMPLETED.equalsIgnoreCase(mActivity.getJobItem().getStatus())) {
                deleteRecord.setVisibility(View.INVISIBLE);
                editRecord.setVisibility(View.INVISIBLE);
            } else {
                deleteRecord.setVisibility(View.VISIBLE);
                editRecord.setVisibility(View.VISIBLE);
            }
        }
    }
}
