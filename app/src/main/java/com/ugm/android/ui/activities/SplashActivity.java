package com.ugm.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.ugm.android.R;

public class SplashActivity extends AbstractBaseActivity {

    private Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        moveToLoginActivity();
    }

    private void moveToLoginActivity() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent loginIntent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                finish();
            }
        }, 3000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
}
