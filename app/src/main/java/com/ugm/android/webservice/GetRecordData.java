package com.ugm.android.webservice;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface GetRecordData {

    @GET("jobdetails/{job_id}")
    Call<List<RecordData>> getRecordData(@Header("Authorization") String authHeader,@Path("job_id") String job_id);
}
