package com.ugm.android.webservice;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface GetJobWithJobDetails {

    @GET("/jobs/jobswithdetails")
    Call<List<JobData>> getJobWithRecords(@Header("Authorization") String authHeader);
}
