package com.ugm.android.webservice;

import com.google.gson.annotations.SerializedName;

public class SettingsData {

    @SerializedName("user_id")
    private String userID;
    @SerializedName("requiredLocation")
    private boolean requiredlocation;
    @SerializedName("whichLocation")
    private String whichlocation;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public boolean isRequiredlocation() {
        return requiredlocation;
    }

    public void setRequiredlocation(boolean requiredlocation) {
        this.requiredlocation = requiredlocation;
    }

    public String getWhichlocation() {
        return whichlocation;
    }

    public void setWhichlocation(String whichlocation) {
        this.whichlocation = whichlocation;
    }
}
