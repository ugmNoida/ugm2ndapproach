package com.ugm.android.webservice;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface GetJobData {


    @GET("jobs")
    Call<List<JobData>> getJobData(@Header("Authorization") String authHeader);
}
