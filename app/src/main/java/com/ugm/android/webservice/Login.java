package com.ugm.android.webservice;

import com.google.gson.annotations.SerializedName;

public class Login {

    @SerializedName("success")
    private String success;
    @SerializedName("id")
    private String id;
    @SerializedName("token")
    private String token;
    @SerializedName("deviceSerialNumber")
    private String deviceSrlNumber;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("email")
    private String email;
    @SerializedName("companyName")
    private String companyName;
    @SerializedName("contactNumber")
    private String contactNumber;
    @SerializedName("countryName")
    private String countryName;


    public void Login(String userName,String password){
        this.success = userName;
        this.id = password;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDeviceSrlNumber() {
        return deviceSrlNumber;
    }

    public void setDeviceSrlNumber(String deviceSrlNumber) {
        this.deviceSrlNumber = deviceSrlNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
