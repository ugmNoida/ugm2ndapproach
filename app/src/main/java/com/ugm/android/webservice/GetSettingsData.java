package com.ugm.android.webservice;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface GetSettingsData {

    @GET("users/preferences")
    Call<SettingsData> getSettingsData(@Header("Authorization") String authHeader);
}
