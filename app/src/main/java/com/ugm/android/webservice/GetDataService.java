package com.ugm.android.webservice;

import com.ugm.android.database.entity.Record;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {

    @GET("/photos")
    Call<List<Record>> getAllRecords();

}
