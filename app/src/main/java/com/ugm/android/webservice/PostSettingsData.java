package com.ugm.android.webservice;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface PostSettingsData {

    @POST("users/preferences")
    Call<JobResponse> sendSettingsData(@Header("Authorization") String authHeader, @Body JsonObject jsonObj);
}
