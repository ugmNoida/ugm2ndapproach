package com.ugm.android.webservice;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface GetRegisterData {

    @POST("/register")
    Call<RegisterData> getRegisterData(@Body JsonObject jsonObject);
}
