package com.ugm.android.webservice;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.DELETE;

public interface DeleteJobData {

    @DELETE("jobs/{job_id}")
    Call<JobResponse> deleteJobData(@Header("Authorization") String authHeader, @Path("job_id") String jobId);
}
