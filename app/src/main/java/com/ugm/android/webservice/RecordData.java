package com.ugm.android.webservice;

import com.google.gson.annotations.SerializedName;

public class RecordData {

    @SerializedName("_id")
    private String id;
    @SerializedName("job_id")
    private String jobID;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("onlypitch")
    private boolean onlypitch;
    @SerializedName("pitch")
    private String pitch;
    @SerializedName("depth")
    private String depth;
    @SerializedName("rodNumber")
    private String rodNumber;
    @SerializedName("rodLength")
    private String rodLength;
    @SerializedName("roll")
    private String roll;
    @SerializedName("temperature")
    private String temperature;
    @SerializedName("status")
    private String status;
    @SerializedName("createdDate")
    private String createdDate;
    @SerializedName("updatedDate")
    private String updatedDate;
    @SerializedName("relativeElevation")
    private String relativeElevation;
    @SerializedName("isEdited")
    private boolean isEdited;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public boolean getOnlypitch() {
        return onlypitch;
    }

    public void setOnlypitch(boolean onlypitch) {
        this.onlypitch = onlypitch;
    }

    public String getPitch() {
        return pitch;
    }

    public void setPitch(String pitch) {
        this.pitch = pitch;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getRodNumber() {
        return rodNumber;
    }

    public void setRodNumber(String rodNumber) {
        this.rodNumber = rodNumber;
    }

    public String getRodLength() {
        return rodLength;
    }

    public void setRodLength(String rodLength) {
        this.rodLength = rodLength;
    }

    public String getRoll() {
        return roll;
    }

    public void setRoll(String roll) {
        this.roll = roll;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getRelativeElevation() {
        return relativeElevation;
    }

    public void setRelativeElevation(String relativeElevation) {
        this.relativeElevation = relativeElevation;
    }

    public boolean getIsEdited() {
        return isEdited;
    }

    public void setIsEdited(boolean isEdited) {
        this.isEdited = isEdited;
    }
}
