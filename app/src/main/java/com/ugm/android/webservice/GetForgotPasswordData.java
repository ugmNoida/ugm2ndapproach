package com.ugm.android.webservice;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface GetForgotPasswordData {

    @POST("users/forgotpassword")
    Call<ForgotPassword> getForgotPasswordData(@Body JsonObject jsonObject);
}
