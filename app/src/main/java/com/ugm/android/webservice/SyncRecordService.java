package com.ugm.android.webservice;

import com.ugm.android.database.entity.Record;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SyncRecordService {

    @POST("/records")
    Call<Record> syncRecord(@Body Record record);

}
