package com.ugm.android.webservice;

import com.google.gson.JsonArray;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface SendRecordData {

    @POST("jobdetails")
    Call<JobResponse> sendJobData(@Header("Authorization") String authHeader, @Body JsonArray jsonArray);

    @HTTP(method = "DELETE", path = "/jobdetails", hasBody = true)
    Call<JobResponse> deleteJobData(@Header("Authorization") String authHeader, @Body JsonArray jsonArray);

}
