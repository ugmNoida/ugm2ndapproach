package com.ugm.android.webservice;

import com.google.gson.JsonArray;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface SendJobData {

    @POST("jobs")
    Call<JobResponse> sendJobData(@Header("Authorization") String authHeader,@Body JsonArray jsonArray);

}
