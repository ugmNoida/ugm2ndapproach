package com.ugm.android.webservice;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface GetLoginData {

    @POST("users/login")
    Call<Login> getLoginData(@Body JsonObject jsonObject);
}
