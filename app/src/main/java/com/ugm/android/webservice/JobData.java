package com.ugm.android.webservice;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JobData {

    @SerializedName("user_id")
    private String userID;
    @SerializedName("id")
    private String id;
    @SerializedName("jobName")
    private String jobName;
    @SerializedName("jobDescription")
    private String jobDescription;
    @SerializedName("clientName")
    private String clientName;
    @SerializedName("companyName")
    private String companyName;
    @SerializedName("defaultRodLength")
    private String defaultRodLength;
    @SerializedName("firstRodLength")
    private String firstRodLength;
    @SerializedName("locationName")
    private String locationName;
    @SerializedName("status")
    private String status;
    @SerializedName("createdDate")
    private String createdDate;
    @SerializedName("updatedDate")
    private String updatedDate;
    @SerializedName("prefDepth")
    private String prefDepth;
    @SerializedName("prefPitch")
    private String prefPitch;

    @SerializedName("jobs_details")
    private List<RecordData> records;


    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDefaultRodLength() {
        return defaultRodLength;
    }

    public void setDefaultRodLength(String defaultRodLength) {
        this.defaultRodLength = defaultRodLength;
    }

    public String getFirstRodLength() {
        return firstRodLength;
    }

    public void setFirstRodLength(String firstRodLength) {
        this.firstRodLength = firstRodLength;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getPrefDepth() {
        return prefDepth;
    }

    public void setPrefDepth(String prefDepth) {
        this.prefDepth = prefDepth;
    }

    public String getPrefPitch() {
        return prefPitch;
    }

    public void setPrefPitch(String prefPitch) {
        this.prefPitch = prefPitch;
    }

    public List<RecordData> getRecords() {
        return records;
    }

    public void setRecords(List<RecordData> records) {
        this.records = records;
    }
}
