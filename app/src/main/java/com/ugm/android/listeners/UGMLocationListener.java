package com.ugm.android.listeners;

import android.location.Location;


/**
 * Created by smisra on 7/30/18.
 */

public interface UGMLocationListener {
    void locationCallBack(Location location);
}
