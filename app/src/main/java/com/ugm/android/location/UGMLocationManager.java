package com.ugm.android.location;

import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.ugm.android.UGMApplication;
import com.ugm.android.listeners.UGMLocationListener;
import com.ugm.android.utilities.UGMUtility;

public class UGMLocationManager {

    private static final String TAG = "UGMLocationManager";

    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private LocationSettingsRequest locationSettingsRequest;
    private Task<LocationSettingsResponse> locationSettingRespTask;

    private static UGMLocationManager locationManager;
    public boolean locationStarted = false;
    private UGMLocationListener locationListener;
    private Location bestLocation;

    public static UGMLocationManager getInstance() {
        if (null == locationManager) {
            synchronized (UGMLocationManager.class) {
                if (null == locationManager) {
                    locationManager = new UGMLocationManager();
                }
            }
        }
        return locationManager;
    }

    private UGMApplication getUGMApplication() {
        return UGMApplication.getInstance();
    }

    private UGMLocationManager() {
        initLocationSettings();
    }

    private void initLocationSettings() {
        if (null == fusedLocationClient) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(getUGMApplication());
        }
        locationRequest = getLocationRequest();
        locationSettingsRequest = buildLocationSettingsRequest(locationRequest);

        SettingsClient settingsClient = LocationServices.getSettingsClient(getUGMApplication());
        locationSettingRespTask = settingsClient.checkLocationSettings(locationSettingsRequest);
    }

    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(0);
        locationRequest.setFastestInterval(0);
        locationRequest.setSmallestDisplacement(0);//Approx. 1.6 feet
        return locationRequest;
    }

    private LocationSettingsRequest buildLocationSettingsRequest(LocationRequest locationRequest) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        return builder.build();
    }

    private LocationCallback locationCallback() {
        if (null == locationCallback) {
            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    if (null != locationListener) {
                        Location currentLocation = locationResult.getLastLocation();
                        if (isBestLocation(currentLocation)) {
                            bestLocation = currentLocation;
                            if (UGMUtility.isGoogleMapEnabled())
                                locationListener.locationCallBack(currentLocation);
                        }
                    }
                }
            };
        }
        return locationCallback;
    }

    private boolean isBestLocation(Location location) {
        if (null == bestLocation) {
            return true;
        }
        return bestLocation.distanceTo(location) < 3 ? true : false;
    }

    private void requestLocationUpdates() {
        try {
            if (fusedLocationClient == null) {
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(getUGMApplication());
            }
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback(), null);
            locationStarted = true;
        } catch (SecurityException e) {
            Log.e(TAG, "Exception while requestLocationUpdates:" + e.toString());
        }
    }

    private void addLocationSettingListener(Task<LocationSettingsResponse> locationSettingRespTask) {
        locationSettingRespTask.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                Log.i(TAG, "LocationSettingsResponse ONSUCCESS");
                requestLocationUpdates();
            }
        });

        locationSettingRespTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "Device location setting fails to satisfy." + e.toString());
            }
        });
    }

    public boolean isLocationStarted() {
        return locationStarted;
    }

    //TODO: Check the play service is available or not
    //TODO: Check the Location permission is enable ot not
    //TODO: Check GPS is enabled or not
    public synchronized void startLocation(UGMLocationListener locationListener) {
        initLocationSettings();
        this.locationListener = locationListener;
        try {
            fusedLocationClient.removeLocationUpdates(locationCallback());
            if (!locationSettingRespTask.isSuccessful()) {
                requestLocationUpdates();
            } else {
                addLocationSettingListener(locationSettingRespTask);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception occurred while startLocationUpdate:" + e.toString());
        }
    }

    public synchronized void stopLocation() {
        try {
            locationStarted = false;
            if (null != fusedLocationClient) {
                Task<Void> task = fusedLocationClient.removeLocationUpdates(locationCallback());
                task.addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.i(TAG, "Location updates stopped Successfully...");
                    }
                });
                task.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Location updates stopped Failed...");
                    }
                });
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception occurred while stopLocationUpdate:" + e.toString());
        }
        dispose();
    }

    public void dispose() {
        bestLocation = null;
        locationStarted = false;
        locationManager = null;
        locationListener = null;
    }
}
