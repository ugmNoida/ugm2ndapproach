package com.ugm.android.datamodel;

import com.ugm.android.database.entity.Job;

import java.util.Comparator;

public class JobListComparator implements Comparator<Job> {

    @Override
    public int compare(Job job1, Job job2) {
        long job2CreatedTime = job2.getCreatedTime();
        if(job1.getCreatedTime() == job2.getCreatedTime()) {
            return 0;
        } else if(job1.getCreatedTime() > job2.getCreatedTime()) {
            return 1;
        }
        return -1;
    }
}
