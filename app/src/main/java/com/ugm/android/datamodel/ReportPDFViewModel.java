package com.ugm.android.datamodel;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.ugm.android.R;
import com.ugm.android.UGMApplication;
import com.ugm.android.database.entity.Job;
import com.ugm.android.database.entity.Record;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class ReportPDFViewModel {

    private static final String TAG = "ReportPDFViewModel";

    public static File captureChartScreen(Activity context, View rootView, String jobId) {
        try {
            View view = context.getWindow().getDecorView();
            view.setDrawingCacheEnabled(true);
            view.buildDrawingCache();
            Bitmap b1 = view.getDrawingCache();
            Rect frame = new Rect();
            context.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
            int statusBarHeight = frame.top;

            //Find the screen dimensions to create bitmap in the same size.
            int width = context.getWindowManager().getDefaultDisplay().getWidth();
            int height = context.getWindowManager().getDefaultDisplay().getHeight();

            Bitmap capturedBitmap = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height - statusBarHeight);
            view.destroyDrawingCache();

            File rootFolder = context.getExternalFilesDir(Environment.MEDIA_SHARED);
            File capturedFolder = new File(rootFolder, "UGMReport");
            if (!capturedFolder.exists()) {
                capturedFolder.mkdirs();
            }
            File capturedFile = new File(capturedFolder, jobId+"_Chart.jpg");
            if(capturedFile.exists()) {
                capturedFile.delete();
            }
            capturedFile.createNewFile();

            FileOutputStream outputStream = new FileOutputStream(capturedFile);
            int quality = 100;
            capturedBitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();
            return capturedFile;
        } catch (Exception ex) {
            Log.e(TAG, "Exception while getScreenshotFileFromView UGM Report file.", ex);
        }
        return null;
    }

    public static File getPDFFile(Context context, String reportName) {
        try {
            File rootFolder = context.getExternalFilesDir(Environment.MEDIA_SHARED);
            File pdfFolder = new File(rootFolder, "UGMReport");
            if (!pdfFolder.exists()) {
                pdfFolder.mkdirs();
            }
            File pdfFile = new File(rootFolder, reportName);
            pdfFile.deleteOnExit();
            pdfFile.createNewFile();
            return pdfFile;
        } catch (Exception ex) {
            Log.e(TAG, "Exception while creating UGM Report file.", ex);
        }
        return null;
    }

    public static Document openDocument(File file) {
        try {
            Document document = new Document(PageSize.A4, 30, 30, 30, 30);
            PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();
            document.addCreationDate();
            document.addAuthor(UGMUtility.getStringPreference(UGMMacros.PREFS_LOGGEDIN_EMAIL, ""));
            document.addCreator("Under Ground Magnetic");
            document.addTitle(UGMApplication.getInstance().getString(R.string.job_data_report));
            return document;
        } catch(Exception ex){
            Log.e(TAG, "Exception while opening UGM Report Document.", ex);
        }
        return null;
    }

    private static PdfPCell getPdfHeaderValueCell(String headerText, String valueText, Font headerFont, Font valueFont) {
        PdfPCell pdfWordCell = new PdfPCell();
        Phrase jobNameH = new Phrase(headerText, headerFont );
        Phrase jobNameV = new Phrase(valueText, valueFont );
        pdfWordCell.addElement(jobNameH );
        pdfWordCell.addElement(jobNameV );
        return pdfWordCell;
    }

    private static PdfPCell getPdfHeaderCell(String headerText, Font headerFont) {
        PdfPCell pdfWordCell = new PdfPCell();
        Phrase jobNameH = new Phrase(headerText, headerFont );
        pdfWordCell.addElement(jobNameH );
        return pdfWordCell;
    }

    public static Document writeJobContentDocument(Document document, Job job, int recordCnt) {
        try {
            Font titleFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
            Font headerFont = new Font(Font.FontFamily.HELVETICA, 16);
            Font valueFont = new Font(Font.FontFamily.HELVETICA, 14);

            Paragraph rootPara = new Paragraph();
            Paragraph titlePara = new Paragraph();
            titlePara.setAlignment(Paragraph.ALIGN_CENTER);
            Chunk titleText = new Chunk(UGMApplication.getInstance().getString(R.string.job_data_view_report), titleFont);
            titlePara.add(titleText);
            rootPara.add(titlePara);
            addEmptyLine(rootPara, 1);
            PdfPTable table1 = new PdfPTable(2);
            table1.setWidthPercentage(100);
            table1.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.job_data_name)+":", job.getJobName(), headerFont, valueFont));
            table1.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.location)+":", job.getLocationName(), headerFont, valueFont));
            table1.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.created_date)+":", UGMUtility.covertLongToDFormattedDate(job.getCreatedTime(), UGMMacros.FORMATTER1), headerFont, valueFont));
            table1.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.updated_date)+":", UGMUtility.covertLongToDFormattedDate(job.getUpdatedTime(), UGMMacros.FORMATTER1), headerFont, valueFont));
            table1.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.first_rod_length)+":", job.getFirstRodLength(), headerFont, valueFont));
            table1.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.default_rod_length)+":", job.getDefaultRodLength(), headerFont, valueFont));
            table1.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.company_name)+":", job.getCompanyName(), headerFont, valueFont));
            table1.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.client_name)+":", job.getClientName(), headerFont, valueFont));
            PdfPTable table2 = new PdfPTable(3);
            table2.setWidthPercentage(100);
            table2.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.data_points)+":", String.valueOf(recordCnt), headerFont, valueFont));
            table2.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.text_depth)+":", job.getPrefDepth(), headerFont, valueFont));
            table2.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.text_pitch)+":", job.getPrefPitch(), headerFont, valueFont));
            PdfPTable table3 = new PdfPTable(1);
            table3.setWidthPercentage(100);
            //table3.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.device_serial_no)+":", UGMUtility.getStringPreference(UGMMacros.PREFS_FULL_NAME, ""), headerFont, valueFont));
            table3.addCell(getPdfHeaderValueCell(UGMApplication.getInstance().getString(R.string.description)+":", job.getJobDescription(), headerFont, valueFont));
            rootPara.add(table1);
            rootPara.add(table2);
            rootPara.add(table3);
            document.add(rootPara);
        } catch(Exception ex){
            Log.e(TAG, "Exception while writing UGM Report Document.", ex);
        }
        return document;
    }

    public static Document writeRecordContentDocument(Document document, ArrayList<Record> records, Job job) {
        if(null != records && 0 < records.size()) {
            try {
                Font titleFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
                Font headerFont = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);

                Paragraph rootPara = new Paragraph();
                addEmptyLine(rootPara, 1);
                Paragraph titlePara = new Paragraph();
                titlePara.setAlignment(Paragraph.ALIGN_CENTER);
                Chunk titleText = new Chunk(UGMApplication.getInstance().getString(R.string.rodwise_report), titleFont);
                titlePara.add(titleText);
                rootPara.add(titlePara);
                addEmptyLine(rootPara, 1);
                PdfPTable table;
                if(UGMUtility.isGoogleMapEnabled()) {
                    table = new PdfPTable(11);
                } else {
                    table = new PdfPTable(9);
                }
                table.setWidthPercentage(100);
                ArrayList<ReportRecordModel> reportRecordModel = ReportRecordModel.getRodwiseDataModel(records, true, job);
                for (ReportRecordModel recordModel : reportRecordModel) {
                    if (recordModel.isHeader()) {
                        table.addCell(getPdfHeaderCell(recordModel.getRodNumber(), headerFont));
                        if(UGMUtility.isGoogleMapEnabled()) {
                            table.addCell(getPdfHeaderCell(recordModel.getLatitude(), headerFont));
                            table.addCell(getPdfHeaderCell(recordModel.getLongitude(), headerFont));
                        }
                        table.addCell(getPdfHeaderCell(recordModel.getRodLength(), headerFont));
                        table.addCell(getPdfHeaderCell(recordModel.getBoreLength(), headerFont));
                        table.addCell(getPdfHeaderCell(recordModel.getxDistance(), headerFont));
                        table.addCell(getPdfHeaderCell(recordModel.getAvgPitch(), headerFont));
                        table.addCell(getPdfHeaderCell(recordModel.getRelativeDepth(), headerFont));
                        table.addCell(getPdfHeaderCell(recordModel.getDepthInPdf(), headerFont));
                        table.addCell(getPdfHeaderCell(recordModel.getRelativeElevation(), headerFont));
                        table.addCell(getPdfHeaderCell(recordModel.getRecordCreatedDTTM(), headerFont));
                    } else {
                        table.addCell(recordModel.getRodNumber());
                        if(UGMUtility.isGoogleMapEnabled()) {
                            table.addCell(recordModel.getLatitude());
                            table.addCell(recordModel.getLongitude());
                        }
                        table.addCell(recordModel.getRodLength());
                        table.addCell(recordModel.getBoreLength());

                        if(UGMUtility.isValidFloatString(recordModel.getPitch())) {
                            table.addCell(recordModel.getxDistance());
                            table.addCell(recordModel.getAvgPitch());
                            table.addCell(recordModel.getRelativeDepth());
                            table.addCell(recordModel.getDepthInPdf());
                            table.addCell(recordModel.getRelativeElevation());
                        } else {
                            table.addCell("");
                            table.addCell("");
                            table.addCell("");
                            table.addCell("");
                            table.addCell("");
                        }
                        table.addCell(recordModel.getRecordCreatedDTTM());
                    }
                }
                rootPara.add(table);
                document.add(rootPara);
            } catch (Exception ex) {
                Log.e(TAG, "Exception while writing UGM Report Document.", ex);
            }
        }
        return document;
    }

    public static Document addChartScreen(Activity context, Document document, Job job) {
        try {
            File rootFolder = context.getExternalFilesDir(Environment.MEDIA_SHARED);
            File capturedFolder = new File(rootFolder, "UGMReport");
            if (capturedFolder.exists()) {
                File capturedFile = new File(capturedFolder, job.getJobId() + "_Chart.jpg");
                if (capturedFile.exists()) {
                    Paragraph rootPara = new Paragraph();
                    addEmptyLine(rootPara, 1);
                    Paragraph titlePara = new Paragraph();
                    titlePara.setAlignment(Paragraph.ALIGN_CENTER);
                    Font titleFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
                    Chunk titleText = new Chunk(UGMApplication.getInstance().getString(R.string.chart_view_report), titleFont);
                    titlePara.add(titleText);
                    rootPara.add(titlePara);
                    addEmptyLine(rootPara, 1);
                    Image chartImage = Image.getInstance(capturedFile.getAbsolutePath());
                    chartImage.scaleToFit(520, 520);
                    chartImage.setScaleToFitLineWhenOverflow(true);
                    rootPara.add(chartImage);
                    document.add(rootPara);
                }
            }
        } catch(Exception ex){
            Log.e(TAG, "Exception while addChartScreen UGM Report Document.", ex);
        }
        return document;
    }

    public static Document addMapScreen(Activity context, Document document, Job job) {
        try {
            File rootFolder = context.getExternalFilesDir(Environment.MEDIA_SHARED);
            File capturedFolder = new File(rootFolder, "UGMReport");
            if (capturedFolder.exists()) {
                File capturedFile = new File(capturedFolder, job.getJobId() + "_Map.jpg");
                if (capturedFile.exists()) {
                    Paragraph rootPara = new Paragraph();
                    addEmptyLine(rootPara, 1);
                    Paragraph titlePara = new Paragraph();
                    titlePara.setAlignment(Paragraph.ALIGN_CENTER);
                    Font titleFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
                    Chunk titleText = new Chunk(UGMApplication.getInstance().getString(R.string.map_view_report), titleFont);
                    titlePara.add(titleText);
                    rootPara.add(titlePara);
                    addEmptyLine(rootPara, 1);
                    Image chartImage = Image.getInstance(capturedFile.getAbsolutePath());
                    chartImage.scaleToFit(520, 520);
                    chartImage.setScaleToFitLineWhenOverflow(true);
                    rootPara.add(chartImage);
                    document.add(rootPara);
                }
            }
        } catch(Exception ex){
            Log.e(TAG, "Exception while addChartScreen UGM Report Document.", ex);
        }
        return document;
    }

    public static void closeDocument(Document document) {
        if(null != document) {
            document.close();
        }
        document = null;
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
