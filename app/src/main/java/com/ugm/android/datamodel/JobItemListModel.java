package com.ugm.android.datamodel;

import com.ugm.android.R;
import com.ugm.android.UGMApplication;
import com.ugm.android.database.entity.Job;
import com.ugm.android.utilities.UGMMacros;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class JobItemListModel {

    public static final String GROUP_RECENT = UGMApplication.getInstance().getString(R.string.recent);
    public static final String GROUP_OLDER = UGMApplication.getInstance().getString(R.string.older);

    private String headerName;
    private Job jobModel;


    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public Job getJobModel() {
        return jobModel;
    }

    public void setJobModel(Job jobModel) {
        this.jobModel = jobModel;
    }

    public static ArrayList<JobItemListModel> getJobItemList(List<Job> jobs) {
        ArrayList<JobItemListModel> itemList = new ArrayList<JobItemListModel>();
        if(null != jobs && 0 < jobs.size()) {
            boolean older = false;
            boolean recent = false;
            Calendar currentDate = Calendar.getInstance();
            Calendar jobDate = Calendar.getInstance();
            currentDate.add(Calendar.DATE,-7);
            for(int i = 0; i < jobs.size(); i++) {
                Job job = jobs.get(i);
                if (!(UGMMacros.JOB_SOFT_DELETED.equalsIgnoreCase(job.getStatus()))) {
                    JobItemListModel itemListModel = new JobItemListModel();
                    jobDate.setTimeInMillis(job.getUpdatedTime());
                    if (currentDate.getTime().compareTo(jobDate.getTime()) <= 0) {
                        if (!recent) {
                            recent = true;
                            JobItemListModel headerModel = new JobItemListModel();
                            headerModel.setHeaderName(GROUP_RECENT);
                            headerModel.setJobModel(null);
                            itemList.add(headerModel);
                        }
                    } else {
                        if (!older) {
                            older = true;
                            JobItemListModel headerModel = new JobItemListModel();
                            headerModel.setHeaderName(GROUP_OLDER);
                            headerModel.setJobModel(null);
                            itemList.add(headerModel);
                        }
                    }
                    itemListModel.setJobModel(job);
                    itemList.add(itemListModel);
                }
            }
        }
        return itemList;
    }

    public static ArrayList<JobItemListModel> getDeletedJobItemList(List<Job> jobs) {
        ArrayList<JobItemListModel> itemList = new ArrayList<JobItemListModel>();
        if(null != jobs && 0 < jobs.size()) {
            boolean recent = false;
            int deletedJobSize = (jobs.size() > 10) ? 10 : jobs.size();
            for(int i = 0; i < deletedJobSize; i++) {
                Job job = jobs.get(i);
                if (UGMMacros.JOB_SOFT_DELETED.equalsIgnoreCase(job.getStatus())) {
                    JobItemListModel itemListModel = new JobItemListModel();
                    if (!recent) {
                        recent = true;
                        JobItemListModel headerModel = new JobItemListModel();
                        headerModel.setHeaderName(GROUP_RECENT);
                        headerModel.setJobModel(null);
                        itemList.add(headerModel);
                    }
                    itemListModel.setJobModel(job);
                    itemList.add(itemListModel);
                }
            }
        }
        return itemList;
    }
}
