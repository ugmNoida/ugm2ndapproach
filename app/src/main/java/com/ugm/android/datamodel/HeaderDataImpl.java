package com.ugm.android.datamodel;

import android.support.annotation.LayoutRes;

import com.ugm.android.ui.view.stickyheader.HeaderData;

public class HeaderDataImpl implements HeaderData {

    public static final int HEADER_TYPE = 1;

    private int headerType;
    @LayoutRes
    private final int layoutResource;

    public HeaderDataImpl(int headerType, @LayoutRes int layoutResource) {
        this.layoutResource = layoutResource;
        this.headerType = headerType;
    }

    @LayoutRes
    @Override
    public int getHeaderLayout() {
        return layoutResource;
    }

    @Override
    public int getHeaderType() {
        return headerType;
    }
}
