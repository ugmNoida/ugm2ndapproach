package com.ugm.android.datamodel;

import android.util.Log;

import com.ugm.android.R;
import com.ugm.android.UGMApplication;
import com.ugm.android.database.entity.Job;
import com.ugm.android.database.entity.Record;
import com.ugm.android.ui.view.stickyheader.StickyMainData;
import com.ugm.android.utilities.UGMMacros;
import com.ugm.android.utilities.UGMUtility;

import java.util.ArrayList;
import java.util.Collections;

public class ReportRecordModel implements StickyMainData {
    private static final String TAG = "ReportRecordModel";

    private boolean isHeader;
    private String rodNumber;
    private String rodLength;
    private String pitch;
    private String avgPitch;
    private String depth;
    private String roll;
    /* For Graph calculation required fields */
    //(Pitch * RodLength)
    private String depthIncremental;
    //(depthIncremental + relativeDepth(previous value))
    private String relativeDepth;
    //depth + relativeDepth
    private String relativeElevation;
    //(rodLength + boreLength(previous value))
    private String boreLength;
    //SQRT((rodLength*rodLength) - (depthIncremental*depthIncremental))
    private String distanceIncrement;
    //(distanceIncrement + xDistance(previous value))
    private String xDistance;
    // (-1) * depth
    private String depthInPdf;
    private String recordCreatedDTTM;
    private String latitude;
    private String longitude;

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public String getRodNumber() {
        return rodNumber;
    }

    public void setRodNumber(String rodNumber) {
        this.rodNumber = rodNumber;
    }

    public String getRodLength() {
        return rodLength;
    }

    public void setRodLength(String rodLength) {
        this.rodLength = rodLength;
    }

    public String getBoreLength() {
        return boreLength;
    }

    public void setBoreLength(String boreLength) {
        this.boreLength = boreLength;
    }

    public String getxDistance() {
        return xDistance;
    }

    public void setxDistance(String xDistance) {
        this.xDistance = xDistance;
    }

    public String getPitch() {
        return pitch;
    }

    public void setPitch(String pitch) {
        this.pitch = pitch;
    }

    public String getAvgPitch() {
        return avgPitch;
    }

    public void setAvgPitch(String avgPitch) {
        this.avgPitch = avgPitch;
    }

    public String getRelativeDepth() {
        return relativeDepth;
    }

    public void setRelativeDepth(String relativeDepth) {
        this.relativeDepth = relativeDepth;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getRelativeElevation() {
        return relativeElevation;
    }

    public void setRelativeElevation(String relativeElevation) {
        this.relativeElevation = relativeElevation;
    }

    public String getDepthIncremental() {
        return depthIncremental;
    }

    public void setDepthIncremental(String depthIncremental) {
        this.depthIncremental = depthIncremental;
    }

    public String getDistanceIncrement() {
        return distanceIncrement;
    }

    public void setDistanceIncrement(String distanceIncrement) {
        this.distanceIncrement = distanceIncrement;
    }

    public String getRoll() {
        return roll;
    }

    public void setRoll(String roll) {
        this.roll = roll;
    }

    public String getDepthInPdf() {
        return depthInPdf;
    }

    public void setDepthInPdf(String depthInPdf) {
        this.depthInPdf = depthInPdf;
    }

    public String getRecordCreatedDTTM() {
        return recordCreatedDTTM;
    }

    public void setRecordCreatedDTTM(String recordCreatedDTTM) {
        this.recordCreatedDTTM = recordCreatedDTTM;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public static ArrayList<ReportRecordModel> getRodwiseDataModel(ArrayList<Record> records, boolean isPDF, Job job) {
        //ArrayList<Record> records = getDummyData();
        ArrayList<ReportRecordModel> reportRecordModels = new ArrayList<ReportRecordModel>();
        if(isPDF) {
            reportRecordModels.add(getHeaderText(job));
        }
        return getReportRecordModelList(records, reportRecordModels, job);
    }

    public static ArrayList<ReportRecordModel> getChartDataModel(ArrayList<Record> records, Job job) {
        //ArrayList<Record> records = getDummyData();
        ArrayList<ReportRecordModel> reportRecordModels = new ArrayList<ReportRecordModel>();
        return getReportRecordModelList(records, reportRecordModels, job);
    }

    private static ArrayList<ReportRecordModel> getReportRecordModelList(ArrayList<Record> records,
                                 ArrayList<ReportRecordModel> reportRecordModels, Job job) {
        int totalRecords = records.size() - 1;
        String prevRelDepth = "";
        String prevBoreLength = "";
        String prevXDistance = "";
        String prevPitch = "";
        String preAvgPitch = "";
        for(int i = totalRecords; i >= 0; i--) {
            ReportRecordModel model = new ReportRecordModel();
            String rodNumber = String.valueOf(records.get(i).getRodNumber());
            model.setRodNumber(rodNumber);
            model.setRodLength(records.get(i).getRodLength());
            model.setPitch(getValidPitch(records.get(i).getPitch()));
            model.setAvgPitch(getAveragePitch(prevPitch, model.getPitch(), preAvgPitch));
            prevPitch = model.getPitch();
            preAvgPitch = model.getAvgPitch();
            model.setDepth(getValidDepth(records.get(i).getDepth()));
            //TODO: Need to be finalyze
            /*if(i == totalRecords) {
                if(!UGMUtility.isValidFloatString(model.getDepth())) {
                    model.setDepth("0.0");
                }
            }*/
            model.setRoll(records.get(i).getRoll());
            model.setDepthInPdf(getDepthInPdfValue(model.getDepth()));
            String validRodLength = getValidRodLength(model.getRodLength());
            model.setDepthIncremental(getDepthIncrementalValue(model.getAvgPitch(), validRodLength, job));
            model.setRelativeDepth(getRelativeDepthValue(model.getDepthIncremental(), prevRelDepth));
            prevRelDepth = model.getRelativeDepth();
            if(rodNumber.equalsIgnoreCase("0")) {
                model.setRelativeElevation(records.get(i).getRelativeElevation());
            } else {
                model.setRelativeElevation(getRelativeElevationValue(model.getDepth(), model.getRelativeDepth()));
            }
            model.setBoreLength(getBoreLengthValue(validRodLength, prevBoreLength));
            prevBoreLength = model.getBoreLength();
            model.setDistanceIncrement(getDistanceIncrementValue(validRodLength, model.getDepthIncremental()));
            model.setxDistance(getXDistanceValue(model.getDistanceIncrement(), prevXDistance));
            prevXDistance = model.getxDistance();
            model.setRecordCreatedDTTM(UGMUtility.covertLongToDFormattedDate(records.get(i).getCreatedTime(), UGMMacros.FORMATTER2));
            model.setLatitude(records.get(i).getLatitude());
            model.setLongitude(records.get(i).getLongitude());
            model.setHeader(false);
            reportRecordModels.add(model);
        }
        return reportRecordModels;
    }

    public static ReportRecordModel getHeaderText(Job job) {
        ReportRecordModel model = new ReportRecordModel();
        model.setRodNumber(getLocalizedValue(R.string.rod_no_dot));
        model.setRodLength(getLocalizedValue(R.string.rod_len));
        model.setBoreLength(getLocalizedValue(R.string.bore_len));
        model.setxDistance(getLocalizedValue(R.string.x_dist));
        if(UGMMacros.VALUE_PITCH_PERCENTAGE.equalsIgnoreCase(job.getPrefPitch())) {
            model.setAvgPitch(getLocalizedValue(R.string.avg_pitch));
        } else {
            model.setAvgPitch(getLocalizedValue(R.string.avg_pitch)+"(\u00b0)");
        }
        model.setRelativeDepth(getLocalizedValue(R.string.rel_depth));
        if(UGMMacros.VALUE_DEPTH_METER.equalsIgnoreCase(job.getPrefDepth())) {
            model.setDepthInPdf(getLocalizedValue(R.string.text_depth)+"(m)");
        } else {
            model.setDepthInPdf(getLocalizedValue(R.string.text_depth)+"(ft)");
        }
        model.setRelativeElevation(getLocalizedValue(R.string.rel_ele));
        model.setRecordCreatedDTTM(getLocalizedValue(R.string.date));
        model.setLatitude(getLocalizedValue(R.string.lat));
        model.setLongitude(getLocalizedValue(R.string.long_text));
        model.setHeader(true);
        return model;
    }

    private static String getValidRodLength(String rodLengthStr) {
        /*if(UGMUtility.isValidFloatString(rodLengthStr)) {
            float rodLength = Float.valueOf(rodLengthStr);
            if(0 < rodLength) {
                return rodLengthStr;
            }
        }*/
        return rodLengthStr;
    }

    private static String getValidPitch(String pitchStr) {
        if(UGMUtility.isValidFloatString(pitchStr)) {
            float pitch = Float.valueOf(pitchStr);
            return UGMUtility.floatFormat(pitch);
        } else {
            return "";
        }
    }

    private static String getAveragePitch(String prevPitchStr, String currPitchStr, String prevAvgPitch) {
        if(UGMUtility.isValidFloatString(currPitchStr)) {
            float currPitch = Float.valueOf(currPitchStr);
            if(UGMUtility.isValidFloatString(prevPitchStr)) {
                float prevPitch = Float.valueOf(prevPitchStr);
                if((0 < currPitch && 0 > prevPitch) ||
                        (0 > currPitch && 0 < prevPitch)){
                    float avgPitch = (Math.abs(currPitch) + Math.abs(prevPitch))/2;
                    return UGMUtility.floatFormat(avgPitch);
                } else {
                    float avgPitch = (currPitch + prevPitch)/2;
                    return UGMUtility.floatFormat(avgPitch);
                }
            } else {
                return UGMUtility.floatFormat(currPitch);
            }
        } else {
            return prevAvgPitch;
        }
    }

    private static String getValidDepth(String depthStr) {
        if(UGMUtility.isValidFloatString(depthStr)) {
            float depth = Float.valueOf(depthStr);
            return UGMUtility.floatFormat(depth);
        }
        return "";
    }

    private static String getDepthIncrementalValue(String avgPitchStr, String rodLengthStr, Job job) {
        if(UGMUtility.isValidFloatString(rodLengthStr)) {
            float rodLength = Float.valueOf(rodLengthStr);
            if(UGMUtility.isValidFloatString(avgPitchStr)) {
                float pitch = Float.valueOf(avgPitchStr);
                //return String.format("%.2f", ((pitch/100) * rodLength));
                if(UGMMacros.VALUE_PITCH_DEGREE.equalsIgnoreCase(job.getPrefPitch())){
                    return UGMUtility.floatFormat((float) ((pitch * rodLength) / Math.sqrt(3250 + Math.pow(pitch, 2))));
                } else {
                    return UGMUtility.floatFormat((float) ((pitch * rodLength) / Math.sqrt(10000 + Math.pow(pitch, 2))));
                }
            }
        }
        return "";
    }

    private static String getRelativeDepthValue(String depthIncrStr, String prevRelDepthStr) {
        if(UGMUtility.isValidFloatString(depthIncrStr)) {
            float depthIncrement = Float.valueOf(depthIncrStr);
            if(UGMUtility.isValidFloatString(prevRelDepthStr)) {
                float prevRelDepth = Float.valueOf(prevRelDepthStr);
                return UGMUtility.floatFormat(depthIncrement + prevRelDepth);
            }
            return UGMUtility.floatFormat(depthIncrement);
        }
        return "";
    }

    private static String getRelativeElevationValue(String depthStr, String relDepthStr) {
        if(UGMUtility.isValidFloatString(relDepthStr)) {
            float relDepth = Float.valueOf(relDepthStr);
            if(UGMUtility.isValidFloatString(depthStr)) {
                float depth = Float.valueOf(depthStr);
                return UGMUtility.floatFormat(depth + relDepth);
            }
        }
        return "";
    }

    private static String getBoreLengthValue(String rodLenStr, String prevBoreLenStr) {
        if(UGMUtility.isValidFloatString(rodLenStr)) {
            float rodLength = Float.valueOf(rodLenStr);
            if(UGMUtility.isValidFloatString(prevBoreLenStr)) {
                float prevBoreLen = Float.valueOf(prevBoreLenStr);
                return UGMUtility.floatFormat(rodLength + prevBoreLen);
            }
            return UGMUtility.floatFormat(rodLength);
        }
        return "";
    }

    private static String getDistanceIncrementValue(String rodLenStr, String depthIncrStr) {
        if(UGMUtility.isValidFloatString(depthIncrStr)) {
            float depthIncrement = Float.valueOf(depthIncrStr);
            if (UGMUtility.isValidFloatString(rodLenStr)) {
                float rodLength = Float.valueOf(rodLenStr);
                double distIncreamentVal = (Math.sqrt(Math.abs(Math.pow(rodLength, 2) - Math.pow(depthIncrement, 2))));
                return UGMUtility.floatFormat((float)distIncreamentVal);
            }
        }
        return "";
    }

    private static String getXDistanceValue(String distIncrementStr, String prevXDistanceStr) {
        if(UGMUtility.isValidFloatString(distIncrementStr)) {
            float distIncrement = Float.valueOf(distIncrementStr);
            if(UGMUtility.isValidFloatString(prevXDistanceStr)) {
                float prevXDistance = Float.valueOf(prevXDistanceStr);
                return UGMUtility.floatFormat(distIncrement + prevXDistance);
            }
            return UGMUtility.floatFormat(distIncrement);
        }
        return "";
    }

    private static String getDepthInPdfValue(String depthStr) {
        if(UGMUtility.isValidFloatString(depthStr)) {
            float depth = Float.valueOf(depthStr);
            return UGMUtility.floatFormat((-1) * depth);
        }
        return "";
    }

    public static String getRelativeElevation(String pitch, String depth, String rodLength, Job job) {
        pitch = getValidPitch(pitch);
        depth = getValidDepth(depth);
        rodLength = getValidRodLength(rodLength);
        String depthIncremental = getDepthIncrementalValue(pitch, rodLength, job);
        String relativeDepth = getRelativeDepthValue(depthIncremental, "");
        String relativeElevation = getRelativeElevationValue(depth, relativeDepth);
        if(!UGMUtility.isValidString(relativeElevation)) {
            relativeElevation = "0";
        }
        return relativeElevation;
    }

    public static String getPitchAndDepth(String pitch, String depth, String rodLength, Job job) {
        pitch = getValidPitch(pitch);
        depth = getValidDepth(depth);
        rodLength = getValidRodLength(rodLength);
        String depthIncremental = getDepthIncrementalValue(pitch, rodLength, job);
        String relativeDepth = getRelativeDepthValue(depthIncremental, "");
        String relativeElevation = getRelativeElevationValue(depth, relativeDepth);
        if(!UGMUtility.isValidString(relativeElevation)) {
            relativeElevation = "0";
        }
        return relativeElevation;
    }

    //---------------DUMMY DATA-------------------------

    private static ArrayList<Record> getDummyData() {
        ArrayList<Record> records = new ArrayList<Record>();
        for(int i = 0; i <= 41; i++) {
            Record record = getRecord(i);
            records.add(record);
        }
        Collections.reverse(records);
        return records;
    }

    private static Record getRecord(int index) {
        Record record = new Record();
        if(index == 0) {
            record.setRodNumber(index);
            record.setRodLength("0");
            record.setPitch("-25.00");
            record.setDepth("0");
        }
        if(index == 1) {
            record.setRodNumber(index);
            record.setRodLength("1.5");
            record.setPitch("-25.00");
            record.setDepth("0.38");
        } else if(index == 2) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-25.00");
            record.setDepth("1.13");
        } else if(index == 3) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-25.00");
            record.setDepth("1.88");
        } else if(index == 4) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-25.00");
            record.setDepth("2.63");
        } else if(index == 5) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-25.00");
            record.setDepth("3.38");
        } else if(index == 6) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-25.00");
            record.setDepth("4.13");
        } else if(index == 7) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-25.00");
            record.setDepth("4.87");
        } else if(index == 8) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-25.00");
            record.setDepth("5.62");
        } else if(index == 9) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-22.0");
            record.setDepth("6.30");
        } else if(index == 10) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-18.50");
            record.setDepth("6.86");
        } else if(index == 11) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-16.00");
            record.setDepth("7.34");
        } else if(index == 12) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-12.50");
            record.setDepth("7.71");
        } else if(index == 13) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-7.50");
            record.setDepth("7.94");
        } else if(index == 14) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-2.50");
            record.setDepth("8.01");
        } else if(index == 15) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 16) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 17) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 18) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 19) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 20) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 21) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 22) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 23) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 24) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 25) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 26) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 27) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 28) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 29) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("-0.0");
            record.setDepth("8.01");
        } else if(index == 30) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("2.00");
            record.setDepth("7.95");
        }else if(index == 31) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("4.50");
            record.setDepth("7.82");
        }else if(index == 32) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("7.50");
            record.setDepth("7.59");
        }else if(index == 33) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("12.50");
            record.setDepth("7.22");
        }else if(index == 34) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("17.50");
            record.setDepth("6.69");
        }else if(index == 35) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("22.50");
            record.setDepth("6.02");
        }else if(index == 36) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("25.00");
            record.setDepth("5.27");
        }else if(index == 37) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("25.00");
            record.setDepth("4.52");
        }else if(index == 38) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("25.00");
            record.setDepth("3.77");
        }else if(index == 39) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("25.00");
            record.setDepth("3.02");
        }else if(index == 40) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("25.00");
            record.setDepth("2.27");
        }else if(index == 41) {
            record.setRodNumber(index);
            record.setRodLength("3");
            record.setPitch("25.00");
            record.setDepth("1.52");
        }
        return record;
    }

    /*public static ArrayList<ReportRecordModel> getRodwiseDataModel(ArrayList<Record> records) {
        ArrayList<ReportRecordModel> reportRecordModels = new ArrayList<ReportRecordModel>();
        int totalRecords = records.size() - 1;
        String prevRelDepth = "0";
        String prevBoreLength = "0";
        String prevXDistance = "0";
        String prevDepth = "0";
        for(int i = 0; i < records.size(); i++) {
            //for(int i = totalRecords; i >= 0; i--) {
            ReportRecordModel model = new ReportRecordModel();
            model.setRodNumber(String.valueOf(records.get(i).getRodNumber()));
            model.setRodLength(records.get(i).getRodLength());
            model.setPitch(getValidPitch(records.get(i).getPitch()));
            model.setDepth(getValidDepth(records.get(i).getDepth(), prevDepth));
            float validDepth = Float.parseFloat(model.getDepth());
            if(validDepth < 0 || validDepth > 0) {
                prevDepth = String.valueOf(validDepth);
            }
            //model.setDepthInPdf(getDepthInPdfValue(model.getDepth()));
            model.setDepthInPdf(getDepthInPdfValue(model.getDepth()));
            model.setDepthIncremental(getDepthIncrementalValue(model.getPitch(), model.getRodLength()));
            model.setRelativeDepth(getRelativeDepthValue(model.getDepthIncremental(), prevRelDepth));
            prevRelDepth = model.getRelativeDepth();
            model.setRelativeElevation(getRelativeElevationValue(model.getDepth(), prevRelDepth));
            model.setBoreLength(getBoreLengthValue(model.getRodLength(), prevBoreLength));
            prevBoreLength = model.getBoreLength();
            model.setDistanceIncrement(getDistanceIncrementValue(model.getRodLength(), model.getDepthIncremental()));
            model.setxDistance(getXDistanceValue(model.getDistanceIncrement(), prevXDistance));
            prevXDistance = model.getxDistance();
            reportRecordModels.add(model);
        }
        return reportRecordModels;
    }*/

    public static String getLocalizedValue(int id) {
        return UGMApplication.getInstance().getString(id);
    }
}
